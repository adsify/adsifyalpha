<?php

use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('status')->insert([
    	[
    		'code' => 101,
            'name' => 'processing',
            'hint' => 'Adsify is collecting ads for this page right now..',
        ],
        [
    		'code' => 102,
            'name' => 'running',
            'hint' => 'This page has ads running at the moment.',
        ],
        [
    		'code' => 103,
            'name' => 'noads',
            'hint' => 'This page has no ads running at the moment.',
        ],
        [
            'code' => 104,
            'name' => 'unused',
            'hint' => 'page',
        ],
        [
    		'code' => 111,
            'name' => 'new',
            'hint' => 'post',
        ],
        [
    		'code' => 112,
            'name' => 'running',
            'hint' => 'post',
        ],
        [
    		'code' => 113,
            'name' => 'stopped',
            'hint' => 'post',
        ],
        [
    		'code' => 201,
            'name' => 'sleep',
            'hint' => 'server',
        ],
        [
    		'code' => 202,
            'name' => 'processing',
            'hint' => 'server',
        ],
        [
    		'code' => 203,
            'name' => 'broken',
            'hint' => 'server',
        ],
        [
    		'code' => 301,
            'name' => 'free',
            'hint' => 'subscriber',
        ],
        [
    		'code' => 302,
            'name' => 'active',
            'hint' => 'subscriber',
        ],
        [
    		'code' => 303,
            'name' => 'failed',
            'hint' => 'subscriber',
        ],
        [
            'code' => 304,
            'name' => 'unactive',
            'hint' => 'subscriber',
        ],
        [
    		'code' => 900,
            'name' => 'suspended',
            'hint' => 'user',
        ],
        [
            'code' => 501,
            'name' => 'adsify-logs',
            'hint' => 'log',
        ],
        [
            'code' => 502,
            'name' => 'python-logs',
            'hint' => 'log',
        ],
        [
            'code' => 503,
            'name' => 'adsify-payment',
            'hint' => 'log',
        ],
        [
    		'code' => 601,
            'name' => 'pending',
            'hint' => 'ticket',
        ],
        [
    		'code' => 602,
            'name' => 'solved',
            'hint' => 'ticket',
        ],
        [
    		'code' => 603,
            'name' => 'canceled',
            'hint' => 'ticket',
        ],
        [
            'code' => 701,
            'name' => 'completed',
            'hint' => 'payment',
        ],
        [
            'code' => 702,
            'name' => 'failed',
            'hint' => 'payment',
        ]
        ]);

        DB::table('servers')->insert([
        [   
            'name' => 'php-server',
            'status_code' => 201,
        ]
        ]);

        DB::table('profiles')->inser([
            [
                'name' => 'php-core',
                'proxy' => 'none',
                'fb' => 'none',
                'server_id' => 1,
                'status_code' => 201,
            ]
        ]);

        DB::table('categories')->insert([
        [   
            'name' => 'Payment',
        ],
        [   
            'name' => 'Technical',
        ],
        [   
            'name' => 'Account',
        ],
        [   
            'name' => 'Suggestion',
        ],
        ]);

        DB::table('countries')->insert([
        [   
            'id' => 0,
            'name' => 'Global ads',
        ]
        ]);
    }
}
