<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTablePosts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->bigInteger('id');
            $table->primary('id');
            $table->string('type');
            $table->string('text', 3000);
            $table->string('url', 3000);
            $table->bigInteger('page_id');
            $table->string('post');
            $table->string('thumbnail')->nullable();
            $table->integer('status_code');
            $table->timestamps();
            $table->foreign('page_id')->references('id')->on('pages');
            $table->foreign('status_code')->references('code')->on('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {   
        Schema::dropIfExists('posts');
    }
}
