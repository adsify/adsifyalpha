<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTablePages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->bigInteger('id');
            $table->primary('id');
            $table->string('name');
            $table->integer('status_code');
            $table->string('logo_src');
            $table->string('size')->nullable();
            $table->timestamps();
            $table->foreign('status_code')->references('code')->on('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {   
        Schema::dropIfExists('pages');
    }
}
