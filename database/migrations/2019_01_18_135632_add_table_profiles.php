<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableProfiles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('proxy');
            $table->string('fb');
            $table->integer('server_id')->unsigned();
            $table->integer('status_code');
            $table->string('last_activity')->nullable();
            $table->integer('log')->default('0');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
            $table->foreign('status_code')->references('code')->on('status');
            $table->foreign('server_id')->references('id')->on('servers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
