<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('journal.profile');
});

Route::get('/xtest', 'JournalController@test');

Route::get('/maintenance', function(){
	return view('layouts.maintenance');
});

Route::get('/spa/{any}', function () {
    return view('layouts.spa');
})->where('any','.*');

//-----------------------  Adsify -------------------------------

Route::get('/', 'HomeController@home');
Route::get('/home' , 'HomeController@home');
Route::get('/faq' , 'HomeController@faq');
Route::get('/policy' , 'HomeController@policy');
Route::get('/terms' , 'HomeController@terms');

//---------------------------  Journal Controller  ------------------------

Auth::routes(['verify' => false]);

Route::get('/journal' , 'JournalController@index');

Route::post('/journal/filter' , 'JournalController@filter');
Route::get('/journal/filter/reset', 'JournalController@resetFilter');

Route::get('/journal/pages', 'JournalController@showPages');
Route::get('/journal/pages/get', 'JournalController@getPages');
Route::get('/journal/pages/similars/{id}', 'JournalController@getSimilars');

Route::post('/journal/store', 'JournalController@storePage');
Route::post('/journal/similar/store', 'JournalController@storeSimilar');
Route::post('/journal/page/archive', 'JournalController@archivePage');
Route::post('/journal/page/unarchive', 'JournalController@unarchivePage');

Route::get('/journal/post/{id}', 'JournalController@details');

Route::get('/journal/subscription', 'UserController@subscription');
Route::post('/journal/subscription/subscribe', 'PaymentController@subscribe');
Route::get('/journal/subscription/cancel', 'PaymentController@cancel');
Route::get('/thankyou', function(){
	return view('journal.thankyou');
});

Route::get('/journal/profile', 'UserController@profile');
Route::post('/journal/profile/update', 'UserController@profileUpdate');
Route::get('/journal/profile/cancel', 'UserController@emailCancel');
Route::get('/journal/profile/email', 'UserController@emailUpdate');

Route::get('/journal/bookmarks/{id}' , 'JournalController@addBookmark');
Route::get('/journal/bookmarks' , 'JournalController@bookmarks');
Route::get('/journal/bookmarks/cancel/{id}' , 'JournalController@cancelBookmark');

Route::get('/banned' , function(){
    return view('errors.banned');
});

//-------------------------  Support Controller  --------------------------------

Route::get('/journal/tickets', 'TicketsController@create');
Route::post('/journal/ticket/store', 'TicketsController@store');
Route::get('/journal/ticket/{id}', 'TicketsController@show');
Route::post('/journal/ticket/comment' , 'TicketsController@postComment');
Route::post('/journal/ticket/cancel' , 'TicketsController@cancelTicket');

//------------------------- Payment Controller ------------------------------------
Route::get('/journal/subscription/agreement' , 'PaymentController@createAgreement');
Route::get('/journal/subscription/{status}' , 'PaymentController@executeAgreement');
Route::post('/notify', 'PaymentController@paypalNotify');

Route::get('/plan/create', 'PaymentController@create');
Route::get('/plan/show/{id}', 'PaymentController@show');
Route::get('/plan/list', 'PaymentController@list');
Route::get('/plan/delete/{id}' , 'PaymentController@delete');
Route::get('/plan/activate/{id}', 'PaymentController@activate');

// -----------------------  Admin Controller  --------------------------------------

Route::get('/admin', 'AdminController@servers');
Route::get('/admin/servers', 'AdminController@servers');
Route::post('/admin/server/store', 'AdminController@addServer');
Route::get('/admin/server/remove/{id}', 'AdminController@removeServer');
Route::get('/admin/servers/logs/{code}' , 'AdminController@getLogs');
Route::get('/admin/servers/log/{id}' , 'AdminController@serverLogs');
Route::get('/admin/server/log/clear/{id}', 'AdminController@serverClearLogs');
Route::post('/admin/servers/update', 'AdminController@serverUpdate');

Route::get('/admin/servers/profile/remove/{id}', 'AdminController@removeProfile');
Route::get('/admin/servers/profile/log/{id}', 'AdminController@profileLogs');
Route::get('/admin/servers/profile/log/clear/{id}', 'AdminController@profileClearLogs');
Route::post('/admin/servers/profile/update', 'AdminController@profileUpdate');

Route::get('/admin/pages', 'AdminController@pages');
Route::get('/admin/pages/featured', 'AdminController@featuredPages');
Route::post('/admin/pages/featured/store', 'AdminController@storeFeatured');
Route::post('/admin/pages/featured/delete/{id}', 'AdminController@deleteFeatured');

Route::get('/admin/settings' , 'AdminController@settings');
Route::post('/admin/settings/update', 'AdminController@updateSettings');

Route::get('/admin/subscriptions/billed', 'AdminController@billedSubs');
Route::get('/admin/subscriptions/unbilled', 'AdminController@unbilledSubs');
Route::get('/admin/subscriptions/suspended', 'AdminController@suspendedSubs');
Route::get('/admin/subscriptions/free', 'AdminController@freeSubs');

Route::get('/admin/payments/plans', 'AdminController@listPlans');
Route::get('/admin/payments/manual', 'AdminController@manualPayment');

Route::post('/admin/payments/plans/create', 'AdminController@createPlan');
Route::post('/admin/payments/manual/create', 'AdminController@createPayment');

Route::get('/admin/payments/plans/activate/{id}', 'AdminController@activatePlan');
Route::get('/admin/payments/plans/unactivate/{id}', 'AdminController@unactivatePlan');
Route::get('/admin/payments/plans/show/{id}', 'AdminController@showPlan');
Route::get('/admin/payments/plans/delete/{id}' , 'AdminController@deletePlan');
Route::get('/admin/payments/plans/setcurrent/{id}' , 'AdminController@setCurrentPlan');

Route::get('/admin/subscriptions/suspend/{id}', 'AdminController@suspend_sub');
Route::get('/admin/subscriptions/activate/{id}', 'AdminController@activate_sub');
Route::get('/admin/subscriptions/cancel/{id}', 'AdminController@cancel_sub');

Route::get('/admin/users/active', 'AdminController@activeUsers');
Route::get('/admin/users/suspend', 'AdminController@suspendUsers');

Route::get('/admin/users/logs/{id}' , 'AdminController@userLogs');
Route::get('/admin/users/{id}', 'AdminController@userInfo');

Route::get('/admin/users/suspend/{id}', 'AdminController@suspend_user');
Route::get('/admin/users/activate/{id}', 'AdminController@activate_user');

Route::get('/admin/tickets/pending', 'AdminController@pendingTickets');
Route::get('/admin/tickets/solved', 'AdminController@solvedTickets');
Route::get('/admin/tickets/canceled', 'AdminController@canceledTickets');
Route::post('/admin/ticket/create', 'AdminController@createTicket');

Route::get('/admin/ticket/{id}', 'AdminController@showTicket');
Route::post('/admin/ticket/comment', 'AdminController@postComment');

Route::get('/admin/ticket/{id}/solve', 'AdminController@solveTicket');
Route::get('/admin/ticket/{id}/cancel', 'AdminController@cancelTicket');

// ---------------- API CONTROLLER -----------------

Route::get('/api/pages', 'ApiController@pages');
Route::post('/api/posts', 'ApiController@posts');
Route::post('/api/similars', 'ApiController@similars');

Route::post('/api/errors', 'ApiController@errors');
Route::post('/api/errors/image', 'ApiController@imageError');
Route::post('/api/server/update', 'ApiController@updateProfile');
Route::post('/api/server/health', 'ApiController@serverHealth');
Route::post('/api/server/store', 'ApiController@addServer');
Route::post('/api/server/profile/store', 'ApiController@addProfile');

Route::get('/api/task', 'ApiController@getTask');
Route::get('/api/tasks', 'ApiController@getTasks');
Route::post('/api/task/posts', 'ApiController@taskPosts');
Route::post('/api/tasks/restore', 'ApiController@restoreTasks');

Route::get('/api/newpage', 'ApiController@getPage');
Route::get('/api/simpage', 'ApiController@getSimPage');
Route::post('/api/tasks/store', 'ApiController@storeTasks');

Route::get('/api/servers/check', 'ApiController@checkServers');
Route::get('/api/verifyPosts', 'ApiController@verify_posts');
Route::get('/api/verifyUsers', 'ApiController@verify_users');
Route::get('/api/verifyPages', 'ApiController@verify_pages');

Route::get('/api/newtasks', 'ApiController@getNewTasks');
