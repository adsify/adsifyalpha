<?php

use Illuminate\Http\Request;

/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::group(['prefix' => 'auth'], function($router){
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
});

Route::group(['middleware' => 'jwt.auth'], function ($router) {
    Route::get('/spa/posts', 'SpaController@posts');
    Route::get('/spa/posts/{id}', 'SpaController@getPost');
    Route::get('/spa/pages', 'SpaController@pages');
    Route::post('/spa/pages/add', 'SpaController@addPage');
    Route::get('/spa/tickets', 'SpaController@tickets');
    Route::post('/spa/tickets/add', 'SpaController@addTicket');
    Route::get('/spa/comments/{id}', 'SpaController@comments');
    Route::post('/spa/comments/add', 'SpaController@addComment');
    Route::get('/spa/user', 'SpaController@getUser');
    Route::post('/spa/profile/update', 'SpaController@profileUpdate');
    Route::get('/spa/profile/cancel', 'SpaController@emailCancel');
    Route::get('/spa/stats/pages', 'StatsController@statsPages');
    Route::post('/spa/stats/users', 'StatsController@statsUsers');
    Route::post('/spa/pages/archive', 'SpaController@archivePage');
    Route::post('/spa/pages/unarchive', 'SpaController@unarchivePage');
});





// ------------- VUE JS ROUTES -------------------