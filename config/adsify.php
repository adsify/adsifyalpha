<?php

	return [
		'price'   	  => 20,
		'mail'     	  => 'support@adsify.io',
		'support'  	  => 'support@adsify.io',
		'delay'    	  => env('DELAY'),
		'trial'    	  => env('TRIAL'),
		'free'     	  => env('FREE_PAGES'),
		'maintenance' => env('MAINTENANCE'),
		'message'	  => str_replace("_"," ",env("MAINTENANCE_MESSAGE")),
		'norefer'     => 'https://noreferer.pro/?to=',
	];

