<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => 'email.adsify.io',
        'secret' => '501883deed496e31ba726e9e1856af05-9b463597-e35e7822',
        'endpoint' => 'api.mailgun.net/v3/email.adsify.io',
    ],

    'paypal' => [
        'mode' => 'live',
        'sandbox' => [
            'id' => '',
            'secret' => '',
            'notify' => 'https://ipnpb.sandbox.paypal.com/cgi-bin/webscr'
        ],
        'live' => [
            'id' => '',
            'secret' => '',
            'notify' => 'https://ipnpb.paypal.com/cgi-bin/webscr'
        ],
        
        'url' => [
            'redirect' => '',
            'cancel' => 'https://adsify.io/journal',
            'agreement' => [
                'success' => 'https://adsify.io/journal/subscription/true',
                'failure' => 'https://adsify.io/journal/subscription/false',
            ],
        ],
    ],

];
