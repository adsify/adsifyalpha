@extends('layouts.nsmaster')

@section('body')
    <section class="main-content d-flex flex-column align-items-center">
      <div class="self-container d-flex flex-column page-text error">
        <hr class="hr" />
        <div style="width: 70%;margin: auto;">
            <h4 class="">404</h4>
            <h6>Ooops... the page you looking for does not exist!</h6>
        </div>
        <div class="text-center" style="margin-top: 25px;">
            <a href="{{url('/journal')}}" class="btn btn-primary btn-sm btn-shadow " style="width: 40%;">GO BACK HOME</a>
        </div>
      </div>
    </section>
@endsection
