@extends('layouts.admin')

@push('before-css')
    <!-- This page CSS -->
    <!-- chartist CSS -->
    <link href="{{asset('plugins/vendors/morrisjs/morris.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/pages/easy-pie-chart.css')}}" rel="stylesheet">
    <!--c3 CSS -->
    <link href="{{asset('plugins/vendors/c3-master/c3.min.css')}}" rel="stylesheet">
    <!--Toaster Popup message CSS -->
    <link href="{{asset('plugins/vendors/toast-master/css/jquery.toast.css')}}" rel="stylesheet">
    <!-- Vector CSS -->
    <link href="{{asset('plugins/vendors/vectormap/jquery-jvectormap-2.0.2.css')}}" rel="stylesheet" />
    <!-- page css -->
    <link href="{{asset('assets/css/pages/google-vector-map.css')}}" rel="stylesheet">

    <link href="{{asset('plugins/vendors/jsgrid/jsgrid.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('plugins/vendors/jsgrid/jsgrid-theme.min.css')}}" rel="stylesheet" type="text/css"/>

    <link href="{{asset('assets/css/style-2.css')}}" rel="stylesheet">

     <link href="{{asset('assets/css/pages/footable.core.css')}}" rel="stylesheet">
@endpush

@section('content')
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 mb-4">
                <div class="float-right">
                    <a data-toggle="modal" href="" data-target=".add-server"  class="btn waves-effect waves-light btn-rounded btn-primary float-right ml-4">Add Page</a>
                </div>
            </div>
            <div class="modal fade add-server" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel3" aria-hidden="true">
                <div class="modal-dialog modle-510">
                    <form action="{{ url('admin/pages/featured/store') }}" method="post">
                        {{ csrf_field() }}
                        <div class="modal-content">
                            <div class="modal-header pl-5 pt-5 pr-5 pb-0">
                                <h4 class="modal-title text-uppercase font-weight-bold" id="myLargeModalLabel3">Add Page</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body  p-5">
                                <div class="form-group row">
                                    <label class="text-dark font-weight-bold col-md-4">Page link</label>
                                    <div class="col-md-8">
                                        <input  type="text" required="" placeholder="Link" name="link" class="form-control">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group row">
                                    <label class="text-dark font-weight-bold col-md-4">Name</label>
                                    <div class="col-md-8">
                                        <input  type="text" required="" placeholder="Name" name="name" class="form-control">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group row">
                                    <label class="text-dark font-weight-bold col-md-4">Running Ads</label>
                                    <div class="col-md-8">
                                        <input  type="text" required="" placeholder="Ads" name="ads" class="form-control">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group row">
                                    <label class="text-dark font-weight-bold col-md-4">Page Likes</label>
                                    <div class="col-md-8">
                                        <input  type="text" required="" placeholder="Likes" name="likes" class="form-control">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group row">
                                    <label class="text-dark font-weight-bold col-md-4">Chance</label>
                                    <div class="col-md-8">
                                        <input  type="text" required="" placeholder="Chance" name="chance" class="form-control">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group row">
                                    <label class="text-dark font-weight-bold col-md-4">Shop Link</label>
                                    <div class="col-md-8">
                                        <input  type="text" required="" placeholder="Shop - Without Https://" name="shop" class="form-control">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="modal-footer  p-4">
                                <button type="submit" class="btn btn-rounded btn-success">Save</button>
                                <button type="button" class="btn btn-rounded  btn-secondary">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            @if(sizeof($pages) > 0)
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <h4 class="card-title text-uppercase m-b-0 pull-left">Featured Pages</h4>
                            <div id="example23_wrapper" class="dataTables_wrapper no-footer">
                                <table id="example23" class="display nowrap table table m-t-30 table-hover2 contact-list dataTable no-footer" role="grid" aria-describedby="example23_info">
                                <thead>
                                    <tr class="table-header" role="row">
                                        <th class="sorting" tabindex="0" style="width: 280.6px;">Name</th>
                                        <th class="sorting" tabindex="0" aria-controls="example23" style="width: 117.6px;">Running Ads</th>
                                        <th class="sorting" tabindex="0" aria-controls="example23" rowspan="1" colspan="1"  style="width: 180.6px;">Likes</th>
                                        <th class="sorting" tabindex="0" aria-controls="example23" rowspan="1" colspan="1"  style="width: 180.6px;">Chance</th>
                                        <th class="sorting" tabindex="0" aria-controls="example23" style="width: 280.6px;">Shop</th>
                                        <th class="op-0 sorting" tabindex="0" aria-controls="example23" style="width: 41.6px;">Activation</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($pages as $page)
                                    <tr role="row" class="odd">
                                        <td class="text-dark weight-600 sorting_1"><img src="{{asset($page->logo_src)}}" class="img-circle border w-40 m-r-5" alt="" title="">
                                            {{$page->name}}
                                        </td>
                                        <td>{{$page->ads}}</td>
                                        <td>{{$page->likes}}</td>
                                        <td>{{$page->chance}}%</td>
                                        <td>
                                            <a target="_blank" href="{{config('adsify.norefer')}}https://{{$page->shop}}">{{$page->shop}}</a>
                                        </td>
                                        <td>
                                            <a href="" class="dropdown-toggle no-after" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-ellipsis-v"></i>
                                            </a>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item" href="{{url('/admin/pages/featured/delete/'.$page->id)}}">Delete</a>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
@endsection

@push('js')
    <!-- This page plugins -->
    <!-- Popup message jquery -->
    <script src="{{asset('plugins/vendors/toast-master/js/jquery.toast.js')}}"></script>
    <!-- EASY PIE CHART JS -->
    <script src="{{asset('plugins/vendors/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js')}}"></script>
    <script src="{{asset('plugins/vendors/jquery.easy-pie-chart/easy-pie-chart.init.js')}}"></script>
    <!-- ============================================================== -->
    <script src="{{asset('plugins/vendors/knob/jquery.knob.js')}}"></script>

    <!--Sparkline JavaScript -->
    <script src="{{asset('plugins/vendors/sparkline/jquery.sparkline.min.js')}}"></script>

    <!--Morris JavaScript -->
    <script src="{{asset('plugins/vendors/raphael/raphael-min.js')}}"></script>
    <script src="{{asset('plugins/vendors/morrisjs/morris.js')}}"></script>



    <!-- Vector map JavaScript -->
    <script src="{{asset('plugins/vendors/vectormap/jquery-jvectormap-2.0.2.min.js')}}"></script>
    <script src="{{asset('plugins/vendors/vectormap/jquery-jvectormap-world-mill-en.js')}}"></script>

    <!-- Dashboard JS -->
    <script src="{{asset('assets/js/dashboard-analytics.js')}}"></script>

     <!-- This is data table -->
    <script src="{{asset('plugins/vendors/datatables/jquery.dataTables.min.js')}}"></script>
    <script>
        $(function () {
            $('#myTable').DataTable();
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 5
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 6,
                "drawCallback": function (settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function (group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function () {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });


            $('#myTable2').DataTable();
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 5
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 6,
                "drawCallback": function (settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function (group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function () {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });


            $('#myTable3').DataTable();
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 5
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 6,
                "drawCallback": function (settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function (group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function () {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });


        });
        $('#example23').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });
    
    </script>
    <script>
        $(function () {
            $('#editable-datatable').DataTable();
        });
    </script>
    <!-- Popup message jquery -->
    <script src="{{asset('plugins/vendors/toast-master/js/jquery.toast.js')}}"></script>
    <!-- Style switcher -->
    <script src="{{asset('plugins/vendors/styleswitcher/jQuery.style.switcher.js')}}"></script>
    <!-- Editable -->
    <script src="{{asset('plugins/vendors/jquery-datatables-editable/jquery.dataTables.js')}}"></script>
    <script src="{{asset('plugins/vendors/datatables/dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('plugins/vendors/tiny-editable/mindmup-editabletable.js')}}"></script>
    <script src="{{asset('plugins/vendors/tiny-editable/numeric-input-example.js')}}"></script>
    <!-- JSgrid table JavaScript -->
    <script src="{{asset('plugins/vendors/jsgrid/db.js')}}"></script>
    <script src="{{asset('plugins/vendors/jsgrid/jsgrid.min.js')}}"></script>
    <script src="{{asset('plugins/vendors/jsgrid/jsgrid-data.js')}}"></script>


@endpush