@extends('layouts.admin')

@push('before-css')

<link href="{{asset('assets/css/pages/dashboard-server.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/vendors/morrisjs/morris.css')}}" rel="stylesheet">
    <!--c3 CSS -->
    <link href="{{asset('plugins/vendors/c3-master/c3.min.css')}}" rel="stylesheet">
    <!--Toaster Popup message CSS -->
    <link href="{{asset('plugins/vendors/toast-master/css/jquery.toast.css')}}" rel="stylesheet">
    <!-- page css -->
    <link href="{{asset('assets/css/pages/tab-page.css')}}" rel="stylesheet">
    
    <link href="{{asset('plugins/vendors/bootstrap-checkbox/awesome-bootstrap-checkbox.css')}}" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="{{asset('assets/css/vendor/owl.carousel.min.css')}}">

@endpush

@section('content')
    <div class="container-fluid" id="servers">
        <div class="row page-titles">
            <div class="col-md-5 col-sm-12 align-self-center">
                <h4 class="weight-500 m-0">Adsify Servers</h4>
            </div>
            <div class="col-md-7 col-sm-12 align-self-center text-right">
                <ul class="nav nav-tabs customtab pro-customtab" role="tablist">
                    <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#pro-statistics" role="tab"><span>Statistics</span></a> </li>
                    <li class="nav-item"> <a class="nav-link" @click="getLogs(501)" data-toggle="tab" href="#pro-task" role="tab"><span>Logs</span></a> </li>
                </ul>
            </div>
        </div>
        <div class="tab-content" >
            <!--div class="modal fade log-message" style="z-index: 1060;" tabindex="-1" role="dialog"  aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="row form-group m-b-20">
                                <div class="col-md-3">
                                    <h4 class="font-16 m-0 p-0 font-weight-bold">Log Message</h4>
                                </div>
                                <div class="col-md-9">
                                    <textarea class="form-control text-h">@{{message}}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div-->
            <div class="tab-pane p-0 active" id="pro-statistics" role="tabpanel">
                <div class="modal fade add-server" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel3" aria-hidden="true">
                    <div class="modal-dialog modle-big">
                        <form action="{{ url('admin/server/store') }}" method="post">
                            {{ csrf_field() }}
                            <div class="modal-content">
                                <div class="modal-header pl-5 pt-5 pr-5 pb-0">
                                    <h4 class="modal-title text-uppercase font-weight-bold" id="myLargeModalLabel3">Add Server</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                </div>
                                <div class="modal-body  p-5">
                                    <div class="form-group row">
                                        <label class="text-dark font-weight-bold col-md-4">Name</label>
                                        <div class="col-md-8">
                                            <input  type="text" placeholder="Name" name="name" class="form-control" required="">
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group row">
                                        <label class="text-dark font-weight-bold col-md-4">Description</label>
                                        <div class="col-md-8">
                                            <textarea class="form-control" name="description" required=""></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="modal-footer  p-4">
                                    <button type="submit" class="btn btn-rounded btn-success">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal fade edit-profile" style="z-index: 1060;" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel3" aria-hidden="true">
                    <div class="modal-dialog modle-big">
                        <form action="{{ url('admin/servers/profile/update') }}" method="post">
                            {{ csrf_field() }}
                            <div class="modal-content">
                                <div class="modal-header pl-5 pt-5 pr-5 pb-0">
                                    <h4 class="modal-title text-uppercase font-weight-bold" id="myLargeModalLabel3">Edit Profile</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                </div>
                                <div class="modal-body  p-5">
                                    <div class="form-group row">
                                        <label class="text-dark font-weight-bold col-md-4">Name</label>
                                        <input type="hidden" name="id" :value="profile.id">
                                        <div class="col-md-8">
                                            <input  type="text" placeholder="Name" name="name" :value="profile.name" class="form-control" required="">
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group row">
                                        <label class="text-dark font-weight-bold col-md-4">Proxy</label>
                                        <div class="col-md-8">
                                            <textarea class="form-control" name="proxy" :value="profile.proxy" required=""></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="text-dark font-weight-bold col-md-4">Facebook</label>
                                        <div class="col-md-8">
                                            <textarea class="form-control" name="fb" :value="profile.fb" required=""></textarea>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label">Status - (current : @{{profile.status}})</label>
                                        <div class="col-8">
                                            <select class="custom-select col-12" name="status">
                                                <option value="201">SLEEP</option>
                                                <option value="203">BROKEN</option>
                                            </select>
                                        </div>
                                      </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="modal-footer  p-4">
                                    <button type="submit" class="btn btn-rounded btn-success">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3 col-md-3 col-lg-3">
                        <h5 class="card-title float-left m-t-20 m-l-20 m-r-20 m-b-40  align-self-center text-uppercase">Servers</h5>
                    </div>
                    <div class="col-sm-9 col-md-9 col-lg-9 mb-4">
                        <div class="float-right">
                            <a href="" data-toggle="modal"  data-target=".add-server"  class="btn waves-effect waves-light btn-rounded btn-primary float-right ml-4">Add Server</a>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                @if($servers)
                <div class="col-lg-12" id="log">
                    <div class="row">
                        @include('partials.admin.server-items')
                    </div>
                </div>
                @endif
            </div>
            <div class="clearfix"></div>
            <div class="tab-pane p-0" id="pro-task" role="tabpanel" >
                <div class="card" >
                    <div class="card-body">
                        <div class="row">
                            <div class=" col-md-6 col-sm-6">
                                <h5 class="card-title float-left m-t-15 m-l-20 m-r-20 align-self-center text-uppercase">Servers Logs</h5>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-12 mt-4">
                            <ul class="list-inline m-b-30 text-uppercase lp-5 font-medium font-12">
                                <li @click="getLogs(501)"><i class="fa fa-circle m-r-5 font-9 text-danger"></i>Adsify Logs</li>
                                <!--li @click="getLogs(502)"><i class="fa fa-circle m-r-5 font-9 text-warning"></i>Servers Logs</li-->
                                <li @click="getLogs(503)"><i class="fa fa-circle m-r-5 font-9 text-danger"></i>Payments Logs</li>
                                
                            </ul>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table class="table color-table primary-table">
                                    <thead>
                                    <tr>
                                        <th>Message</th>
                                        <th>Date</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr v-for="log in serverLogs">
                                        <td>@{{log.message}}</td>
                                        <td>@{{log.created_at}}</td>
                                        <!--td class="flex flex-column justify-center relative shadow-hover default hover-bg-white br2 hide-child">
                                            <a href="#" data-toggle="modal" data-target=".log-message" @click="sendInfo(log.message)" class="log-content flex flex-column items-center justify-center color-inherit w-100 pa2 br2 br--top no-underline hover-bg-blue4 hover-white" title="Message">
                                                <i class="fas fa-align-justify"></i>
                                            </a>
                                        </td-->
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="text-center">
                                <button @click="prevPage" type="button" class="btn waves-effect waves-light btn-sm btn-rounded btn-primary">Previous</button>
                                <button @click="nextPage" type="button" class="btn waves-effect waves-light btn-sm btn-rounded btn-primary">Next</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@push('js')

    <script src="{{asset('assets/js/vendor/owl.carousel.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
          $(".owl-carousel").owlCarousel({
            lazyLoad:true,
            dots: false,
          });
        });
    </script>

    <script src="{{asset('plugins/vendors/d3/d3.min.js')}}"></script>
    <script src="{{asset('plugins/vendors/c3-master/c3.min.js')}}"></script>
    <!--jquery knob -->
    <script src="{{asset('plugins/vendors/knob/jquery.knob.js')}}"></script>
    <!--Morris JavaScript -->
    <script src="{{asset('plugins/vendors/raphael/raphael-min.js')}}"></script>
    <script src="{{asset('plugins/vendors/morrisjs/morris.js')}}"></script>
    <!-- Popup message jquery -->

    <!-- Dashboard JS -->
    <script src="{{asset('assets/js/dashboard-projects.js')}}"></script>
    <script src="http://localhost:8000/plugins/vendors/sparkline/jquery.sparkline.min.js"></script>
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="{{asset('plugins/vendors/styleswitcher/jQuery.style.switcher.js')}}"></script>
    <script src="{{asset('plugins/vendors/datatables/jquery.dataTables.min.js')}}"></script>

    <script>
        $(function() {
            $('#myTable').DataTable();
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 8,
                "drawCallback": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });

        });
        $('#example23').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });
    </script>

    <script type="text/javascript">
        var logs = new Vue({
            el:'#servers',
            data:{
                logs: [],
                pageSize:5,
                currentPage:1,
                profile:{
                    id:"",
                    name:"",
                    proxy:"",
                    fb:"",
                    status:"",
                },
                clearLink:"",
            },
            methods: {
                getLogs: function(code){
                    console.log()
                    axios.get(window.Laravel.url+'/admin/servers/logs/'+code)
                        .then(response => {
                            this.logs = response.data;
                        })
                        .catch(error => {
                            console.log('error : ' , error);
                        });
                },
                getLog: function(id){
                    axios.get(window.Laravel.url+'/admin/servers/profile/log/'+id)
                        .then(response => {
                            this.logs = response.data.logs;
                            this.profile.id = response.data.profile.id;
                            this.profile.name = response.data.profile.name;
                            this.profile.proxy = response.data.profile.proxy;
                            this.profile.fb = response.data.profile.fb;
                            this.profile.status = response.data.profile.status_code;
                            this.clearLink = window.Laravel.url+'/admin/servers/profile/log/clear/'+id;
                        })
                        .catch(error => {
                            console.log('error : ' , error);
                        });
                },
                nextPage:function() {
                  if((this.currentPage*this.pageSize) < this.logs.length) this.currentPage++;
                },
                prevPage:function() {
                  if(this.currentPage > 1) this.currentPage--;
                },
                clearLog:function(){
                    axios.get(this.clearLink)
                        .then(response => {
                            this.logs = [];
                        })
                        .catch(error => {
                            console.log('error : ' , error);
                        });
                }
            },
            computed:{
                serverLogs:function() {
                  return this.logs.filter((row, index) => {
                    let start = (this.currentPage-1)*this.pageSize;
                    let end = this.currentPage*this.pageSize;
                    if(index >= start && index < end) return true;
                  });
                }
            },
            mounted:function(){
                this.getLogs(501);
            }
        });
    </script>
    
    <script type="text/javascript">
        function confirm(url){
            Swal({
              title: 'Are you sure?',
              text: "",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes!',
              cancelButtonText: 'No.'
            }).then((result) => {
              if (result.value) {
                $.get(url, function(data) {
                    location.reload();
                });
              }
            })
        }
    </script>
    

@endpush