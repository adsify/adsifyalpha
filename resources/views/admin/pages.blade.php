@extends('layouts.admin')

@push('before-css')
    <link href="{{asset('assets/css/pages/easy-pie-chart.css')}}" rel="stylesheet">
    <!--c3 CSS -->
    <link href="{{asset('plugins/vendors/c3-master/c3.min.css')}}" rel="stylesheet">

    <link href="{{asset('assets/css/style-2.css')}}" rel="stylesheet">
@endpush

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card panel-main o-income panel-refresh">
                    <div class="refresh-container">
                        <div class="preloader">
                            <div class="loader">
                                <div class="loader__figure"></div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body panel-wrapper">
                        <div class="d-flex m-b-10 no-block">
                            <h5 class="card-title m-b-0 align-self-center">top 5 pages</h5>
                        </div>
                        <div class="row">
                            @foreach($top5 as $page)
                            <div class="col text-center">
                                <div class="chart easy-pie-chart-1" data-percent="{{ $page->percent }}">
                                    <span class="percent  text-purple">{{$page->percent}}/100</span></div>
                                <p class="font-16 font-weight-bold">{{$page->name}}</p>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <h4 class="card-title text-uppercase m-b-0 pull-left">All pages</h4>
                            <div id="tableSort_wrapper" class="dataTables_wrapper no-footer">
                                <table id="tableSort" class="display nowrap table table m-t-30 table-hover2 contact-list footable-loaded footable dataTable no-footer" role="grid">
                                <thead>
                                <tr class="table-header" role="row">
                                    <th tabindex="0" rowspan="1" colspan="1" style="width: 289.6px;">Page Name</th>
                                    <th tabindex="0" rowspan="1" colspan="1" style="width: 125.6px;">Uses</th>
                                    <th tabindex="0" rowspan="1" colspan="1" style="width: 190.6px;">Status</th>
                                    <th tabindex="0" rowspan="1" colspan="1" style="width: 43.6px;">&nbsp;</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($pages as $page)
                                <tr role="row" class="odd">
                                    <td class="text-dark weight-600 sorting_1">
                                        <img src="{{ asset($page->logo_src) }}" class="img-circle w-40 m-r-5" alt="" title="">
                                        {{ $page->name }}
                                    </td>
                                    <td>{{ $page->users_count }}</td>
                                    <td>{{ $page->status->name }}</td>
                                    <td><a href="" class="dropdown-toggle no-after" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-v"></i></a>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" target="_blank" href="{{config('adsify.norefer')}}https://www.facebook.com/{{ $page->id }}">Page Link</a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                               </tbody>
                            </table>
                            
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{asset('plugins/vendors/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js')}}"></script>
    <script src="{{asset('plugins/vendors/jquery.easy-pie-chart/easy-pie-chart.init.js')}}"></script>
    <script src="{{asset('plugins/vendors/datatables/jquery.dataTables.min.js')}}"></script>
    <script>
        
        $('#tableSort').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });
    </script>


@endpush