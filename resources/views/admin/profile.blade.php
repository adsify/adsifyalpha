@extends('layouts.admin')

@push('before-css')
<link href="{{asset('plugins/vendors/switchery/dist/switchery.min.css')}}" rel="stylesheet">

<script>
    window.Laravel = {!! json_encode([
        'url' => url('/'),
        'id'  => $user->id,
    ]) !!}
</script>
@endpush

@section('content')
<div class="container-fluid">
        <div class="row">
          <div class="col-lg-12 col-md-12">
            <div class="card">
              <div class="card-body  card2 pt-3">
                <div class="row">
                  <div class="col-lg-6 col-md-9 font-18 font-weight-bold text-uppercase">Profile</div>
                  <div class="col-lg-6 col-md-9 font-18 font-weight-bold text-uppercase">Agreement</div>
                </div>
              </div>
              <div class="card-body">
                <div class="row">
                  <div class="col-md-6">
                    <h2 class="font-25 font-medium">{{$user->name.' '.$user->last}}</h2>
                    <div class="row mb-2">
                      <div class="col-6 font-weight-bold text-dark">Email</div>
                      <div class="col">{{$user->email}}</div>
                    </div>
                    <div class="row mb-2">
                      <div class="col-6 font-weight-bold text-dark">Subscription</div>
                      <div class="col">{{strtoupper($user->subscription->status->name)}}</div>
                    </div>
                    <div class="row mb-2">
                      <div class="col-6 font-weight-bold text-dark">Subscription Service</div>
                      <div class="col">{{strtoupper($user->subscription->service)}}</div>
                    </div>
                    <div class="row mb-2">
                      <div class="col-6 font-weight-bold text-dark">Account Creation</div>
                      <div class="col">{{$user->created_at}}</div>
                    </div>
                  </div>
                  @if(isset($user->agreement))
                  <div class="col-md-6">
                    <h2 class="font-25 font-medium">{{$user->agreement->id}}</h2>
                    <p class="m-b-20">State : {{$user->agreement->state}}</p>
                    <div class="row mb-2">
                      <div class="col-6 font-weight-bold text-dark">Payer Email</div>
                      <div class="col">{{$user->agreement->payer_email}}</div>
                    </div>
                    <div class="row mb-2">
                      <div class="col-6 font-weight-bold text-dark">Payer Name</div>
                      <div class="col">{{$user->agreement->payer_name}}</div>
                    </div>
                    <div class="row mb-2">
                      <div class="col-6 font-weight-bold text-dark">Payer Status</div>
                      <div class="col">{{$user->agreement->payer_status}}</div>
                    </div>
                    <div class="row mb-2">
                      <div class="col-6 font-weight-bold text-dark">Payer Address</div>
                      <div class="col">{{$user->agreement->payer_address}}</div>
                    </div>
                    <div class="row mb-2">
                      <div class="col-6 font-weight-bold text-dark">Payer Method</div>
                      <div class="col">{{$user->agreement->payer_method}}</div>
                    </div>
                    <div class="row mb-2">
                      <div class="col-6 font-weight-bold text-dark">Start Date</div>
                      <div class="col">{{$user->agreement->created_at}}</div>
                    </div>
                  </div>
                  @endif
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-body  card2 pt-5">
                <div class="row">
                  <div class="col-lg-12 col-md-12 font-18 font-weight-bold text-uppercase">Activity</div>
                </div>
              </div>
              <div class="card-body" id="info">
                <div class="row">
                  <div class="col-md-12 font-16">
                    <div class="table-responsive">
                      <table class="table color-table m-b-0">
                        <thead>
                          <tr>
                            <th scope="col">Description</th>
                            <th scope="col">IP</th>
                            <th scope="col">Agent</th>
                            <th scope="col">Creation Date</th>
                          </tr>
                        </thead>
                        <tbody v-for="log in pageLogs">
                          <tr>
                            <td class="font-bold">@{{ log.description }}</td>
                            <td>@{{ JSON.parse(log.properties).ip }}</td>
                            <td>@{{ JSON.parse(log.properties).agent }}</td>
                            <td>@{{ log.created_at }}</td>
                          </tr>
                        </tbody>
                      </table>
                      <div class="text-center">
                            <button @click="prevPage" type="button" class="btn waves-effect waves-light btn-sm btn-rounded btn-primary">Previous</button>
                            <button @click="nextPage" type="button" class="btn waves-effect waves-light btn-sm btn-rounded btn-primary">Next</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-body card2 pt-5">
                <div class="float-right">
                    <a href="{{url('/admin/payments/manual').'?email='.$user->email}}" class="btn waves-effect waves-light btn-rounded btn-primary float-right ml-4">Add Payment</a>
                </div>
                <div class="row">
                  <div class="col-lg-12 col-md-12 font-18 font-weight-bold text-uppercase">Payments</div>
                </div>
              </div>
              @if(isset($user->payments))
              <div class="card-body">
              	@foreach($user->payments as $payment)
                <div class="row">
                  <div class="col-lg-4 col-md-6 font-14 font-weight-bold m-b-20"><span class="fa fa-circle text-danger circle-tab mr-3"></span> #{{$payment->txn_id}} </div>
                  <div class="col-lg-4 col-md-6 m-b-20"> <span class="font-bold">{{$payment->title}}</span>
                    <div class="clearfix"></div>
                    <span class="mt-2 d-block m-b-10">{{$payment->service.' : $'.$payment->amount.'.00'}}</span> </div>
                  <div class="col-lg-4 col-md-6 font-weight-bold m-b-20"> {{$payment->created_at}}</div>
                </div>
                <div class="boder-li"></div>
                @endforeach
              </div>
              @endif
            </div>
          </div>
        </div>
      </div>

@endsection

@push('js')
<script type="text/javascript">
    var app = new Vue({
        el:'#info',
        data:{
            logs: [],
            pageSize:5,
            currentPage:1,
        },
        methods: {
            getLogs: function(id){
                axios.get(window.Laravel.url+'/admin/users/logs/'+id)
                    .then(response => {
                        this.logs = response.data;
                        console.log('success : ' , response.data);
                    })
                    .catch(error => {
                        console.log('error : ' , error);
                    });
            },
            nextPage:function() {
              if((this.currentPage*this.pageSize) < this.logs.length) this.currentPage++;
            },
            prevPage:function() {
              if(this.currentPage > 1) this.currentPage--;
            },
        },
        computed:{
            pageLogs:function() {
              return this.logs.filter((row, index) => {
                let start = (this.currentPage-1)*this.pageSize;
                let end = this.currentPage*this.pageSize;
                if(index >= start && index < end) return true;
              });
            }
        },
        mounted:function(){
            this.getLogs(window.Laravel.id);
        }
    });


</script>
@endpush