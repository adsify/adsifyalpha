@extends('layouts.admin')

@push('before-css')

    <!--c3 CSS -->
    <link href="{{asset('plugins/vendors/c3-master/c3.min.css')}}" rel="stylesheet">

    <link href="{{asset('assets/css/style-2.css')}}" rel="stylesheet">
@endpush

@section('content')
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 mb-4">
                <div class="float-right">
                    <a data-toggle="modal" href="" data-target=".add-server"  class="btn waves-effect waves-light btn-rounded btn-primary float-right ml-4">Add Plan</a>
                </div>
            </div>
            <div class="modal fade add-server" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel3" aria-hidden="true">
                <div class="modal-dialog modle-510">
                    <form action="{{ url('admin/payments/plans/create') }}" method="post">
                        {{ csrf_field() }}
                        <div class="modal-content">
                            <div class="modal-header pl-5 pt-5 pr-5 pb-0">
                                <h4 class="modal-title text-uppercase font-weight-bold" id="myLargeModalLabel3">Add Plan</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body  p-5">
                                <div class="form-group row">
                                    <label class="text-dark font-weight-bold col-md-4">Name</label>
                                    <div class="col-md-8">
                                        <input  type="text" required="" placeholder="Name" name="name" class="form-control">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group row">
                                    <label class="text-dark font-weight-bold col-md-4">Price</label>
                                    <div class="col-md-8">
                                        <input  type="text" required="" placeholder="Price" name="price" class="form-control">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="modal-footer  p-4">
                                <button type="submit" class="btn btn-rounded btn-success">Save</button>
                                <button type="button" class="btn btn-rounded  btn-secondary">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            @if(sizeof($plans) > 0)
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <h4 class="card-title text-uppercase m-b-0 pull-left">Payment Plans</h4>
                            <div id="tableSort_wrapper" class="dataTables_wrapper no-footer">
                                <table id="tableSort" class="display nowrap table table m-t-30 table-hover2 contact-list footable-loaded footable dataTable no-footer" role="grid" aria-describedby="tableSort_info">
                                <thead>
                                <tr class="table-header" role="row">
                                    <th tabindex="0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Member: activate to sort column descending" style="width: 289.6px;">ID Plan</th>
                                    <th tabindex="0" rowspan="1" colspan="1" style="width: 325.6px;">Name</th>
                                    <th tabindex="0" rowspan="1" colspan="1" style="width: 325.6px;">Price</th>
                                    <th tabindex="0" rowspan="1" colspan="1" style="width: 190.6px;">
                                    State
                                    </th>
                                    <th class="op-0 sorting" tabindex="0" rowspan="1" colspan="1" style="width: 43.6px;">Options</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($plans as $plan)
                                <tr role="row" class="odd">
                                    <td class="text-dark weight-600 sorting_1">
                                        {{ $plan->plan_id }}
                                    </td>
                                    <td>{{$plan->name}}</td>
                                    <td>{{$plan->price}}</td>
                                    <td>{{$plan->state}}</td>
                                    <td>
                                        <a href="" class="dropdown-toggle no-after" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-ellipsis-v"></i>
                                        </a>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="plans/show/{{$plan->plan_id}}">Show</a>
                                            <a class="dropdown-item" href="plans/activate/{{ $plan->plan_id}}">Activate</a>
                                            <a class="dropdown-item" href="plans/unactivate/{{ $plan->plan_id}}">UnActivate</a>
                                            <a class="dropdown-item" href="plans/delete/{{ $plan->plan_id}}">Delete</a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                               </tbody>
                            </table>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
@endsection

@push('js')
     <!-- This is data table -->
    <script src="{{asset('plugins/vendors/datatables/jquery.dataTables.min.js')}}"></script>
    <script>
        $('#tableSort').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });
    </script>


@endpush