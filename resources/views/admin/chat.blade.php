@extends('layouts.admin')

@push('before-css')
  <!-- Page CSS -->
  <link href="{{asset('assets/css/pages/chat-app-page.css')}}" rel="stylesheet">
@endpush
@section('content')
  <div class="container-fluid">
    <div class="row">
      <!-- Column -->
      <div class="col-md-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-md-12">
                <div class="chat">
                  <div class="chat-header clearfix"> 
                    <div class="chat-about">
                      <div class="chat-with text-dark float-right">Owner : {{$ticket->user->email}} - <a target="_blank" href="{{url('/admin/users/'.$ticket->user->id)}}">View Profile</a></div>
                      <div class="chat-with text-dark">Title: {{$ticket->title}}</div>
                      <div class="chat-with">Status: {{strtoupper($ticket->status->name)}}</div>
                      <div class="chat-with">Category: {{strtoupper($ticket->category->name)}}</div>
                    </div>
                    @if($ticket->status_code == 601)
                    <div class="ml-auto float-right text-light">
                      <a href="{{url('/admin/ticket/'.$ticket->id.'/solve')}}" class="btn waves-effect waves-light btn-light-round btn-success">Solve</a>
                      <a href="{{url('/admin/ticket/'.$ticket->id.'/cancel')}}" class="btn waves-effect waves-light btn-light-round btn-danger">Canceled</a>
                    </div>
                    @endif
                  </div>
                  <div class="chat-history" id="slimtest2" style="height:400px;">
                    <ul>
                      @foreach($comments as $comment)
                        @if($comment->user_id == Auth::user()->id)
                          <li>
                            <div class="message-data"> <span class="message-data-name"><i class="fa fa-circle online"></i> {{$comment->user->name}}</span> <span class="message-data-time">{{$comment->created_at}}</span> </div>
                            <div class="message my-message"> {{$comment->comment}} </div>
                          </li>
                        @else
                          <li class="clearfix">
                            <div class="message-data text-right"> 
                              <span class="message-data-time" >{{$comment->created_at}}</span>
                              &nbsp; &nbsp; <span class="message-data-name" >{{$comment->user->name}}</span> <i class="fa fa-circle me"></i> </div>
                            <div class="message other-message float-right"> {{$comment->comment}} </div>
                          </li>
                        @endif
                      @endforeach
                    </ul>
                  </div>
                  @if($ticket->status_code == 601)
                  <form action="{{url('/admin/ticket/comment')}}" method="post">
                    @csrf
                    <div class="chat-message clearfix">
                      <input type="hidden" name="ticket_id" value="{{ $ticket->id }}">
                      <button type="submit mb-2">Send</button>
                      <textarea name="comment" class="form-control" id="message-to-send" placeholder ="Type your message" rows="3"></textarea>
                    </div>
                  </form>
                  @endif
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('js')


  <!-- Popup message jquery -->
  <script src="{{asset('plugins/vendors/toast-master/js/jquery.toast.js')}}"></script>
  <!-- Style switcher -->
  <script src="{{asset('plugins/vendors/styleswitcher/jQuery.style.switcher.js')}}"></script>
@endpush
