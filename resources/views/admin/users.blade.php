@extends('layouts.admin')

@push('before-css')
    <link href="{{asset('plugins/vendors/c3-master/c3.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/style-2.css')}}" rel="stylesheet">
@endpush

@section('content')
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <h4 class="card-title text-uppercase m-b-0 pull-left">{{$title}} Users</h4>
                            <div id="tableSort_wrapper" class="dataTables_wrapper no-footer">
                                <table id="tableSort" class="display nowrap table table m-t-30 table-hover2 contact-list footable-loaded footable dataTable no-footer" role="grid" >
                                <thead>
                                <tr class="table-header" role="row">
                                    <th class="sorting" rowspan="1" colspan="1" style="width: 289.6px;">Full Name</th>
                                    <th class="sorting" rowspan="1" colspan="1" style="width: 325.6px;">Email Address</th>
                                    <th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 325.6px;">Payment Service</th>
                                    <th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 190.6px;">Status</th>
                                    <th class="op-0 sorting" tabindex="0" rowspan="1" colspan="1" style="width: 43.6px;">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $user)
                                <tr role="row" class="odd">
                                    <td class="text-dark weight-600 sorting_1">
                                        {{ $user->name . ' ' . $user->last }}
                                    </td>
                                    <td>{{$user->email}}</td>
                                    <td>{{$user->subscription->service}}</td>
                                    <td>{{$user->subscription->status->name}}</td>
                                    
                                    <td class="text-center">
                                        <a href="" class="dropdown-toggle no-after" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-ellipsis-v"></i>
                                        </a>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="{{url('/admin/users/'.$user->id)}}">View Profile</a>
                                            @if($user->subscription->status_code != 900)
                                            <a class="dropdown-item" href="suspend/{{ $user->id}}">Suspend</a>
                                            @else
                                            <a class="dropdown-item" href="activate/{{ $user->id}}">Activate</a>
                                            @endif
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                               </tbody>
                            </table>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')

    <script src="{{asset('plugins/vendors/datatables/jquery.dataTables.min.js')}}"></script>
    <script> 
        $('#tableSort').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });
    </script>

@endpush