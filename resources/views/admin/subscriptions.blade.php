@extends('layouts.admin')

@push('before-css')

    <!--c3 CSS -->
    <link href="{{asset('plugins/vendors/c3-master/c3.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/style-2.css')}}" rel="stylesheet">
@endpush

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="card">
                    <div class="card-body p-30">
                        <div class="d-flex pt-3 pb-2 no-block">
                            <div class="align-self-center mr-5 ml-4"><img src="{{asset('assets/imgs/icon/note.svg')}}" alt="" title="" width="55"></div>
                            <div class="align-slef-center mr-auto">
                                <h2 class="m-b-2 text-uppercase font-30 font-medium lp-5 text-pink">{{ $today }}</h2>
                                <h6 class="text-light m-b-0">Daily Subs</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="card">
                    <div class="card-body p-30">
                        <div class="d-flex pt-3 pb-2 no-block">
                            <div class="align-self-center mr-5 ml-4"><img src="{{asset('assets/imgs/icon/badge.svg')}}" alt="" title="" width="55"></div>
                            <div class="align-slef-center mr-auto">
                                <h2 class="m-b-2 text-uppercase font-30 font-medium lp-5 text-primary">$ {{ $month }}</h2>
                                <h6 class="text-light m-b-0">Monthly Revenue</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-12">
                <div class="card">
                    <div class="card-body p-30">
                        <div class="d-flex pt-3 pb-2 no-block">
                            <div class="align-self-center  mr-5 ml-4"><img src="{{asset('assets/imgs/icon/users.svg')}}" alt="" title="" width="55"> </div>
                            <div class="align-slef-center mr-auto">
                                <h2 class="m-b-2 text-uppercase font-30 font-medium lp-5 text-orange">{{$totalusers}}</h2>
                                <h6 class="text-light m-b-0">Today users</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <h4 class="card-title text-uppercase m-b-0 pull-left">{{$title}} Subscribers</h4>
                            <div id="tableSort_wrapper" class="dataTables_wrapper no-footer">
                                <table id="tableSort" class="display nowrap table table m-t-30 table-hover2 contact-list footable-loaded footable dataTable no-footer" role="grid" aria-describedby="tableSort_info">
                                <thead>
                                <tr class="table-header" role="row">
                                    <th tabindex="0" rowspan="1" colspan="1" style="width: 289.6px;">Name</th>
                                    <th tabindex="0" rowspan="1" colspan="1" style="width: 325.6px;">Email Address</th>
                                    <th tabindex="0" rowspan="1" colspan="1" aria-label="Email Address: activate to sort column ascending" style="width: 325.6px;">Payment Service</th>
                                    <th tabindex="0" rowspan="1" colspan="1" style="width: 190.6px;">
                                    Last Update/Delay
                                    </th>
                                    <th class="op-0 sorting" tabindex="0" rowspan="1" colspan="1" style="width: 43.6px;">Change Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($subs as $sub)
                                <tr role="row" class="odd">
                                    <td class="text-dark weight-600 sorting_1">
                                        {{ $sub->user->name }}
                                    </td>
                                    <td>{{$sub->user->email}}</td>
                                    <td>{{$sub->service}}</td>
                                    @if(isset($sub->delay))
                                    <td>{{$sub->delay}}</td>
                                    @else
                                    <td>{{$sub->updated_at}}</td>
                                    @endif
                                    <td class="text-center">
                                        <a href="" class="dropdown-toggle no-after" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-ellipsis-v"></i>
                                        </a>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="{{url('/admin/users/'.$sub->user->id)}}">View Profile</a>
                                            <a class="dropdown-item" href="cancel/{{ $sub->user->id}}">Set Free</a>
                                            <a class="dropdown-item" href="activate/{{ $sub->user->id}}">Set Active</a>
                                            <a class="dropdown-item" href="suspend/{{ $sub->user->id}}">Set Suspended</a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                               </tbody>
                            </table>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')

     <!-- This is data table -->
    <script src="{{asset('plugins/vendors/datatables/jquery.dataTables.min.js')}}"></script>
    <script>
        $('#tableSort').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });
    </script>


@endpush