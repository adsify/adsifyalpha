@extends('layouts.admin')

@push('before-css')
    <!--c3 CSS -->
    <link href="{{asset('plugins/vendors/c3-master/c3.min.css')}}" rel="stylesheet">

    <link href="{{asset('assets/css/style-2.css')}}" rel="stylesheet">

    <link href="{{asset('assets/css/pages/footable.core.css')}}" rel="stylesheet">
@endpush
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="modal fade add-ticket" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modle-big">
                <form action="{{ url('admin/ticket/create') }}" method="post">
                    {{ csrf_field() }}
                    <div class="modal-content">
                        <div class="modal-header pl-5 pt-5 pr-5 pb-0">
                            <h4 class="modal-title text-uppercase font-weight-bold" id="myLargeModalLabel3">Create new ticket</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body  p-5">
                            <div class="form-group row">
                                <label class="text-dark font-weight-bold col-md-4">User Email</label>
                                <div class="col-md-8">
                                    <input  type="email" placeholder="Email address" name="email" class="form-control" required="">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group row">
                                <label class="text-dark font-weight-bold col-md-4">Message</label>
                                <div class="col-md-8">
                                    <textarea class="form-control" name="message" required=""></textarea>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group row">
                                <label class="text-dark font-weight-bold col-md-4">Title</label>
                                <div class="col-md-8">
                                    <input type="text" name="title" autocomplete="off" class="form-control">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group row">
                                <label class="text-dark font-weight-bold col-md-4">Category</label>
                                <div class="col-md-8">
                                    <select name="category" class="form-control">
                                        <option value="0" selected="selected">Choose...</option>
                                        @if(isset($categories))
                                        @foreach($categories as $cat)
                                        <option value="{{$cat->id}}">{{$cat->name}}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="modal-footer  p-4">
                            <button type="submit" class="btn btn-rounded btn-success">Create</button>
                            <button type="button" class="btn btn-rounded  btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="text-right col-md-12 mb-3">
            <a href="" data-toggle="modal" data-target=".add-ticket"  class="btn waves-effect waves-light btn-rounded btn-primary float-right ml-4">Create Ticket</a>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <h4 class="card-title text-uppercase m-b-0 pull-left">{{$title}} Tickets</h4>
                        <div id="tableSort_wrapper" class="dataTables_wrapper no-footer">
                        <table id="tableSort" class="display nowrap table m-t-30 table-hover2 contact-list footable-loaded footable dataTable no-footer" role="grid" >
                            <thead>
                            <tr class="table-header" role="row">
                            	<th tabindex="0" rowspan="1" colspan="1" style="width: 180.6px;">#TicketID</th>
                            	<th tabindex="0" rowspan="1" colspan="1" style="width: 120.6px;">Category</th>
                            	<th tabindex="0" rowspan="1" colspan="1" style="width: 200.6px;">Title</th>
                            	<th tabindex="0" rowspan="1" colspan="1" style="width: 190.6px;">Created By</th>
                                <th tabindex="0" rowspan="1" colspan="1" style="width: 120.6px;">Date</th>
                            	<th class="op-0" tabindex="0" rowspan="1" colspan="1" style="width: 43.6px;">&nbsp;</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($tickets as $ticket)
                            <tr role="row" class="odd">
                                <td class="text-dark weight-600">
                                	@if($ticket->status_code == 601)
    			                    <span class="fa fa-circle text-warning circle-tab mr-3"></span>
    			                    @elseif($ticket->status_code == 602)
    			                    <span class="fa fa-circle text-success circle-tab mr-3"></span>
    			                    @else
    			                    <span class="fa fa-circle text-danger circle-tab mr-3"></span>
    			                    @endif
                                    #{{$ticket->ticket_id}}
                                </td>
                                <td>{{$ticket->category->name}}</td>
                                <td>{{$ticket->title}}</td>
                                <td>{{$ticket->user->email}}</td>
                                <td>{{$ticket->created_at}}</td>
                                <td class="">
                                	<a href="{{url('/admin/ticket/'.$ticket->id)}}" class="no-after" ><i class="fas fa-arrow-right"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')

    <script src="{{asset('plugins/vendors/datatables/jquery.dataTables.min.js')}}"></script>
    <script> 
        $('#tableSort').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });
    </script>

@endpush