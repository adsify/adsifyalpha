@extends('layouts.admin')

@push('before-css')
<link href="{{asset('plugins/vendors/switchery/dist/switchery.min.css')}}" rel="stylesheet">
@endpush

@section('content')
<div class="container-fluid">
    <div class="row">
		<div class="col-lg-16 col-md-12">
			<div class="card">
			  <div class="card-body">
			    <h4 class="card-title text-uppercase m-b-25">Adsify Manual Payment</h4>
			    <form class="form" action="{{url('/admin/payments/manual/create')}}" method="POST">
			      @csrf
			      <div class="form-group m-t-40 row">
			        <label class="col-4 col-form-label">User Email</label>
			        <div class="col-8">
			          <input class="form-control" type="text" name="email" value="{{$email}}" required>
			        </div>
			      </div>
			      <div class="form-group row">
			        <label class="col-4 col-form-label">Amount</label>
			        <div class="col-8">
			          <input class="form-control" type="text" name="amount" value="" required="">
			        </div>
			      </div>
			      <div class="form-group row">
			        <label class="col-4 col-form-label">Type</label>
			        <div class="col-8">
			          <input class="form-control" type="text" name="type" value="" required placeholder="paypal/bitcoin">
			        </div>
			      </div>
			      <div class="form-group row">
			        <label class="col-4 col-form-label">Transaction ID</label>
			        <div class="col-8">
			          <input class="form-control" type="text" name="txn_id" value="" required placeholder="id from paypal">
			        </div>
			      </div>
			      <div class="form-group row">
			      	<div class="col-4"></div>
			      	<div class="col-4">
			      		<button type="submit" class="btn waves-effect waves-light btn-light-round btn-primary">Add Payment</button>
			      	</div>
			      </div>
			    </form>
			  </div>
			</div>
		</div>
	</div>
</div>

@endsection

@push('js')

@endpush