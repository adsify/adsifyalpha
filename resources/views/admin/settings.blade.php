@extends('layouts.admin')

@push('before-css')
<link href="{{asset('plugins/vendors/switchery/dist/switchery.min.css')}}" rel="stylesheet">
@endpush

@section('content')
<div class="container-fluid">
    <div class="row">
		<div class="col-lg-16 col-md-12">
			<div class="card">
			  <div class="card-body">
			    <h4 class="card-title text-uppercase m-b-25">Adsify Settings</h4>
			    <form class="form" action="{{url('/admin/settings/update')}}" method="POST">
			      @csrf
			      <div class="form-group m-t-40 row">
			        <label class="col-4 col-form-label">Free Pages</label>
			        <div class="col-8">
			          <input class="form-control" type="text" name="pages" value="{{ config('adsify.free') }}">
			        </div>
			      </div>
			      <div class="form-group row">
			        <label class="col-4 col-form-label">Payment Delay</label>
			        <div class="col-8">
			          <input class="form-control" type="text" name="delay" value="{{ config('adsify.delay') }}">
			        </div>
			      </div>
			      <div class="form-group row">
			        <label class="col-4 col-form-label">Free Trial Days</label>
			        <div class="col-8">
			          <input class="form-control" type="text" name="trial" value="{{ config('adsify.trial') }}">
			        </div>
			      </div>
			      <div class="form-group row">
			        <label class="col-4 col-form-label">Maintenance mode</label>
			        <div class="col-8">
				        <select class="custom-select col-12" name="maintenance">
	                        <option selected value="0">OFF</option>
	                        <option value="1">ON</option>
	                    </select>
			        </div>
			      </div>
			      <div class="form-group row">
			        <label class="col-4 col-form-label">Maintenance Message</label>
			        <div class="col-8">
			          <textarea class="form-control" name="message">{{config('adsify.message')}}</textarea>
			        </div>
			      </div>
			      <div class="form-group row">
			      	<div class="col-4"></div>
			      	<div class="col-4">
			      		<button type="submit" class="btn waves-effect waves-light btn-light-round btn-primary">Save Settings</button>
			      	</div>
			      </div>
			    </form>
			  </div>
			</div>
		</div>
	</div>
</div>

@endsection

@push('js')

@endpush