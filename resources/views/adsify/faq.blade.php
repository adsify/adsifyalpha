@extends('layouts.nsmaster')

@section('body')
	<section class="main-content d-flex flex-column align-items-center">
      <div class="self-container d-flex flex-column page-text">
        <hr class="hr" />
        <h4>Frequently Asked Questions</h4>
        <p>
          This page contains the most asked questions in our platform and we think they will help you to undestand the platform
        </p>
        
        <h5>What is Adsify ?</h5>
    		<p>Adsify is The only online platform that collects Facebook advertisements from your competitors and organizes them in a useful platform, so you can see what types of deals or products your competitors are offering. Adsify provide you with a running tally of what your competitors are doing.</p>
            <p>Adsify Collect every advertisements of each page you have and keep you updated with the post stats and if they are still running or not.
            </p>

        <h5>Why it takes so long to collect ads ?</h5>
            <p>Once you add your page , Adsify will process the page and start fetching the page ads and delivering them to you as soon as possible, it may take 5-10 minutes depends on the how many ads the page has.</p>
            <p>Once its done the page status turns to "RUNNING" and you can check the collected ads in myads page.</p>
            <p>If the page has no ads, The page status will change to "NOADS" which means that no ads are running on this page.</p>
            
        <h5>What pages you can add ?</h5>
            <p>On Adsify you can add any page you want to follow their ads, just copy the link of the page and add it to Adsify.</p>
            <p>If you have a free plan, you only have {{config('adsify.free')}} pages to add if you need more feel free to subscribe its only ${{config('adsify.price')}} per month.</p>

    	<h5>What pages you can add ?</h5>
    		<p>On Adsify you can add any page you want to follow their ads, just copy the link of the page and add it to Adsify.</p>
            <p>If you have a free plan, you only have {{config('adsify.free')}} pages to add if you need more feel free to subscribe its only ${{config('adsify.price')}} per month.</p>

    	<h5>Does Adsify check the pages that has NO ADS ?</h5>
    		<p>Yes. Adsify check the pages that has no ads every day, to keep you updated. Once Adsify find a published ads, Adsify will give you the details of each ad.</p>
        
        <h5>How can i add a post to bookmarks ?</h5>
            <p>To add a post to your bookmarks, simply right click on the post and click add to favorites.</p>
      
        <h5>How To Subscribe to Adsify ?</h5>
    		<p>Once you register and verify your email, you will log-in automatically and you will see a subscribe button on the top right of the page on pc, or go subscription link after you click on settings button. Currently we have Paypal as the only payment method available.</p>

        <h5>You didn't receive your subscription ?</h5>
    		<p>Once you subscribe we will verify your payment, it may take few minutes but you will still benefit the premium plan of Adsify .If takes more than a day, please contact us.</p>

        <h5>Can i refund my subscription ?</h5>
        <p>If you have been a subscriber for more than 3 days, we have no right to refund your payment.</p>

        <h5>You have an other problem ?</h5>
        <p>Please contact us at : support@adsify.io</p>
      </div>
    </section>
@endsection