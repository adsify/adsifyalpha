@extends('layouts.nsmaster')

@section('body')
<section class="main-content d-flex flex-column align-items-center">
  <div class="self-container d-flex flex-column page-text">
	<hr class="hr" />
	<h4>Terms and Conditions</h4>

	<p>
	Please read these terms of use ("Agreement" or "Terms and Conditions") carefully before using the website and services offered by adsify.io (“Adsify”). The Agreement sets out the legally binding terms and conditions for your use of the website at adsify.io (the "Site") and all services provided by Adsify on the site.
	</p>
	<p>
	By using our website you agree to be legally bound by these terms, which shall take effect immediately on your first use of our website. If you do not agree to be legally bound by all the following terms please do not access and/or use our website.
	</p>

	<h5>
	Changes to the Terms of Use
	</h5>

	<p>
	We may revise and update these Terms of Use from time to time in our sole discretion. All changes are effective immediately when we post them, and apply to all use of the Website thereafter. Your continued use of the Website following the posting of revised Terms of Use means that you accept and agree to the changes. You are expected to check this page from time to time and when agreeing to any particular service or offer, so you are aware of any changes, as they are binding on you.
	</p>

	<h5>
	Electronic Communication
	</h5>

	<p>
	In using this website or sending us email, you are communicating with us electronically. You consent to receive communications from us electronically subject to the Privacy Policy. We will communicate with you by email or by posting notices on this site. You agree that all agreements, notices, disclosures and other communications that we provide to you electronically satisfy any legal requirement that such communications be in writing.
	</p>

	<h5>
	Registration and Use of Password
	</h5>

	<p>
	We may require each user to have a unique user name (Email) and password combination in order to access and use certain features or functions of the Site. Please read our Privacy Policy, which describes the personally identifiable information ("Personal Information") we collect, use, disclose, manage and store. As part of the registration process for the feature or function, you will choose a user name and password. Your user name and password are personal to you and you may not allow any others person(s) to use your user name or password under any circumstances. We are not liable for any harm caused or related to the theft or misappropriation of your user name or password, disclosure of your user name or password, or your authorization of anyone else to use your user name or password. You agree to notify us immediately if you become aware of or believe there is or may have been any unauthorized use of (or activity using) your user name or password or any other need to deactivate your user name or password due to security concerns.
	</p>
	<p>
	PASSWORDS ARE ISSUED ON A PERSONAL BASIS. ACCORDINGLY, ALL CONTENT OR INSTRUCTIONS TRANSMITTED BY OR RECEIVED FROM ANYONE PRESENTING YOUR PASSWORD ON THE SITE WILL BE DEEMED BINDING ON YOU. You agree that you are solely responsible and liable for all actions taken via your password, whether or not made with your knowledge or authority. You agree to guard your password carefully, with the full awareness that a failure to keep it secure will enable others to engage in transactions through the Site for which you will be legally responsible.
	</p>

	<h5>
	Intellectual Property and Copyright.
	</h5>

	<p>
	Adsify shall retain all Intellectual Property Rights in the software, tools, designs, documentation, data and any other material developed or provided by Adsify, including but not limited to the services and any data, text, pictures, sound, graphics, logos, marks, symbols, video, visual, oral or other digital material and any other content of any description.
	</p>
	<p>
	3rd party content displayed by Adsify remains property of the owner(s). In order to view this content, you must not be prohibited by the relevant 3rd party. Furthermore you must ensure that you have permission from the owner(s) before you reproduce, publish, transmit, distribute, display, modify, create derivative works from, sell or exploit in any way any of the Contents of this website.
	</p>
	<p>
	By using Adsify’s Services, you acknowledge and agree that Adsify owns all legal right, title and interest in, including any intellectual property rights which subsist in the site (whether those rights happen to be registered or not).
	</p>
	<p>
	Unless you have been expressly authorized to do so in writing by Adsify, you agree that in using the Adsify and Services, you will not use any trade mark, logo or content that is likely or intended to cause confusion about the owner or authorized user of these marks, names or logos. All content on Adsify is owned by Adsify and is protected by applicable intellectual property and proprietary rights and laws. Inquiries and permission requests may be sent to support@adsify.io.
	</p>

	<h5>
	Restrictions on Use
	</h5>

	<p>
	You may use this site for purposes expressly permitted by this site. As a condition of your use of Adsify’s website(s), you warrant to Adsify that you will not use the website(s) for any purpose that is unlawful or prohibited by these terms, conditions, and notices. For example, you may not (and may not authorize any party to) (i) co-brand this site, or (ii) frame this site, or (iii) download any content from this site (other than as provided by these terms) without the express prior written permission of an authorized representative of Adsify. For purposes of these Terms of Use, co-branding means to display a name, logo, trademark, or other means of attribution or identification of any party in such a manner as is reasonably likely to give a user the impression that such other party has the right to display, publish, or distribute this site or content accessible within this site. You agree to cooperate with Adsify to prevent or remedy any unauthorized use. In addition, you may not use Adsify’s website(s) in any manner which could disable, overburden, damage, or impair the website(s) or interfere with any other party’s use and enjoyment of the website(s). You may not obtain or attempt to obtain any materials, content, or information through any means not intentionally made available or provided for through the website(s).
	</p>

	<h5>
	Submissions Generally
	</h5>

	<p>
	All remarks, suggestions, ideas, graphics, discussions, chats, postings, transmissions, bulletin boards or other information communicated to Adsify through the Website (collectively, a "Submission") will forever be the property of Adsify. Adsify will treat any Submission as nonproprietary and non-confidential. By posting, uploading, inputting, providing or submitting your Submission you acknowledge that you and not Adsify have full responsibility for the message, including its legality, reliability, appropriateness, originality, and copyright. You further warrant and represent that you own or otherwise control all of the rights to your Submission as described in this section including, without limitation, all the rights necessary for you to provide, post, upload, input or submit the Submissions. Anything you transmit or post becomes the property of Adsify and may be used for any purpose, including, but not limited to, reproduction, disclosure, transmission, publication, broadcast, posting, or marketing.
	</p>
	<p>
	No compensation will be paid with respect to the use of your Submission, as provided herein. Adsify is under no obligation to post or use any Submission you may provide and may in Adsify sole discretion remove any Submission at any time, for any reason, without notice to you.
	</p>
	<p>
	Adsify may from time to time monitor Submissions on the Website however Adsify is under no obligation to do so. Adsify assumes no responsibility or liability arising from the content of any such Submissions, nor for any error, defamation, libel, slander, omission, falsehood, obscenity, pornography, profanity, danger, or inaccuracy contained in any information within such locations on the Website, including in Submissions. You are strictly prohibited from posting or transmitting any unlawful, threatening, libelous, defamatory, obscene, scandalous, inflammatory, pornographic, or profane material or any material that could constitute or encourage conduct that would be considered a criminal offense, give rise to civil liability, or otherwise violate any law. Adsify will fully cooperate with any law enforcement authorities or court order requesting or directing Adsify to disclose the identity of anyone posting any such information or materials.
	</p>

	<h5>
	Third Party Links
	</h5>

	<p>
	This website contains hyperlinks and other pointers to web sites operated by third parties ("Hyperlinked Websites"). Unless otherwise stated at those websites, those websites are not controlled by Adsify. By using those links, you acknowledge and agree that:
	</p>
	<p>
	Adsify is not responsible for the contents of any Hyperlinked Websites or any hyperlink contained in a Hyperlinked Website. Adsify provides these hyperlinks to you as a convenience only, and the inclusion of any link does not imply any endorsement of the Hyperlinked Website by Adsify, its related bodies corporate, affiliates, corporate partners or licensors. You access such Hyperlinked Websites entirely at your own risk.
	</p>

	<h5>
	Disclaimer of Warranties
	</h5>

	<p>
	ALL MATERIALS, INFORMATION, SOFTWARE, PRODUCTS, AND SERVICES INCLUDED IN OR AVAILABLE THROUGH THIS SITE (THE “CONTENT”) ARE PROVIDED “AS IS” AND “AS AVAILABLE” FOR YOUR USE. THE CONTENT IS PROVIDED WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. Adsify AND ITS AGENTS DO NOT WARRANT THAT THE CONTENT IS ACCURATE, RELIABLE OR CORRECT; THAT THIS SITE WILL BE AVAILABLE AT ANY PARTICULAR TIME OR LOCATION; THAT ANY DEFECTS OR ERRORS WILL BE CORRECTED; OR THAT THE CONTENT IS FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS. YOUR USE OF THIS SITE IS SOLELY AT YOUR RISK. BECAUSE SOME JURISDICTIONS DO NOT PERMIT THE EXCLUSION OF CERTAIN WARRANTIES, THESE EXCLUSIONS MAY NOT APPLY TO YOU.
	</p>

	<h5>
	Limitation of Liability
	</h5>

	<p>
	UNDER NO CIRCUMSTANCES SHALL Adsify OR ITS AGENTS BE LIABLE FOR ANY DIRECT, INDIRECT, PUNITIVE, INCIDENTAL, SPECIAL, OR CONSEQUENTIAL DAMAGES THAT RESULT FROM THE USE OF OR INABILITY TO USE, THIS SITE. THIS LIMITATION APPLIES WHETHER THE ALLEGED LIABILITY IS BASED ON CONTRACT, TORT, NEGLIGENCE, STRICT LIABILITY, OR ANY OTHER BASIS, EVEN IF Adsify HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. BECAUSE SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF INCIDENTAL OR CONSEQUENTIAL DAMAGES, Adsify’S LIABILITY IN SUCH JURISDICTIONS SHALL BE LIMITED TO THE EXTENT PERMITTED BY LAW.
	</p>

	<h5>
	Indemnity
	</h5>

	<p>
	Without limiting the operation of any other provisions of these Terms and Conditions of Use, you agree to indemnify Adsify and keep Adsify indemnified, holding Adsify harmless from all claims, demand, damages or loss (including legal costs on a full indemnity basis) incurred by Adsify as a result of, or flows naturally from, your breach of these Terms and Conditions of Use including without limitation the breach of any warranties.
	</p>

	<h5>
	Severability
	</h5>

	<p>
	If any provision of these Terms and Conditions shall be deemed unlawful, void or for any reason unenforceable, then that provision shall be deemed severable from these Terms and Conditions and shall not affect the validity and enforceability of any remaining provisions.
	</p>

	<h5>
	Subscription
	</h5>

	<p>
	Adsify uses PayPal as a primary payment method with a monthly subscription until a user cancel it from his side, we are not responsible if you did not cancel your subscription and we still get paid.
	</p>

	<p>
	Adsify has no right to refund your payments, once you subscribe it charges your account immediately.
	</p>

	<p>We reserve the right to modify or terminate the Adsify Subscription or your Account for any reason, without notice at any time.</p>

	<p>Fraud: Without limiting any other remedies, Adsify may suspend or terminate your Account if we suspect that you (by conviction, settlement, insurance or escrow investigation, or otherwise) have engaged in fraudulent activity in connection with the Site.</p>

	<h5>
	Your Questions, comments, or concerns.
	</h5>

	<p>
	Adsify welcomes your questions, comments and concerns about our Website and these Terms. Please send them to support@adsify.io .
	</p>
	
  </div>
</section>
	

@endsection


