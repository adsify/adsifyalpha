<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Adsify - Ecommerce Spy App on Facebook Pages and Ads</title>
    <meta name="description" content="Adsify is a Facebook spy app that collects Facebook ads from your competitors and organizes them in a useful feed, so you can see what types ecommerce products your facebook competitors are offering and that will help you improve your ecommerce store." />
    <link rel="canonical" href="https://adsify.io" />
  	<link rel="icon" type="image/png" sizes="16x16" href="{{asset('assets/favicon.ico')}}">
    <link rel="stylesheet" href="{{ asset('assets/font/gilroy/fonts.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/unlanding.css') }}">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-133284277-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-133284277-1');
    </script>

  </head>
  <body>
    <section class="header-wrapper d-flex flex-column align-items-center">
      <header class="header d-flex justify-content-between align-items-center">
        <a class="header__logo" href="{{url('/')}}"><img src="{{asset('assets/img/logo.svg')}}" alt="logo"/></a>
        <div class="d-flex aligh-items-center nav-m">
          <svg class="close-nav" style="width:30px;height:30px" viewBox="0 0 24 24">
            <path
              fill="#fff"
              d="M12,2C17.53,2 22,6.47 22,12C22,17.53 17.53,22 12,22C6.47,22 2,17.53 2,12C2,6.47 6.47,2 12,2M15.59,7L12,10.59L8.41,7L7,8.41L10.59,12L7,15.59L8.41,17L12,13.41L15.59,17L17,15.59L13.41,12L17,8.41L15.59,7Z"
            />
          </svg>
          <nav class="d-flex align-items-center">
            <ul class="d-flex navbar">
              <li><a class="nav_item" href="#about">About</a></li>
              <li><a class="nav_item" href="#features">Features</a></li>
              <li><a class="nav_item" href="#video">Video</a></li>
              <li><a class="nav_item" href="#pricing">Pricing</a></li>
              <li><a class="nav_item login" href="{{url('login')}}">Login</a></li>
            </ul>
          </nav>
          <a class="btn btn-header" href="{{url('register')}}">Try it Now</a>
        </div>
        <svg id="menu-act" style="width:40px;height:40px" viewBox="0 0 24 24">
          <path fill="#fff" d="M3,6H21V8H3V6M3,11H21V13H3V11M3,16H21V18H3V16Z" />
        </svg>
      </header>
      <div
        class="self-container header-content__main-wrapper d-flex justify-content-between align-items-center"
      >
        <div class="header-content__left-part d-flex flex-column">
          <h1><p>Compete Better</p><p>in Ecommerce</p><p>with <span>Adsify!</span></p></h1>
          <p class="header-content__s-text">
            Adsify is a New online platform that spy on facebook ads from your competitors
            and organizes them in a useful platform, so you can see what types of deals or products
            your competitors are offering. Adsify provide you with a running tally of what your
            competitors are doing.
          </p>
          <p class="header-content__t-text">
            No other platform does what Adsify do. Adsify will collect the advertisements
            running on pages you choose And provide you with a comprehensive guide on the Ads details like countries, Ad days running and if the ad is stopped or still running. Adsify provide you with a running tally of what your competitors are doing.
          </p>
          <a class="btn btn-first up" href="{{url('register')}}">Try it for Free</a>
        </div>
        <div class="header-content__right-part">
          <img class="main" src="{{asset('assets/img/Illustration.svg')}}" alt="Adsify facebook spy app" />
          <img class="woman" src="{{asset('assets/img/Woman.svg')}}" alt="Adsify facebook spy tool" />
          <img class="woman-hand" src="{{asset('assets/img/Fill_20.svg')}}" alt="Adsify facebook spy ads" />
          <img class="chart" src="{{asset('assets/img/Chart2.png')}}" alt="Adsify facebook spy app" />
          <img class="girl" src="{{asset('assets/img/Girl.svg')}}" alt="Adsify facebook spy app" />
          <img class="key" src="{{asset('assets/img/Key.svg')}}" alt="Adsify facebook spy app" />
          <img class="circ-r" src="{{asset('assets/img/Group_30.svg')}}" alt="Adsify facebook spy app" />
          <img class="circ" src="{{asset('assets/img/Group_30.svg')}}" alt="Adsify facebook spy app" />
          <img class="circ-r2" src="{{asset('assets/img/Group_30.svg')}}" alt="Adsify facebook spy app" />
          <img class="girl-search" src="{{asset('assets/img/Girl_Search.svg')}}" alt="Adsify facebook spy app" />
          <img class="search-loop" src="{{asset('assets/img/Search_loop.svg')}}" alt="Adsify facebook spy app" />
          <img class="search-loop-hand" src="{{asset('assets/img/girl_hand.svg')}}" alt="Adsify facebook spy app" />
          <img class="man" src="{{asset('assets/img/OM.svg')}}" alt="Adsify facebook spy app" />
          <img class="comment-box" src="{{asset('assets/img/comment_box.svg')}}" alt="Adsify facebook spy app" />
        </div>
      </div>
      <a class="btn btn-first un" href="{{url('register')}}">Try it for Free</a>
    </section>
    <section class="why-use d-flex flex-column align-items-center">
      <div class="self-container d-flex flex-column align-items-center">
        <hr class="hr hr-color" />
        <div class="play-price__header d-flex justify-content-between flex-wrap">
          <h4 class="play-price__head"><p>Why Use Adsify?</p></h4>
          <p class="play-price__text">
            Adsify Gives you a total freedom to spy on any facebook pages whithout hidding any product ads. Better than any other spy on the market. Adsify will help you to be a real competitor.
          </p>
        </div>
        <div>
          <div class="why-use__item-wrapper">
            <div class="why-use__item">
              <div class="why-use__item-first-img">
                <img class="main-det-img" src="{{asset('assets/img/Detail.svg')}}" alt="Adsify facebook spy app" />
                <img class="why-use__item-first-img-arrow1" src="{{asset('assets/img/Detailarrow1.svg')}}" alt="Adsify facebook spy app" />
                <img class="why-use__item-first-img-arrow2" src="{{asset('assets/img/Detailarrows2.svg')}}" alt="Adsify facebook spy app" />
                <img class="why-use__item-first-img-girl" src="{{asset('assets/img/Girl_and_Macbook.svg')}}" alt="Adsify facebook spy app" />
                <img class="why-use__item-first-img-girl-leg" src="{{asset('assets/img/Leg_R.svg')}}" alt="Adsify facebook spy app" />
              </div>
              <h5>Spy on competitors</h5>
              <p>
                Skim through the facebook ads to find the ones that compete with your products and services
                and take sufficient notes on the information that you have gathered, so it can be
                useful to your business strategy.
              </p>
            </div>
          </div>
          <div class="why-use__item-wrapper marg-top d-flex justify-content-end">
            <div class="why-use__item">
              <div class="why-use__item-second-img">
                <img class="main-det-img" src="{{asset('assets/img/IMG.svg')}}" alt="Adsify facebook spy app" />
                <img class="why-use__item-second-img-plant" src="{{asset('assets/img/Plant.png')}}" alt="Adsify facebook spy app" />
                <img class="why-use__item-second-img-hand" src="{{asset('assets/img/person-hand.svg')}}" alt="Adsify facebook spy app" />
              </div>
              <h5>Find Winning products easily</h5>
              <p>
                Know exactly when your competitors release new products. Track these ads to see when
                they start to become a winning product, so you can get started on a competing offer.
              </p>
            </div>
          </div>
          <div class="why-use__item-wrapper marg-top">
            <div class="why-use__item">
              <div class="why-use__item-thirth-img">
                <img class="main-det-img" src="{{asset('assets/img/IMG2.svg')}}" alt="Adsify facebook spy app" />
                <img class="why-use__item-second-img-plant" src="{{asset('assets/img/Plant.png')}}" alt="Adsify facebook spy app" />
                <img class="why-use__item-second-img-yoga" src="{{asset('assets/img/person-yoga.svg')}}" alt="Adsify facebook spy app" />
              </div>
              <h5>Saves Time</h5>
              <p>
                It takes a lot of time to check every Facebook page that competes with you, it can
                be very time consuming
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="what-adsify d-flex flex-column align-items-center">
      <div class="self-container d-flex flex-column align-items-center">
        <hr id="about" class="hr hr-color" />
        <h4 class="what-adsify__head">
          <p>What Adsify Does</p>
          <p>Different?</p>
        </h4>
        <div class="d-flex justify-content-between flex-wrap align-items-center">
          <ul class="what-adsify__left-block">
            <li class="what-adsify__left-block-item">
              <h5>Transparent</h5>
              <p>
                Adsify gives you full control of what you need to see, add pages and we will collect
                and display relative information from the ads they are running at the same moment.
              </p>
            </li>
            <li class="what-adsify__left-block-item">
              <h5>Informative</h5>
              <p>
                Learn about innovations your competitors are offering right away. Adsify offers the
                information you need to retain a competitive advantage in your market.
              </p>
            </li>
            <li class="what-adsify__left-block-item">
              <h5>Searchable</h5>
              <p>
                Adsify provides a database that is filterable, so you can easily find the
                competition and with the bookmarking option, you can access your saved ads easily.
              </p>
            </li>
            <li class="what-adsify__left-block-item">
              <h5>Affordable</h5>
              <p>
                Adsify saves you time and money, so you are not stuck for hours researching new ads
                by your competitors. This allows you to stretch your resources more effectively
                without losing productivity.
              </p>
            </li>
          </ul>
          <div class="what-adsify__right-block d-flex flex-column align-items-center">
            <div
              class="what-adsify__right-block-base d-flex justify-content-between align-items-center"
            >
              <img class="what-adsify__first" src="{{asset('assets/img/first-ic.svg')}}" alt="info" />
              <img src="{{asset('assets/img/second-icon.svg')}}" alt="info" />
              <img src="{{asset('assets/img/Search.svg')}}" alt="info" />
              <img src="{{asset('assets/img/Search_Copy.svg')}}" alt="info" />
            </div>
            <span class="what-adsify__shadow"><img src="{{asset('assets/img/logo-noci.svg')}}" alt="logo"/></span>
            <div class="what-adsify__person">
              <img class="what-adsify__person-m" src="{{asset('assets/img/Person.svg')}}" alt="person" />
              <img class="what-adsify__person-shoe" src="{{asset('assets/img/shoe.svg')}}" alt="shoe" />
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="help-client d-flex flex-column align-items-center">
      <div class="self-container d-flex flex-column align-items-center">
        <hr id="features" class="hr hr-color" />
        <h4 class="help-client__head">
          <p><span>We Help</span></p>
          <p>Our <span>Clients</span></p>
          <p>With Our Features</p>
        </h4>
        <div class="help-client__item-list d-flex flex-wrap justify-content-between">
          <div class="help-client__item d-flex flex-column align-items-start">
            <img src="{{asset('assets/img/1help.png')}}" alt="Facebook ads spy app" title="spy on facebook ads and pages" />
            <h5>Spy on Facebook Pages</h5>
            <p>
              Browse through a collection of competitor’s ads that really matter. Adsify allows you
              to choose the companies that are a direct threat to your business and presents
              competitor advertisements that really matter.
            </p>
          </div>
          <div class="help-client__item d-flex flex-column align-items-start">
            <img src="{{asset('assets/img/4help.png')}}" alt="best facebook ads exemples" title="find the best facebook ads exemples on adsify" />
            <h5>Ideal Thumbnails and Videos</h5>
            <p>
              When you add images and videos it can increase your message’s reach. However, this is
              only if you choose the right images and videos. Therefore, we use our expertise in the
              industry to suggest images and videos and even <strong>Videos Thumbnails</strong> that will reap the best rewards for your business.
            </p>
          </div>
          <div class="help-client__item d-flex flex-column align-items-start">
            <img src="{{asset('assets/img/2help.png')}}" alt="save the best ads posts" title="Adsisfy bookmarking" />
            <h5>Bookmark Favorite Ads</h5>
            <p>
              Save your favorite ads, so you can easily access them. It helps you keep track of the
              ads that are the most effective, so you can compete directly with your competitors.
            </p>
          </div>
          <div class="help-client__item d-flex flex-column align-items-start">
            <img src="{{asset('assets/img/5help.png')}}" alt="search winning product easy" title="find winning products easily" />
            <h5>Exposes Winning Products Quickly</h5>
            <p>
              Know exactly when your competitors release new products, because you see the ads when
              the consumers see them. Track these ads to see when they have come up with a winning
              product, so you can get started on a competing offer.
            </p>
          </div>
          <div class="help-client__item d-flex flex-column align-items-start">
            <img src="{{asset('assets/img/3help.png')}}" alt="analyze facebook ads" title="facebook ads analyze" />
            <h5>Provides In-Depth Analytics</h5>
            <p>
              Get the information you need about the ads that you will run, find the weak points and correct your target audience quickly to <strong>Compete Better</strong> and to provide your company with valuable information to create a competitive advantage.
            </p>
            <p>Also you can view <strong>Recommended Pages</strong> that shows you the pages similar to what you have added and that are running ads at the moment.</p>
          </div>
          <div class="help-client__item d-flex flex-column">
            <img src="{{asset('assets/img/6help.png')}}" alt="easy to use interface" />
            <h5>Effortless Interface</h5>
            <p>
              Adsify offers a simple and effortless interface that allows you to have everything
              that you need with just a few clicks. All of the information that you need is easy to
              access, so you can spend time improving your business and not collecting data about
              what your competitors are doing.
            </p>
          </div>
        </div>
      </div>
    </section>
    <section class="play-price d-flex flex-column align-items-center">
      <div id="video" class="play-video-wrapper d-flex justify-content-center align-items-center">
        <img src="{{asset('assets/img/video-bg.svg')}}" alt="Adsify video" />
        <!--a class="play-video__button d-flex flex-column align-items-center" href="#">
          <span class="round-button__wrapper"
            ><span class="round-button d-flex justify-content-center align-items-center"
              ><img src="{{asset('assets/img/Fill.svg')}}" alt="play"/></span
          ></span>
          <span class="play-video__text-button">PLAY</span>
        </a-->
      </div>
      <div class="self-container d-flex flex-column align-items-center">
        <hr id="pricing" class="hr hr-color" />
        <div class="play-price__header d-flex justify-content-between flex-wrap">
          <h4 class="play-price__head">
            <p>Simple</p>
            <p>And Fair Pricing</p>
          </h4>
          <p class="play-price__text">
            Adsify offers reasonable pricing that may alter from time to time depending on the
            market. However, all of our pricing is transparent, so there are no hidden fees and you
            will understand what you are paying for and what you are getting at all times.
          </p>
        </div>
        <div class="price-wrapper d-flex flex-wrap">
          <div class="price-item d-flex flex-column">
            <div class="price-item__header d-flex flex-column align-items-center">
              <div class="price-decor free"><img src="{{asset('assets/img/Safari-price.svg')}}" alt="Adsify facebook spy app" /></div>
              <p class="price-item__price-type free">Free</p>
              <p class="price-item__price">$0/month</p>
            </div>
            <div
              class="price-item__footer d-flex flex-column align-items-center justify-content-between"
            >
              <ul class="price-item__list">
                <li>{{config('adsify.trial')}} Days for Free</li>
                <li>Add Pages</li>
                <li>Consulting Ads</li>
                <li>Limited Features</li>
              </ul>
              <a class="btn btn-second" href="{{url('register')}}">Get Started</a>
            </div>
          </div>
          <div class="price-item price-item--marg d-flex flex-column">
            <div class="price-item__header d-flex flex-column align-items-center">
              <div class="price-decor premium">
                <img src="{{asset('assets/img/Safari-price2.svg')}}" alt="Adsify facebook spy app" />
              </div>
              <p class="price-item__price-type premium">Premium</p>
              <p class="price-item__price"><span>$75</span> ${{config('adsify.price')}}/month</p>
            </div>
            <div class="price-item__footer d-flex flex-column align-items-center justify-content-between">
              <ul class="price-item__list">
                <li>Add Unlimited Pages</li>
                <li>Manage Your Pages</li>
                <li>Access Recommended Pages</li>
                <li>Consulting Ads</li>
                <li>Updated Ads Hourly</li>
                <li>Filter Ads</li>
                <li>Bookmark Top Ads</li>
                <li>Detailed Ads</li>
                <li>Access Support Platform</li>
              </ul>
              <a class="btn btn-second" href="{{url('register')}}">Get Started</a>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="join-wrapper d-flex flex-column align-items-center">
      <div class="self-container d-flex flex-column align-items-center">
        <hr class="hr hr-color" />
        <h4 class="join-wrapper__head">
          <p>IS IT TOO LATE</p>
          <p>TO JOIN ADSIFY ?</p>
        </h4>
        <p class="join-wrapper__text">
          Adsify offers reasonable pricing that may alter from time to time depending on the market.
          However, all of our pricing is transparent, so there are no hidden fees and you will
          understand what you are paying for and what you are getting at all times.
        </p>
        <a href="{{url('register')}}" class="btn btn-first">Try it for Free</a>
      </div>
    </section>
    <footer class="main-footer__wrapper d-flex flex-column align-items-center">
      <div class="self-container">
        <section class="main-footer__content d-flex flex-column align-items-center">
          <p>Why waste thousands of dollars testing ads blindly?</p>
          <p>Find proven ads, innovate, <span class="mark-text">profit.</span></p>
          <ul class="main-footer__social-list d-flex align-items-center">
            <li>
              <a href="/"><img src="{{asset('assets/img/Facebook.svg')}}" alt="facebook"/></a>
            </li>
            <li>
              <a href="/"><img src="{{asset('assets/img/Twitter.svg')}}" alt="twitter"/></a>
            </li>
          </ul>
        </section>
        <hr class="hr" />
        <section
          class="main-footer__footer d-flex justify-content-between align-items-center flex-wrap"
        >
          <div class="main-footer__nav-section d-flex align-items-center flex-wrap flex1 ">
            <div class="main-footer__logo">
              <a href="/"><img src="{{asset('assets/img/logo.svg')}}" alt="Adsify logo"/></a>
            </div>
            <div class="main-footer__menu">
              <ul class="d-flex align-items-center main-footer__menu-list">
                <li><a class="main-footer__menu-item" href="{{url('faq')}}">FAQ.</a></li>
                <li><a class="main-footer__menu-item" href="{{url('policy')}}">Privacy & Policy.</a></li>
                <li><a class="main-footer__menu-item" href="{{url('/terms')}}">Terms & Conditions.</a></li>
              </ul>
            </div>
          </div>
          <div class="main-footer__copyright">
            <p>Copyright @2019 Adsify.io, All Rights Reserved</p>
          </div>
        </section>
      </div>
    </footer>
  <script type="text/javascript" src="{{asset('assets/js/bundle.js')}}"></script>
</body>
</html>