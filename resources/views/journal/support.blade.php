@extends('layouts.master')

@section('body')
	 <main class="default-transition" style="opacity: 1;">
        <div class="container-fluid">
            <div class="list disable-text-selection">
				<div class="row">
	                <!--div class="col-5 mb-4">
	                    <div class="card">
	                        <div class="card-header p-0 position-relative">
	                            <div class="position-absolute handle card-icon">
	                                <i class="simple-icon-shuffle"></i>
	                            </div>
	                        </div>
	                        <div class="card-body d-flex justify-content-between align-items-center">
	                            <h6 class="mb-0">Tickets Awaiting</h6>
	                            <div role="progressbar" class="progress-bar-circle position-relative" data-color="#922c88" data-trailcolor="#d7d7d7" aria-valuemax="100" aria-valuenow="64" data-show-percent="true">
	                            <svg viewBox="0 0 100 100" style="display: block; width: 100%;"><path d="M 50,50 m 0,-48 a 48,48 0 1 1 0,96 a 48,48 0 1 1 0,-96" stroke="#d7d7d7" stroke-width="4" fill-opacity="0"></path><path d="M 50,50 m 0,-48 a 48,48 0 1 1 0,96 a 48,48 0 1 1 0,-96" stroke="#922c88" stroke-width="4" fill-opacity="0" style="stroke-dasharray: 301.635, 301.635; stroke-dashoffset: 108.589;"></path></svg><div class="progressbar-text" style="position: absolute; left: 50%; top: 50%; padding: 0px; margin: 0px; transform: translate(-50%, -50%); color: rgb(146, 44, 136);">64%</div></div>
	                        </div>
	                    </div>
	                </div>
	                <div class="col-5 mb-4">
	                    <div class="card">
	                        <div class="card-header p-0 position-relative">
	                            <div class="position-absolute handle card-icon">
	                                <i class="simple-icon-shuffle"></i>
	                            </div>
	                        </div>
	                        <div class="card-body d-flex justify-content-between align-items-center">
	                            <h6 class="mb-0">Tickets Solved</h6>
	                            <div role="progressbar" class="progress-bar-circle position-relative" data-color="#922c88" data-trailcolor="#d7d7d7" aria-valuemax="100" aria-valuenow="75" data-show-percent="true">
	                            <svg viewBox="0 0 100 100" style="display: block; width: 100%;"><path d="M 50,50 m 0,-48 a 48,48 0 1 1 0,96 a 48,48 0 1 1 0,-96" stroke="#d7d7d7" stroke-width="4" fill-opacity="0"></path><path d="M 50,50 m 0,-48 a 48,48 0 1 1 0,96 a 48,48 0 1 1 0,-96" stroke="#922c88" stroke-width="4" fill-opacity="0" style="stroke-dasharray: 301.635, 301.635; stroke-dashoffset: 75.4088;"></path></svg><div class="progressbar-text" style="position: absolute; left: 50%; top: 50%; padding: 0px; margin: 0px; transform: translate(-50%, -50%); color: rgb(146, 44, 136);">75%</div></div>
	                        </div>
	                    </div>
	                </div-->
	                <div class="col-12 text-zero text-center mb-5">
			            <button type="button" data-toggle="modal" data-target="#addticket" class="btn btn-primary btn-lg mr-1">CREATE TICKET</button>
			        </div>
			        <div class="modal fade" id="addticket" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
			            <div class="modal-dialog" role="document">
			                <div class="modal-content">
			                	<form action="{{ url('journal/ticket/store') }}" method="post">
				                    <div class="modal-header">
				                        <h5 class="modal-title">CREATE NEW SUPPORT TICKET</h5>
				                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				                            <span aria-hidden="true">×</span>
				                        </button>
				                    </div>
				                    <div class="modal-body">
				                            {{ csrf_field() }}
				                            <div class="form-group">
				                                <label class="col-form-label" for="">Title</label>
				                                <input type="text" name="title" autocomplete="off" class="form-control">
				                            </div>
				                            <div class="form-group">
				                                <label class="col-form-label" for="">Category</label>
				                                <select name="category" class="form-control">
		                                            <option value="0" selected="selected">Choose...</option>
		                                            @if(isset($categories))
		                                            @foreach($categories as $cat)
		                                            <option value="{{$cat->id}}">{{$cat->name}}</option>
		                                            @endforeach
		                                            @endif
		                                        </select>
				                            </div>
				                            <!--div class="form-group">
				                                <label class="col-form-label" for="">Priority</label>
				                                <select name="priority" class="form-control">
		                                            <option value="0" selected="selected">Choose...</option>
		                                            <option value="1">Low</option>
		                                            <option value="2">Normal</option>
		                                            <option value="3">High</option>
		                                        </select>
				                            </div-->
				                            <input type="hidden" name="priority" value="2">
				                            <div class="form-group">
				                                <label class="col-form-label" for="">Message</label>
				                                <textarea name="message" class="form-control"></textarea>
				                            </div>
				                        
				                    </div>
				                    <div class="modal-footer">
				                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				                        <button type="submit" class="btn btn-primary">Add Ticket</button>
				                    </div>
			                    </form>
			                </div>
			            </div>
			        </div>
	            </div>
	            @if(isset($tickets))
	            <div class="row">
	                <div class="col-12 list">
	                	@foreach($tickets as $ticket)
	                    <div class="card d-flex flex-row mb-3">
	                        <div class="d-flex flex-grow-1 min-width-zero">
	                            <div class="card-body align-self-center d-flex flex-column flex-md-row justify-content-between min-width-zero align-items-md-center">
	                            	<div class="w-xs-100 mb-1">
	                            		@if($ticket->status_code == 601)
	                                    <span class="badge badge-pill badge-warning">{{strtoupper($ticket->status->name)}}</span>
	                                    @elseif($ticket->status_code == 602)
										<span class="badge badge-pill badge-success">{{strtoupper($ticket->status->name)}}</span>
	                                    @else
										<span class="badge badge-pill badge-danger">{{strtoupper($ticket->status->name)}}</span>
	                                    @endif
	                                </div>
	                                <p class="mb-1 text-muted text-small w-xs-100">{{$ticket->category->name}}</p>
	                                <p class="mb-1 text-muted text-small w-xs-100">{{$ticket->title}}</p>
	                                <p class="list-item-heading mb-1 truncate w-40 w-xs-100" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="{{$ticket->message}}">
		                                {{$ticket->message}}
	                                </p>
	                                <p class="mb-1 text-muted text-small w-15 w-xs-100">{{date('d M Y H:m', strtotime($ticket->updated_at))}}</p>
	                            </div>
				                <div class="pl-1 align-self-center pr-4">
				           			<a href="{{url('journal/ticket/'.$ticket->id)}}">Show</a>
				                </div>
	                        </div>
	                    </div>
	                    @endforeach
	                    <nav class="mt-4 mb-3">
	                        <ul class="pagination justify-content-center mb-0">
	                            {{ $tickets->render() }}
	                        </ul>
	                    </nav>
	                </div>
	            </div>
                @endif
            </div>
        </div>
    </main>



	
@endsection


@section('scripts')

@endsection
