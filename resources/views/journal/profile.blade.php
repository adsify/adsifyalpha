@extends('layouts.master')

@section('body')
    <main class="default-transition" style="opacity: 1;">
        <div class="container-fluid ">
            <div class="col-lg-6 col-md-12 col-sm-12">
            	<div class="card mb-4">
                    <div class="card-body">
                        <h5 class="mb-4">Profile</h5>
                        <form action="{{ url('journal/profile/update') }}" method="POST">
                        	@csrf
                        	<div class="form-group row">
                                <label class="col-lg-4 col-md-4 col-sm-4 col-form-label">First & Last name</label>
                                <div class="col-lg-4 col-md-4 col-sm-12 mb-1">
                                    <input type="text" class="form-control" id="first" value="{{$user->name}}" name="first" placeholder="First name">
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <input type="text" class="form-control" id="last" value="{{$user->last}}" name="last" placeholder="Last name">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-lg-4 col-md-4 col-sm-4 col-form-label">Email</label>
                                @if($user->updator == null)
                                <div class="col-lg-8 col-md-8 col-sm-12">
                                    <input type="email" class="form-control" id="email" name="email" value="{{$user->email}}" placeholder="Email" >
                                </div>
                                @else
                                <div class="col-lg-8 col-md-8 col-sm-12">
                                    <input type="email" class="form-control mb-2" id="email" name="email" value="{{$user->updator->email}}" disabled="true" placeholder="Email" >
                                    <span>Current email : {{$user->email}} <a href="{{url('/journal/profile/cancel')}}" class="badge badge-pill badge-primary position-relative ">Cancel Update</a></span>
                                    
                                </div>
                                <span class="badge badge-pill display-block badge-warning position-absolute badge-verify mt-2">Not Verified</span>
                                @endif
                            </div>
							
							<div class="form-group row" id="passtxt">
                                <label for="email" class="col-lg-4 col-md-4 col-sm-12 col-form-label">Password</label>
                                <div class="col-lg-8 col-md-8 col-sm-12">
                                	<button type="button" onclick="doit()" class="btn btn-secondary btn-xs mb-1">Change</button>
                                </div>
                            </div>

							<fieldset id="passform" style="display: none;">
								<div class="form-group row">
                                    <label class="col-lg-4 col-md-4 col-sm-12 col-form-label">Old Password</label>
                                    <div class="col-lg-8 col-md-8 col-sm-12">
                                        <input type="text" name="pass" class="form-control" placeholder="Old Password">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-4 col-md-4 col-sm-12 col-form-label">New Password</label>
                                    <div class="col-lg-8 col-md-8 col-sm-12">
                                        <input type="text" name="new" class="form-control"  placeholder="New Password" pattern="[A-Za-z0-9]{8,}" title="The password length should be more than 8 characters">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-4 col-md-4 col-sm-12 col-form-label">Confirm Password</label>
                                    <div class="col-lg-8 col-md-8 col-sm-12">
                                        <input type="text" name="confirm" class="form-control" placeholder="Confirm New Password">
                                    </div>
                                </div>
							</fieldset>
                           
                            <div class="form-group row mb-0">
                                <div class="offset-5 col-2">
                                    <button type="submit" class="btn btn-primary mb-0">Update now</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>

@endsection

@section('scripts')
	<script type="text/javascript">
		function doit(){
			$('#passtxt').hide();
			$('#passform').show();
		}
	</script>
@endsection