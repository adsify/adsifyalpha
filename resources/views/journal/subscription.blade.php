@extends('layouts.master')

@section('body')
    <main class="default-transition" style="opacity: 1;">
        <div class="container-fluid">
            <div class="list" data-check-all="checkAll">
                <div class="row">
                	@if($sub->status_code == 301 || $sub->status_code == 304)
                    <div class="col-lg-12 col-md-12 col-sm-12 mb-4">
                        <div class="card mb-4 subscription">
                        	<div class="what-adsify__right-block d-flex flex-column align-items-center">
					            <div class="what-adsify__right-block-base d-flex justify-content-between align-items-center">
					              <img class="what-adsify__first" src="{{asset('assets/img/first-ic.svg')}}" alt="info">
					              <img src="{{asset('assets/img/second-icon.svg')}}" alt="info">
					              <img src="{{asset('assets/img/Search.svg')}}" alt="info">
					              <img src="{{asset('assets/img/Search_Copy.svg')}}" alt="info">
					            </div>
					            <div class="what-adsify__person">
					              <img class="what-adsify__person-m" src="{{asset('assets/img/Person.svg')}}" alt="person">
					              <img class="what-adsify__person-shoe" src="{{asset('assets/img/shoe.svg')}}" alt="shoe">
					            </div>
				            </div>
                            <div class="card-body pt-2 mt-3">
                            	<div class="w-40 w-xs-100 w-md-100 m-auto text-left" >
                            		<p class="text-center title text-white">Adsify Premium</p>
	                                <p class="mb-3 text-white">
		                                Adsify Premium Version gives you more options and whole different experience with a lot of new features :
		                            </p>
		                            <p class="feature">Add Unlimited Pages.</p>
		                            <p class="feature">Manage Your Pages.</p>
		                            <p class="feature">Access Recommended Pages.</p>
		                            <p class="feature">Consulting Ads.</p>
		                            <p class="feature">Updated Ads Hourly.</p>
		                            <p class="feature">Filter Ads.</p>
		                            <p class="feature">Bookmark Top Ads.</p>
		                            <p class="feature">Detailed Ads.</p>
		                            <p class="feature">Access Support Platform.</p>
		                            <p class="feature">Access Night/Light Mode.</p>
                            	</div>
                            	
                            </div>
                            <div class="card-footer text-center">
                            	<form action="{{url('journal/subscription/subscribe')}}" method="POST">
                    				@csrf
                    				<input type="hidden" id="paypal" name="payment" value="paypal" class="custom-control-input" checked="">
                                	<button type="submit" id="sub"class="btn btn-sub mb-1">SUBSCRIBE NOW</button>
                                </form>
                                <p class="text-white">
									Price : ${{config('adsify.price')}}.00 / Month
								</p>
                            </div>
                        </div>
                    </div>
                    @else
						<div class="col-lg-8 col-md-12 col-sm-12">
	                        <div class="card d-block mb-4">
	                            <div class="card-body">
	                                <h5 class="card-title">Subscription</h5>
									<div class="card mb-4 progress-banner">
				                        <div class="card-body pb-1 pt-1 justify-content-between d-flex row align-items-center">
				                            <div class="col-lg-8 col-md-6 col-sm-12 m-text-center">
				                                <i class="iconsmind-Credit-Card2 mr-2 text-white align-text-bottom d-inline-block"></i>
				                                <div>
				                                    <p class="lead text-white">
			                                    	@if($sub->status_code == 301)
			                                    		{{strtoupper('Free Trial')}}
			                                    	@elseif($sub->status_code == 302)
			                                    		{{strtoupper('Premium')}}
			                                    	@elseif($sub->status_code == 303)
			                                    		{{strtoupper('Locked')}}
			                                    	@elseif($sub->status_code == 304)
			                                    		{{strtoupper('None')}}
			                                    	@endif
				                                    </p>
				                                    <p class="text-small text-white">Current plan</p>
				                                </div>
				                            </div>
				                            <div class="col-lg-4 col-md-6 col-sm-12 text-center">
				                            	@if($sub->status_code == 301)
				                            	<form action="{{url('journal/subscription/subscribe')}}" method="POST">
		                            				@csrf
		                            				<input type="hidden" id="paypal" name="payment" value="paypal" class="custom-control-input" checked="">
				                                	<button type="submit" id="sub"class="btn text-white btn-success mb-1">SUBSCRIBE NOW</button>
				                                </form>
				                                @elseif($sub->status_code == 302)
				                                	@if($sub->service == "cancel")
														<form action="{{url('journal/subscription/subscribe')}}" method="POST">
				                            				@csrf
				                            				<input type="hidden" id="paypal" name="payment" value="paypal" class="custom-control-input" checked="">
						                                	<button type="submit" id="sub"class="btn text-white btn-success mb-1">RE-SUBSCRIBE</button>
						                                </form>
				                                	@else
														<button type="button" onclick="cancel()" class="btn text-white btn-danger mb-3">CANCEL SUBSCRIPTION</button>
														@if($sub->last_payment)
														<p class="text-white">Last Payment : {{date('d M Y', strtotime(\Carbon\Carbon::parse($sub->last_payment)))}}</p>
														@endif
				                                	@endif
												@elseif($sub->status_code == 303)
												<p class="text-white">
													Payment Failed, please contact support.
												</p>
				                                @endif
												
												@if($sub->service == "pending")
												<p class="text-white">
													Payment on verification..
												</p>
												@elseif($sub->service == "cancel")
												<p class="text-white">Current subscription will end at :<br> 
						                                	{{date('d M Y', strtotime(\Carbon\Carbon::parse($sub->last_payment)->addMonth()))}}
						                                </p>
												@else
												<p class="text-white">
													Price : ${{config('adsify.price')}}.00 / Month
												</p>
												@endif
				                            </div>
				                        </div>
				                    </div>
	                            </div>
	                        </div>
	                    </div>
						@if(isset($payments))
	                    <div class="col-lg-4 col-md-12 col-sm-12">
	                    	@if(sizeof($payments)>0)
	                    	<h4>Payments history</h4>
	                    	@endif
	                    	<div class="col-12 list" >
	                    		@foreach($payments as $payment)
			                    <div class="card d-flex flex-row mb-2">
			                        <div class="d-flex flex-grow-1 min-width-zero">
			                            <div class="card-body align-self-center d-flex flex-column flex-md-row justify-content-between min-width-zero align-items-md-center">
			                                <p class="list-item-heading mb-1 truncate w-40 w-xs-100">
			                                    #{{$payment->txn_id}}
			                                </p>
			                                <p class="mb-1 text-muted text-small w-15 w-xs-100">{{strtoupper($payment->service)}}</p>
			                                <p class="mb-1 text-muted text-small w-15 w-xs-100">${{strtoupper($payment->amount)}}.00</p>
			                                <p class="mb-1 text-muted text-small w-20 w-xs-100">{{ date('d M Y', strtotime($payment->created_at))}}</p>
			                                <div class="w-10 w-xs-100">
			                                	@if($payment->status_code == 701)
			                                    <span class="badge badge-pill badge-success">Paid</span>
			                                    @elseif($payment->status_code == 703)
												<span class="badge badge-pill badge-danger">Failed</span>
												@endif
			                                </div>
			                            </div>
			                        </div>
			                    </div>
			                    @endforeach
			                </div>
	                    </div>
	                    @endif
					@endif
                    
                </div>
            </div>
        </div>
    </main>

@endsection


@section('scripts')
	<script type="text/javascript">
		function doit(){
			$('#sub').click();
		}

		function cancel(){
			Swal({
			  title: 'Are you sure?',
			  text: "Your subscription will be canceled, but you will still benefit the current paid subscription!",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Yes, Cancel it!',
			  cancelButtonText: 'No'
			}).then((result) => {
			  if (result.value) {
			  	$.get(window.Laravel.url+'/journal/subscription/cancel', function(data) {
			     	
				});
				location.reload();
			  }
			})
		}
	</script>


@endsection