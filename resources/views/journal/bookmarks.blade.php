@extends('layouts.master')

@section('body')
    <main class="default-transition" style="opacity: 1;">
        <div class="container-fluid">
        	@if(count($posts) > 0)
				@include('partials.journal.post')
        	@else
				<div class="text-center">
					<h2>You have no bookmarks yet, want to add one ? Right Click on the posts in your journal</h2>
					<div class="welcome shadow">
						<img src="{{asset('assets/img/bookmark_home.png')}}">
					</div>
				</div>
        	@endif
        </div>
    </main>

@endsection

@section('scripts')
<script type="text/javascript">
	var bricklayer = new Bricklayer(document.querySelector('.bricklayer'));
	function unsave(id){
		Swal({
		  title: 'Are you sure you want to unsave this post?',
		  text: "You may not be able to find this post again!",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, unsave it!'
		}).then((result) => {
		  if (result.value) {
		  	$.get(window.Laravel.url+"/journal/bookmarks/cancel/"+id, function(res){
	            if(res == '1'){
	              Swal(
				      'Okay!',
				      'Your post is unsaved.',
				      'success'
				    );
	              location.reload();
	            }else{
	              Swal(
				      'Ops!',
				      'Something went wrong.',
				      'error'
				    );
	            }
	          });
		  }
		});
	}
	

</script>

@endsection