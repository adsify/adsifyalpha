@extends('layouts.master')

@section('body')
	 <main>
        <div class="container-fluid">
            <div class="mobile-show position-relative" style="top:-12px;">
                <div class="card mb-3">
                    <div class="position-absolute pt-1 pr-2 r-0">
                        <span class="text-extra-small text-muted">{{date('d-m-Y', strtotime($ticket->created_at))}}</span>
                    </div>
                    <div class="card-body p-3">
                        <div class="d-flex flex-row pb-2">
                            <div class=" d-flex flex-grow-1 min-width-zero">
                                <div class="pl-0 align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero">
                                    <div class="min-width-zero">
                                        <p class="mb-0 truncate list-item-heading">Title : {{$ticket->title}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr class="m-0 mb-2">
                        <div class="pl-2 chat-div">
                            <div class="position-absolute r-0 pr-3">
                                @if($ticket->status_code == 601)
                                <span class="badge badge-pill badge-warning">{{strtoupper($ticket->status->name)}}</span>
                                @elseif($ticket->status_code == 602)
                                <span class="badge badge-pill badge-success">{{strtoupper($ticket->status->name)}}</span>
                                @else
                                <span class="badge badge-pill badge-danger">
                                {{strtoupper($ticket->status->name)}}</span>
                                @endif
                            </div>
                            <p class="mb-0 text-default">
                                Category : {{$ticket->category->name}}
                            </p>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="row app-row">
                <div class="col-12 chat-app">
                    <div class="scroll">
                        <div>
						@foreach($comments as $comment)
                            <div class="card d-inline-block mb-3 @if($comment->user_id == $ticket->user_id) float-right @else float-left @endif">
                                <div class="position-absolute pt-1 pr-2 r-0">
                                    <span class="text-extra-small text-muted">{{$comment->created_at}}</span>
                                </div>
                                <div class="card-body p-3">
                                    <div class="d-flex flex-row pb-2">
                                        <div class=" d-flex flex-grow-1 min-width-zero">
                                            <div class="pl-0 align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero">
                                                <div class="min-width-zero">
                                                    <p class="mb-0 truncate list-item-heading">{{strtoupper($comment->user->name) }}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr class="m-0 mb-2">
                                    <div class="pl-2 chat-div">
                                        <p class="mb-0 text-default">
                                            {{$comment->comment}}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="app-menu">
            <div class="p-4">
                <div class="text-center pb-3 border-bottom mb-3">
                    <h4>Ticket details</h4>
                </div>
                
                <div class="d-flex flex-row mb-1 border-bottom pb-3 mb-3">
                    <div class="d-flex flex-grow-1 min-width-zero">
                        <div class="pl-0 align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero">
                            <div class="min-width-zero">
                                <p class=" mb-0 truncate">Title : {{$ticket->title}}</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="d-flex flex-row mb-1 border-bottom pb-3 mb-3">
                    <div class="d-flex flex-grow-1 min-width-zero">
                        <div class="pl-0 align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero">
                            <div class="min-width-zero">
                                <p class=" mb-0 truncate">Category : {{$ticket->category->name}}</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="d-flex flex-row mb-1 border-bottom pb-3 mb-3">
                    <div class="d-flex flex-grow-1 min-width-zero">
                        <div class="pl-0 align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero">
                            <div class="min-width-zero">
                                <p class=" mb-0 truncate">Created at : {{date('d-m-Y', strtotime($ticket->created_at))}}</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="w-xs-100 text-center">
                    @if($ticket->status_code == 601)
                    <span class="badge badge-pill badge-warning">{{strtoupper($ticket->status->name)}}</span>
                    @elseif($ticket->status_code == 602)
                    <span class="badge badge-pill badge-success">{{strtoupper($ticket->status->name)}}</span>
                    @else
                    <span class="badge badge-pill badge-danger">
                    {{strtoupper($ticket->status->name)}}</span>
                    @endif
                </div>
               
            </div>
        </div>
        @if($ticket->status_code == 601)
		<form action="{{url('journal/ticket/comment')}}" method="post">
        	<div class="chat-input-container d-flex justify-content-between align-items-center ">
        		@csrf
        		<input type="hidden" name="ticket_id" value="{{ $ticket->id }}">
	            <input class="form-control flex-grow-1" type="text" autocomplete="off" name="comment" required="" placeholder="Tell us your problem, we will help you...">
	            <div>
	                <!--button type="button" class="btn btn-outline-primary icon-button large">
	                    <i class="simple-icon-paper-clip"></i>
	                </button-->
	                <button type="submit" class="btn btn-primary icon-button large">
	                    <i class="simple-icon-arrow-right"></i>
	                </button>

	            </div>
        	</div>
    	</form>
        @endif
    </main>
@endsection


@section('scripts')

@endsection
