@extends('layouts.master')

@section('body')
    <main class="default-transition" style="opacity: 1;">
        <div class="container-fluid">
            <div class="row" >
			    <div class="col-12">
			        <ul class="nav nav-tabs separator-tabs ml-0 mb-3" role="tablist">
			            <li class="nav-item">
			                <a class="nav-link active show" id="pages-tab" data-toggle="tab" href="#pages" role="tab" aria-controls="pages" aria-selected="true">Pages</a>
			            </li>

			            <li class="nav-item">
			                <a class="nav-link" id="archived-tab" data-toggle="tab" href="#archived" role="tab" aria-controls="archived" aria-selected="false">Archived</a>
			            </li>

			            <!--li class="nav-item">
			                <a class="nav-link" id="featured-tab" data-toggle="tab" href="#featured" role="tab" aria-controls="featured" aria-selected="false">Featureds</a>
			            </li-->

			            <li class="nav-item">
			                <a class="nav-link" id="similars-tab" data-toggle="tab" href="#similars" role="tab" aria-controls="similars" aria-selected="false">Recommended Pages</a>
			            </li>
			        </ul>
			        <div class="@if(sizeof($pages) > 0) float-sm-right @endif text-center text-zero mb-3">
						@if(Auth::user()->subscription->status_code == 301)
			        	<p class="mb-0">Pages available : {{config('adsify.free')-count($pages)}}</p>
			        	@endif
			            <button type="button" data-toggle="modal" data-target="#addpage" class="btn btn-primary btn-lg mr-1">ADD NEW PAGE</button>
			        </div>
			        
			        
			        <div class="modal fade" id="addpage" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
			            <div class="modal-dialog" role="document">
			                <div class="modal-content">
			                    <div class="modal-header">
			                        <h5 class="modal-title" id="exampleModalContentLabel">Add New Page</h5>
			                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			                            <span aria-hidden="true">×</span>
			                        </button>
			                    </div>
			                    <form action="{{ url('/journal/store') }}" method="post" id="addForm">
			                        <div class="modal-body">
			                            @if(Auth::user()->subscription->service != 'archive')
			                            {{ csrf_field() }}
			                            @endif
			                            <div class="form-group @if($errors->get('link')) has-error @endif">
			                            	
			                                <label class="col-form-label" for="">Page link</label>
			                                <div class="input-group mb-3">
			                                    <div class="input-group-prepend">
			                                        <span class="input-group-text" id="basic-addon3">https://www.facebook.com/</span>
			                                    </div>
			                                    <input type="text" id="link" name="link" autocomplete="off" value="{{ old('link') }}" required class="form-control">
			                                    @if($errors->get('link'))
			                                        <ul class="list-unstyled">
			                                        @foreach($errors->get('link') as $error)
			                                            <li>{{ $error }}</li>
			                                        @endforeach
			                                        </ul>
			                                    @endif
			                                </div>
			                                <div class="text-center">
			                            		<img src="{{asset('assets/img/tuto.jpg')}}" style="width: 100%;">
			                            	</div>
			                            </div>
			                        </div>
			                        <div class="modal-footer p-3">
			                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			                            @if(Auth::user()->subscription->service != 'archive')
			                            <button type="submit" id="btnAdd" class="btn btn-primary">Add Page</button>
			                            @else
			                            <button type="button" onclick="gosubscribe('pages')" class="btn btn-primary">Add Page</button>
			                            @endif
			                        </div>
			                    </form>
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
			
			<div class="tab-content" id="display" >
			    <div class="tab-pane active show" id="pages" role="tabpanel" aria-labelledby="pages-tab">
		    	@if(isset($pages))
			    	@if(sizeof($pages) == 0) 
					<div class="welcome shadow">
						<img src="{{asset('assets/img/pages.png')}}">
					</div>
					@else
			    	<div class="ml-3">
			    		<div class="collapse d-md-block">
	                        <span class="mr-3 mb-4 d-inline-block ">
	                        	<span class="mr-3">Select : </span>
	                        	<a href="#" @click="getSelect('all')" class="mr-2 view-icon">
                                    <span class="badge badge-pill badge-outline-primary text-revert" style="border-color: #00A9FF;">All</span>
	                            </a>
	                            <a href="#" @click="getSelect('101')" class="mr-2 view-icon">
                                    <span class="badge badge-pill badge-outline-primary text-revert" style="border-color: #EA8F14;" data-toggle="tooltip" data-placement="top" title="Pages that are processing in the background">Processing</span>
	                            </a>
	                            <a href="#" @click="getSelect('102')" class="mr-2 view-icon">
	                            	<span class="badge badge-pill badge-outline-primary text-revert" style="border-color: #2bc437;" data-toggle="tooltip" data-placement="top" title="Pages thats not running ads at the moment">Running</span>
									
	                            </a>
	                            <a href="#" @click="getSelect('103')" class="mr-2 view-icon active">
	                                <span class="badge badge-pill badge-outline-primary text-revert" style="border-color: #F11712;" data-toggle="tooltip" data-placement="top" title="Pages has ads running at the moment.">No Ads</span>
	                            </a>
	                        </span>
	                        <div class="float-md-right">
	                        	<p>Recommendation : Please add pages that are necessary for better experience in Adsify and Archive the NoAds Pages. Thank you!</p>
	                        </div>
	                    </div>
			    	</div>
			        <div class="row list">
			            <div class="col-lg-4 col-xl-3 col-md-6 col-sm-12 mb-4" v-for="page in select">
			                <div class="card page">
			                	<div class="card-header pt-3">
			                		 <div class="text-center">
			                            <img :src="url + '/' + page.logo_src" class="img-thumbnail border-0 rounded-circle list-thumbnail align-self-center mb-3 shadow">
			                            <h3 class="title">@{{"@"+page.name}}</h3>
			                        </div>
			                	</div>
			                	<hr>
			                	<div class="status" v-bind:style="{ 'box-shadow': '0 0 50px 18px'+ getColor(page.status_code) +', -10px 0 80px 0px '+ getColor(page.status_code) +', 10px 0 80px 0px '+ getColor(page.status_code) }" ></div>
			                    <div class="card-body pb-2 pt-3">
			                        <div class="row text-center">
			                            <span class="col-4 border-right">
			                                <p class="text-muted text-small mb-2">Total Ads :</p>
			                                <p class="mb-3">
			                                    @{{ page.posts.length }}
			                                </p>
			                            </span>
			                            <span class="col-4 border-right">
			                                <p class="text-muted text-small mb-2">Status :</p>
			                                <p class="mb-3">
			                                    <span class="badge badge-pill badge-outline-primary text-revert" v-bind:style="{ 'border-color': getColor(page.status_code)}" data-toggle="tooltip" data-placement="top">@{{page.status.name.toUpperCase()}}</span></p>
			                            </span>
			                            <span class="col-4">
			                                <p class="text-muted text-small mb-2">Total Countries :</p>
			                                <p class="mb-3">@{{page.size}}</p>
			                            </span>
			                        </div>
			                    </div>
			                    <div class="card-footer" style="overflow: hidden;">
			                        <div class="row">
			                            <div class="col-4 text-left">                 
			                                <a target="_blank" v-bind:href="window.Laravel.norefer+'http://www.facebook.com/'+page.id" class="btn btn-outline-info btn-sm mb-0 text-revert"><span class="">View</span></a>
			                            </div>
			                            <div class="col-4 text-center">
			                            	@if(Auth::user()->subscription->status_code == 302)
			                                <form action="{{url('/journal/filter')}}" method="post">
			                                    @csrf
			                                    <input type="hidden" name="page" :value="page.id">
			                                    <button type="button" v-if="page.posts.length == 0" disabled class="btn btn-outline-primary disabled btn-sm mb-0 text-revert"><span class="">Posts</span></button>
			                                    <button type="submit" v-else="" class="btn btn-outline-primary  btn-sm mb-0 text-revert"><span class="">Posts</span></button>
			                                </form>
			                                @else
												<button type="button" class="btn btn-outline-primary btn-sm mb-0 text-revert"><span class="">Posts</span></button>
			                                @endif
			                            </div>
			                            <div class="col-4 text-right">
			                                <form action="{{url('/journal/page/archive')}}" method="post">
			                                @if(Auth::user()->subscription->status_code == 302)
			                                    {{csrf_field()}}
			                                    <input type="hidden" :value="page.id" name="id">
			                                    <button type="submit" class="btn btn-outline-danger btn-sm mb-0 text-revert"><span class="">Archive</span></button>
			                                @else
			                                    <button type="button" onclick="gosubscribe('pages')" class="btn btn-outline-danger btn-sm mb-0 text-revert"><span class="">Archive</span></button>
			                                @endif
			                                </form>
			                            </div>
			                        </div>
			                    </div>
			                </div>
			            </div>
			        </div>
			        @endif
		        @endif
			    </div>
			    
			    <div class="tab-pane" id="archived" role="tabpanel" aria-labelledby="archived-tab">
			        @if(isset($archives))
			        <div class="row list">
			            @foreach($archives as $page)
			            <div class="col-lg-4 col-xl-3 col-md-6 col-sm-12 mb-4">
			                <div class="card page">
			                	<div class="card-header pt-3">
			                		 <div class="text-center">
			                            <img src="{{ url($page->logo_src) }}" alt="{{$page->name}}" class="img-thumbnail border-0 rounded-circle list-thumbnail align-self-center mb-3 shadow">
			                            <h3 class="title">{{'@'.$page->name}}</h3>
			                        </div>
			                	</div>
			                	<div class="status" style="box-shadow:0 0 50px 18px #EA8F14, -10px 0 80px 0px #EA8F14, 10px 0 80px 0px #EA8F14;"></div>
			                    <hr>
			                    <div class="card-body pb-2 pt-3">
			                        
			                        <div class="row text-center">
			                            <span class="col-6 border-right">
			                                <p class="text-muted text-small mb-2">Total Ads :</p>
			                                <p class="mb-3">
			                                    {{ sizeof($page->posts) }}
			                                </p>
			                            </span>
			                            <span class="col-6">
			                                <p class="text-muted text-small mb-2">Status :</p>
			                                <p class="mb-3">
			                                    <span class="badge badge-pill badge-outline-warning text-revert" data-toggle="tooltip" data-placement="top" title="{{$page->status->hint}}">{{strtoupper($page->status->name)}}</span></p>
			                            </span>
			                        </div>
			                    </div>
			                    <div class="card-footer" style="overflow: hidden;">
			                        <div class="row">
			                            <div class="col-12 text-center">
			                                <form action="{{url('/journal/page/unarchive')}}" method="post">
			                                @if(Auth::user()->subscription->status_code == 302)
			                                    {{csrf_field()}}
			                                    <input type="hidden" value="{{$page->id}}" name="id">
			                                    <button type="submit" class="btn btn-warning btn-sm mb-0 "><i class="xicon unarchivebtn"></i><span class="ml-1 text-white">UNARCHIVE</span></button>
			                                @else
			                                    <button type="button" onclick="gosubscribe('pages')" class="btn btn-warning btn-sm mb-0"><i class="xicon unarchivebtn"></i><span class="ml-1 text-white">UNARCHIVE</span></button>
			                                @endif
			                                </form>
			                            </div>
			                        </div>
			                    </div>
			                </div>
			            </div>
			            @endforeach
			        </div>
			        <div class="clearfix"></div>
		            <nav class="mt-4 mb-3">
		                <ul class="pagination justify-content-center mb-0">
		                    {{ $archives->render() }}
		                </ul>
		            </nav>
		            @endif
			    </div>

			    <div class="tab-pane" id="featured" role="tabpanel" aria-labelledby="featured-tab">
			        @if(sizeof($featureds) > 0)
			        <div class="text-center mb-3">
			        	<h4 class="justify-content-center">Every day, we will update you with new pages!</h4>
			        </div>
			        <div class="row list">
			        	@foreach($featureds as $featured)
						<div class="col-lg-4 col-xl-3 col-md-6 col-sm-12 mb-4">
			                <div class="card page">
			                	<div class="card-header pt-3">
			                		 <div class="text-center">
			                            <img src="{{ url($featured->logo_src) }}" alt="{{$featured->name}}" class="img-thumbnail border-0 rounded-circle list-thumbnail align-self-center mb-3 shadow">
			                            <h3 class="title">{{'@'.$featured->name}}</h3>
			                            <div class="br-wrapper br-theme-bootstrap-stars">
				                        	<div class="br-widget br-readonly">
				                        		@for ($i = 0; $i < 5; $i++)
				                        			@if($i < intval($featured->chance))
				                        				<a href="#" class="br-selected"></a>
				                        			@else
														<a href="#"></a>
				                        			@endif
				                        		@endfor
				                        	</div>
			                        	</div>
			                        </div>
			                	</div>
			                	<div class="status" style="top: calc(50% - 15px);"></div>
			                    <hr>
			                    <div class="card-body pb-2 pt-3">
			                        <div class="row text-center">
			                            <span class="col-6 border-right">
			                                <p class="text-muted text-small mb-2">Running Ads</p>
			                                <p class="mb-3">
			                                    {{ $featured->ads }}
			                                </p>
			                            </span>
			                            <span class="col-6">
			                                <p class="text-muted text-small mb-2">Page Likes</p>
			                                <p class="mb-3">
			                                    {{ $featured->likes }}
			                                </p>
			                            </span>
			                        </div>
			                    </div>
			                    <div class="card-footer" style="overflow: hidden;">
			                        <div class="row">
			                        	<div class="col-4 text-center">
			                                <a target="_blank" href="{{config('adsify.norefer')}}http://www.facebook.com/{{$featured->id}}" class="btn btn-outline-info btn-sm mb-0 text-revert"><span class="">View</span></a>
			                            </div>
			                            <div class="col-4 text-center">
			                                <form action="{{url('/journal/store')}}" method="post">
			                                    {{csrf_field()}}
			                                    <input type="hidden" value="{{$featured->id}}" name="link">
			                                    <input type="hidden" value="{{$featured->name}}" name="name">
			                                    <button type="submit" class="btn btn-outline-primary btn-sm mb-0 text-revert"><span class="ml-1">Add Page</span></button>
			                                </form>
			                            </div>
			                            <div class="col-4 text-center">
			                                <a target="_blank" href="https://{{$featured->shop}}" class="btn btn-outline-info btn-sm mb-0 text-revert"><span class="">Store</span></a>
			                            </div>
			                        </div>
			                    </div>
			                </div>
			            </div>
			            @endforeach
			        </div>
			        @else
			        <div class="text-center">
			        	<h1 class="justify-content-center">Coming Soon..</h1>
			        </div>
		            @endif
			    </div>

			    <div class="tab-pane" id="similars" role="tabpanel" aria-labelledby="similars-tab">
			        <div class="text-center">
			        	<h1 class="justify-content-center">Coming Soon..</h1>
			        </div>
			        <!--div class="text-center" v-if="key == ''">
			        	<h3 class="justify-content-center">Select A Page</h1>
			        </div-->
			        @if(sizeof($pages) == -1) 
			        <div class="text-center">
			        	<div class="d-flex w-30 w-xs-100 m-auto">
				        	<select class="form-control select2-single select2-hidden-accessible" name="similar" tabindex="-1" aria-hidden="true" @change="getSimilars()" v-model="key">
				        		@foreach($pages as $page)
				        			@if($page->status_code != 101)
									<option value="{{$page->id}}">{{$page->name}}</option>
									@endif
				        		@endforeach
	                        </select>
				        </div>
			        </div>
			        @endif

			        <div class="col-md-12 text-center mt-4" v-if="similars.length == 0 && key != ''">
			        	<h1>No Similar Pages Collected Yet..</h1>
	        		</div>
			        <div class="row list mt-4" >
						<div class="col-lg-3 col-xl-3 col-md-6 col-sm-12 mb-4" v-for="page in similars">
			                <div class="card page">
			                	<div class="card-header pt-3">
			                		 <div class="text-center">
			                            <img :src="url + '/' + page.logo_src" alt="" class="img-thumbnail border-0 rounded-circle list-thumbnail align-self-center mb-3 shadow">
			                            <h3 class="title">@{{page.name}}</h3>
			                        </div>
			                	</div>
			                	<div class="status" ></div>
			                    <hr>
			                    <div class="card-body pb-2 pt-3">
			                        <div class="row text-center">
			                            <span class="col-6 border-right">
			                                <p class="text-muted text-small mb-2">Running Ads</p>
			                                <p class="mb-3">
			                                    @{{page.ads.toUpperCase()}}
			                                </p>
			                            </span>
			                            <span class="col-6">
			                                <p class="text-muted text-small mb-2">Page Likes</p>
			                                <p class="mb-3">
			                                    @{{page.likes}}
			                                </p>
			                            </span>
			                        </div>
			                    </div>
			                    <div class="card-footer" style="overflow: hidden;">
			                        <div class="row">
			                        	<div class="col-6 text-center">
			                                <a target="_blank" v-bind:href="window.Laravel.norefer+'http://www.facebook.com/'+page.pid" class="btn btn-outline-info btn-sm mb-0 text-revert"><span class="">View</span></a>
			                            </div>
			                            <div class="col-6 text-center">
			                                <form action="{{url('/journal/similar/store')}}" method="post">
			                                    {{csrf_field()}}
			                                    <input type="hidden" :value="page.page_id" name="parent">
			                                    <input type="hidden" :value="page.pid" name="id">
			                                    <input type="hidden" :value="page.name" name="name">
			                                    <input type="hidden" :value="page.logo_src" name="logo">
			                                    <button type="submit" class="btn btn-outline-primary btn-sm mb-0 text-revert"><span class="ml-1">Add Page</span></button>
			                                </form>
			                            </div>
			                        </div>
			                    </div>
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
        </div>
    </main>

@endsection


@section('scripts')
<script type="text/javascript">

	var pages = new Vue({
        el:'#display',
        data:{
            pages:[],
            select:[],
            similars:[],
            color:'#EA8F14',
            type:"all",
            url: window.Laravel.url,
            key:"",
        },
        methods: {
            getSelect: function(type){
            	this.type = type;
                let filtered = this.pages;
                if(type != "all"){
                	filtered = this.pages.filter(p => p.status_code == type);
                	this.select = filtered;
                }else{
                	this.select = this.pages;
                }
            },
            getPages: function(){
            	axios.get(window.Laravel.url+'/journal/pages/get')
                    .then(response => {
                        this.pages = response.data;
                        this.select = this.pages;
                    })
                    .catch(error => {
                        console.log('error : ' , error);
                    });
            },
            getColor: function(code){
            	if(code == 101){
            		return "#EA8F14";
            	}else if(code == 102){
            		return "#2bc437";
            	}else if(code == 103){
            		return "#F11712";
            	}
            },
            getSimilars: function(){
            	axios.get(window.Laravel.url+'/journal/pages/similars/'+this.key)
                    .then(response => {

                        this.similars = response.data;
                    })
                    .catch(error => {
                        console.log('error : ' , error);
                    });
            },
            nextPage:function() {
              if((this.currentPage*this.pageSize) < this.logs.length) this.currentPage++;
            },
            prevPage:function() {
              if(this.currentPage > 1) this.currentPage--;
            },
            clearLog:function(){
                axios.get(this.clearLink)
                    .then(response => {
                        this.logs = [];
                    })
                    .catch(error => {
                        console.log('error : ' , error);
                    });
            },
            format: function(value){
        		if (value) {
			    	return value.slice(0,10);
			  	}
            },
        },
        computed:{
            serverLogs:function() {
              return this.logs.filter((row, index) => {
                let start = (this.currentPage-1)*this.pageSize;
                let end = this.currentPage*this.pageSize;
                if(index >= start && index < end) return true;
              });
            }
        },
        mounted:function(){
            this.getPages();
        },
    });

	$('#btnAdd').on('click',function(){
		if($('#link').val() != ""){
			$('#addForm').submit();
			$(this).prop('disabled',true);
		}
	});


</script>
@endsection