@extends('layouts.master')

@section('body')
    <main class="default-transition" style="opacity: 1;">
        <div class="container-fluid ">
            <div class="col-lg-12 col-md-12 col-sm-12">
            	<div class="card mb-4 text-center">
                    <div class="card-body ">
                        <div class="home-text">
                            <div class="mb-3">
                                <i class="iconsmind-Checked-User" style="font-size: 70px;color:#2C9D35;"></i>
                            </div>
                            <div class="">
                               <h1>Thank you</h1>
                            </div>
                            <p class="text-revert mb-5">
                                Thank you for your subscription.
                                <br>
                                <br>
                                You can start using Adsify Premium Features Now
                            </p>
                            <a href="{{url('/journal')}}" class="btn btn-outline-primary btn-xl mt-4">Get Started</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection

@section('scripts')

@endsection