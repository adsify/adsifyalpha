@extends('layouts.master')

@section('body')
    <main class="default-transition" style="opacity: 1;">
        <div class="container-fluid">
        	@if(isset($posts))
        		@if(sizeof($posts)>0)
        		<div class="list">
					@include('partials.journal.post')
				</div>
				@else
					<div class="text-center">
						<h2>No Posts Collected yet</h2>
						<p>The page you've added will be spied on and the ads will be collected soon..it may take few minutes</p>
						<a href="{{url('/faq')}}" class="btn btn-secondary mb-1">Learn more</a>
					</div>
				@endif
        	@else
				<div class="text-center position-relative">
					<h1>Welcome To Adsify</h1>
					<h2>You have no pages yet, want to add one ? Click below</h2>
					<a href="{{ url('journal/pages') }}" class="btn btn-secondary mb-1">Add Page</a>
				</div>
				<div class="welcome">
					<img src="{{asset('assets/img/journal_home.png')}}">
				</div>
        	@endif
        </div>
    </main>

@endsection


@section('scripts')
<script type="text/javascript">
	var bricklayer = new Bricklayer(document.querySelector('.bricklayer'));
	function setDate(){
		var date = $("#date").val();
		if(!date){
			$("#date").datepicker().datepicker("setDate", new Date());
		}
	}
	/*var app = new Vue({
		el:'#app',
		data:{
			posts: [],
			days: [],
			filter: 0
		},
		methods: {
			getPosts: function(){
				axios.get(window.Laravel.url+'/journal/data/posts')
					.then(response => {
						this.posts = response.data.posts;
						this.days = response.data.days;
						this.filter = response.data.filter;
						console.log('success : ' , response);
					})
					.catch(error => {

					})
			}
		},
		mounted:function(){
			this.getPosts();
		}
	});*/
</script>
@endsection