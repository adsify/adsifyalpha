@extends('layouts.master')
@php
use App\Adsify\Utils;
@endphp

@section('body')
	<main class="default-transition" style="opacity: 1;">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
					<div class="card">
	        			<div class="position-relative border-bottom p-3">
	        				<div>
				                @if($post->type == "image")
				                <img class="card-img-top" src="{{ asset($post->url) }}" alt="Card image cap">
				                @elseif($post->type == "video")
				                <div class="mb-3">
				                	<video src="{{ asset($post->url) }}" width="100%"  preload="metadata" controls />
				                </div>
				               	<h4>Thumbnail</h4>
				               	<img class="card-img-top" src="{{ asset($post->thumbnail) }}" alt="Card image cap">
				                @else
				                	<h4>Collection images</h4>
				                	@php
				                        $output = explode(";",$post->url);
				                        $data = array_chunk($output,sizeof($output)-1)[0];
				                    @endphp
				                	@foreach($data as $img)
										<div class="pb-3">
											<img class="card-img-top" src="{{ asset($img) }}" alt="Card image cap">
										</div>
				                	@endforeach
				                @endif
				            </div>
				        </div>
				    </div>
				</div>
				<div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
					<div class="card">
						<div class="card-body pt-2 pb-0">
		        			<div class="row">
				                <div class="d-flex flex-row mt-2 ml-2 mb-3 pb-3 border-bottom">
				                    <a href="#">
				                        <img alt="Page Picture" src="{{ asset($post->page->logo_src) }}" class="img-thumbnail border-0 rounded-circle list-thumbnail align-self-center small shadow">
				                    </a>
				                    <div class="pl-3 pt-3">
				                        <a target="_blank" href="{{config('adsify.norefer')}}https://www.facebook.com/{{$post->page->id}}" class="list-item-heading mb-0 truncate">{{ '@'.$post->page->name }}</a>
				                    </div>
				                </div>
				            </div>
				            <p class="mb-3 text-one">
				                {!! $post->text !!}
				            </p>
			            </div>
			            <div class="stats">
				            <div class="col-12 mb-2">
				                <div class="row text-center text-muted text-small mb-0 font-weight-light">
				                    @if($post->type == 'image' || $post->type == 'list')
				                    <div class="col-4 border-right">
				                        <span style="">{{ Utils::nbr($post->stats->likes) }} </span>
				                        <div class="clearfix"></div>
				                        <i class="xicon like" style=""></i>
				                    </div>
				                    <div class="col-4 border-right">
				                        <span>{{  Utils::nbr($post->stats->comments) }} </span>
				                        <div class="clearfix"></div>
				                        <i class="xicon comment"></i>
				                    </div>
				                    <div class="col-4">
				                        <span>{{  Utils::nbr($post->stats->shares) }} </span>
				                        <div class="clearfix"></div>
				                        <i class="xicon share"></i>
				                    </div>
				                    @elseif($post->type == 'video')
				                    <div class="col-3 border-right">
				                        <span>{{  Utils::nbr($post->stats->likes) }} </span>
				                        <div class="clearfix"></div>
				                        <i class="xicon like"></i>
				                    </div>
				                    <div class="col-3 border-right">
				                        <span>{{  Utils::nbr($post->stats->comments) }} </span>
				                        <div class="clearfix"></div>
				                        <i class="xicon comment"></i>
				                    </div>
				                    <div class="col-3 border-right">
				                        <span>{{  Utils::nbr($post->stats->shares) }} </span>
				                        <div class="clearfix"></div>
				                        <i class="xicon share"></i>
				                    </div>
				                    <div class="col-3">
				                        <span>{{  Utils::nbr($post->stats->views) }}</span>
				                        <div class="clearfix"></div>
				                        <i class="xicon view"></i>
				                    </div>
				                    @endif
				                </div>
				            </div>
				        </div>
					</div>
				    <div class="card mt-3 pt-3">
				    	<div class="card-body pt-2 pb-4">
				            <p class="text-muted text-small mb-2">Ads Status :</p>
				            <p class="mb-3">
				            	{{strtoupper($post->status->name)}}
	                        </p>
				            <p class="text-muted text-small mb-2">Countries</p>
				            <p class="mb-3">
				            	@foreach($post->countries as $country)
	                            <span class="badge badge-pill badge-outline-secondary mb-1">{{ $country->name }}</span>
	                            @endforeach
	                        </p>
	                        <p class="text-muted text-small mb-2">Estimated Days Running : </p>
				            <p class="mb-3">
				            	{{ $days }}
	                        </p>
	                        <p class="text-muted text-small mb-2">Creation date : </p>
				            <p class="mb-3">
				            	{{date('d M Y', strtotime($post->created_at))}}
	                        </p>
						</div>
					</div>
				</div>
				<div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
					<div class="card">
						<div class="card-body pt-4 pb-4">
							<h3 class="text-center">NoRefer Safe Links</h3>
							<div class="row">
			                    <a target="_blank" href="{{config('adsify.norefer').$post->post}}" class="btn btn-secondary btn-block mb-1">View Original Post</a>
			                    @if($post->shop != "")
			                    <a target="_blank" href="{{$post->shop}}" class="btn btn-warning btn-block mb-1">Post Outgoing Url</a>
			                    @endif
				            </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>

@endsection