<html lang="en"><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Admin Mintone">
    <meta name="author" content="Admin Mintone">

    <title>Adsify Under Maintenace</title>
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('assets/favicon.ico')}}">
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/responsive.css')}}" rel="stylesheet">

</head>
<body class="single-page-bg">
<div id="main-wrapper">
    <div class="navbar-header mt-5 ml-5">
        <a class="navbar-brand" href="https://adsify.io"> <img src="{{asset('assets/img/logo.png')}}" alt="homepage" height="60"> </a> </div>
      <div class="container">
    <div class="page-wrapper m-auto pt-5 mt-5" style="min-height: 576px;">
      <div>
        <div class="row">
          <div class="col-lg-6 col-md-6 text-center img.css')}}"> <img src="{{asset('assets/img/maintenance.png')}}" class="img-fluid" alt="" title=""> </div>
          <div class="col-lg-6 col-md-6 single-403">
            <h2 class="text-primary mt-5"></h2>
            <h3>UNDER MAINTENANCE</h3>
            <h4 class="font-18">{{str_replace("_"," ",env('MAINTENANCE_MESSAGE'))}}</h4>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

</body>
</html>