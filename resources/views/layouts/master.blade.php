<html lang="en">
<head>
    <meta charset="UTF-8">

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name') }}</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('assets/favicon.ico')}}">

    <link rel="stylesheet" href="{{ asset('assets/font/arty/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/vendor/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/vendor/jquery.contextMenu.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/vendor/perfect-scrollbar.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/font/gilroy/fonts.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/dore.dark.blue.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/sweetalert.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/vendor/bootstrap-stars.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/bricklayer.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/font/iconsmind/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/font/simple-line-icons/css/simple-line-icons.css') }}">
    <script>
        window.Laravel = {!! json_encode([
            'url' => url('/'),
            'norefer' => config('adsify.norefer'),
        ]) !!}
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-133284277-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-133284277-1');
    </script>

</head>



<body id="app-container" class="menu-default menu-sub-hidden">
    @include('partials.journal.loader')

    @include('partials.journal.nav')
    
    @include('partials.journal.sidebar')
    
    @include('partials.journal.flush')

    @yield('body')

    <script src="{{ asset('assets/js/vendor/jquery-3.3.1.min.js') }}" style="opacity: 1;"></script>
    <script src="{{ asset('assets/js/vendor/bootstrap.bundle.min.js') }}" style="opacity: 1;"></script>
    <script src="{{ asset('assets/js/vendor/perfect-scrollbar.min.js') }}" style="opacity: 1;"></script>
    <script src="{{ asset('assets/js/vendor/mousetrap.min.js') }}" style="opacity: 1;"></script>
    <script src="{{ asset('assets/js/vendor/jquery.contextMenu.min.js') }}" style="opacity: 1;"></script>
    <script src="{{ asset('assets/js/vue.min.js') }}" style="opacity: 1;"></script>
    <script src="{{ asset('assets/js/axios.min.js') }}" style="opacity: 1;"></script>
    <script src="{{ asset('assets/js/dore.script.js') }}" style="opacity: 1;"></script>
    <script src="{{ asset('assets/js/scripts.js') }}" style="opacity: 1;"></script>
    <script src="{{ asset('assets/js/sweetalert.js') }}" style="opacity: 1;"></script>
    <script src="{{ asset('assets/js/vendor/bootstrap-datepicker.js') }}" style="opacity: 1;"></script>
    <script src="{{ asset('assets/js/vendor/bootstrap-notify.min.js') }}" style="opacity: 1;"></script>
    <script src="{{ asset('assets/js/bricklayer.min.js') }}" style="opacity: 1;"></script>

    <script type="text/javascript">
        $('.alert .close').click(function(){
            $('.alert').hide();
        })
        function gosubscribex(){
            Swal("You can't do this action without a paid subscription");
        }

        function gosubscribe(mode){
            var imageurl = window.Laravel.url;
            if(mode == 'details'){
                imageurl += "/assets/img/sub_details.jpg";
            }else if(mode == 'filter'){
                imageurl += "/assets/img/sub_filter.jpg";
            }else if(mode == 'night'){
                imageurl += "/assets/img/sub_night.png";
            }else if(mode == 'support'){
                imageurl += "/assets/img/sub_support.jpg";
            }else if(mode == 'pages'){
                imageurl += "/assets/img/sub_pages.png";
            }
            Swal.fire({
              title: 'Sorry!',
              width: '50%',
              text: 'This is only for paid subscriptions.',
              imageUrl: imageurl,
              imageWidth: 800,
              imageHeight: 471,
              imageAlt: 'Custom image',
              animation: false
            });
        }
    </script>
    @yield('scripts')

</body>
</html>