<html lang="en">
  <head>

    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Adsify') }}</title>
    <link rel="canonical" href="https://adsify.io" />
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('assets/favicon.ico')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/vendor/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/font/gilroy/fonts.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/unlanding.css') }}">
    
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-133284277-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-133284277-1');
    </script>

  </head>
  <body>

    <section class="header-wrapper d-flex flex-column align-items-center" style="padding-bottom: 0px;">
      <center>
      <header class="header justify-content-between align-items-center">
        <a class="header__logo" href="{{url('/')}}"><img src="{{asset('assets/img/logo.svg')}}" alt="logo"/></a>
      </header>
      </center>
    </section>
    <center>
        @include('partials.journal.flush')
    </center>

    @yield('body')
    <footer class="main-footer__wrapper d-flex flex-column align-items-center" >
      <div class="myborder" style="box-shadow: 0px 10px 0px 10px #ffffff;"></div>
      <div class="self-container">
        <section class="main-footer__content d-flex flex-column align-items-center">
          <p>Why waste thousands of dollars testing ads blindly?</p>
          <p>Find proven ads, innovate, <span class="mark-text">profit.</span></p>
          <ul class="main-footer__social-list d-flex align-items-center">
            <li>
              <a href="/"><img src="{{asset('assets/img/Facebook.svg')}}" alt="facebook"/></a>
            </li>
            <li>
              <a href="/"><img src="{{asset('assets/img/Twitter.svg')}}" alt="twitter"/></a>
            </li>
          </ul>
        </section>
        <hr class="hr" />
        <section class="main-footer__footer d-flex justify-content-between align-items-center flex-wrap">
          <div class="main-footer__nav-section d-flex align-items-center flex-wrap flex1 ">
            <div class="main-footer__logo">
              <a href="{{url('/')}}"><img src="{{asset('assets/img/logo.svg')}}" alt="Adsify logo"/></a>
            </div>
            <div class="main-footer__menu">
              <ul class="d-flex align-items-center main-footer__menu-list">
                <li><a class="main-footer__menu-item" href="{{url('/faq')}}">FAQ.</a></li>
                <li><a class="main-footer__menu-item" href="{{url('/policy')}}">Privacy & Policy.</a></li>
                <li><a class="main-footer__menu-item" href="{{url('/terms')}}">Terms & Conditions.</a></li>
              </ul>
            </div>
          </div>
          <div class="main-footer__copyright">
            <p>Copyright @2018 Adsify.io, All Rights Reserved</p>
          </div>
        </section>
      </div>
    </footer>

    <script src="{{ asset('assets/js/vendor/jquery-3.3.1.min.js') }}" style="opacity: 1;"></script>
    <script type="text/javascript">
        $(function(){
            var input = $('.input-global')
            input.each(function() {
                var this_input = $(this);
                this_input.click(function() {
                    this_input.addClass('input-focus')
                });
            });
        });
    </script>
</html>
