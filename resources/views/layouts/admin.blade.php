<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('assets/imgs/favicon.png')}}">
    <title>Admin Adsify</title>
    <link href="{{asset('plugins/vendors/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/vendors/perfect-scrollbar/css/perfect-scrollbar.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/font/arty/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/sweetalert.css') }}">
    <script>
        window.Laravel = {!! json_encode([
            'url' => url('/'),
        ]) !!}
    </script>
    @stack('before-css')

    <link rel="stylesheet" href="{{asset('plugins/vendors/toast-master/css/jquery.toast.css')}}">
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/responsive.css')}}" rel="stylesheet">

    @stack('after-css')
</head>
<body class="fix-header fix-sidebar card-no-border">
<!-- ============================================================== -->
<!-- Preloader - style you can find in spinners.css')}} -->
<!-- ============================================================== -->
<div class="preloader">
    <div class="loader">
        <div class="loader__figure"></div>
        <p class="loader__label">Admin </p>
    </div>
</div>
<div id="main-wrapper">

    @include('partials.admin.header')

    <div class="container">
        @include('partials.admin.sidebar')
        <div class="page-wrapper">
            
            @include('partials.admin.flush')

            @yield('content')
            
        </div>
    </div>

</div>

<script src="{{asset('plugins/vendors/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap popper Core JavaScript -->
<script src="{{asset('plugins/vendors/bootstrap/js/popper.min.js')}}"></script>
<script src="{{asset('plugins/vendors/bootstrap/js/bootstrap.min.js')}}"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="{{asset('plugins/vendors/ps/perfect-scrollbar.jquery.min.js')}}"></script>
<!--Wave Effects -->
<script src="{{asset('assets/js/waves.js')}}"></script>
<!--Menu sidebar -->
<script src="{{asset('assets/js/sidebarmenu.js')}}"></script>
<!--Custom JavaScript -->
<script src="{{asset('assets/js/custom.min.js')}}"></script>
<script src="{{ asset('assets/js/sweetalert.js') }}" style="opacity: 1;"></script>

<script>
    $('#slimtest1, #slimtest2').perfectScrollbar();
</script>

<script src="{{asset('assets/js/vue.min.js')}}"></script>
<script src="{{asset('assets/js/axios.min.js')}}"></script>
<!-- ============================================================== -->
@stack('js')
</body>
</html>
