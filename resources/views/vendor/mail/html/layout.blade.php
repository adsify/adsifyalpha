<DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>

<body>
        <table style="background-color:#24325c;margin: 0;padding:0;border-collapse:collapse;border-spacing:0px;background-repeat:repeat;background-position:center top;width:100%;height:100%;background-image:url('https://my.stripo.email/content/guids/CABINET_d93b3993b2266d7e846d711243bb30b6/images/78261545211792246.png')"><tr>
            <table align='center' border='0' cellpadding='0' cellspacing='0' width='600' style='border-collapse:collapse;border-spacing:0px;background-color:#f8f8f8;border-radius:15px 15px 15px 15px; margin-top:40px;margin-bottom:40px;'>
                <tr>
                    <td align='center' bgcolor='#ffffff' style='padding: 40px 0 30px 0;border-radius:15px 15px 0px 0px;'>
                        <img src='https://adsify.io/assets/img/logo.png' alt='Adsify' height='100' style='display: block;' />
                    </td>
                </tr>
                <tr>
                    <td style='padding: 40px 30px 40px 30px;'>
                     <table border='0' cellpadding='0' cellspacing='0' width='100%'>
                        <tr>
                            <td style='color: #153643;text-align:center; font-family: Arial, sans-serif; font-size: 24px;padding-bottom:20px;'>
                                <b> {{ $header ?? '' }}</b>
                            </td>
                        </tr>
                        <tr>
                          <td style='color:#8e959c;font-size:14px;line-height:21px;font-family:sans-serif;padding-bottom:20px;'>
                          {{ Illuminate\Mail\Markdown::parse($slot) }}
                          </td>
                        </tr>
                     </table>
                    </td>
                </tr>
                <tr>
                    <td bgcolor='#ddf3ff' style='border-radius:0px 0px 15px 15px;padding: 30px;color: #191919;text-align:center; font-family: Arial, sans-serif; font-size: 14px;'>
                         &reg; Adsify, Copyright 2019<br>
                    </td>
                </tr>
            </table>
          </tr>
        </table>
        </body>
</html>