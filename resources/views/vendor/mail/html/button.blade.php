<table class="action" align="center" width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center">
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="center">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style='text-align:center;padding:30px;padding-bottom:0px;'>
                                    <a href="{{ $url }}" style='border-radius:4px;display:inline-block;font-size:12px;font-weight:bold;line-height:22px;padding:10px 20px;text-align:center;text-decoration:none!important;color:#fff!important;border:1px solid rgba(0,0,0,0.25);background-color:#35b463;font-family:sans-serif;'>{{ $slot }}</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
