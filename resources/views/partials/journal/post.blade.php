@php
use App\Adsify\Utils;
@endphp

@if(isset($journal))
    @if($journal != '2')
    <div class="text-center col-md-12">
        <nav class="mt-1 mb-3">
            <ul class="pagination justify-content-center mb-0">
                {{ $posts->render() }}
            </ul>
        </nav>
    </div>
    @endif
@endif
<div class="bricklayer">
@foreach($posts as $post)
    <div class="card mb-4 @if(isset($bookmark)) bookmark @endif post @if($post->type == 'list') collection @endif " data-id="{{$post->id}}">
        <div class="position-relative border-bottom" >
            <span class="badge badge-pill @if($post->status_code == 112)badge-success @elseif($post->status_code == 111)badge-warning @else badge-danger @endif position-absolute badge-top-left">{{strtoupper($post->status->name)}}</span>
            <span class="badge badge-pill badge-secondary position-absolute badge-top-left-2">{{ $days[$post->id] }} Days</span>
            <span class="badge badge-pill badge-primary position-absolute badge-top-left-3">
                @if($post->status_code == 111) {{$post->page->size}} @else {{ sizeof($post->countries) }} @endif Countries</span>
            @if(isset($bookmark))
            <span class="position-absolute book"><i class="icons ibookmark"></i></span>
            @endif
            <div class="content" >
                @if($post->type == "image")
                <div class="image">
                    <img class="card-img-top" src="{{ asset($post->url) }}" alt="Card image cap">
                </div>
                @elseif($post->type == "video")
                <div class="thumbnail" style="background-image: url('{{ asset($post->thumbnail) }}')"></div>
                <div class="video">
                    <video src="{{ asset($post->url) }}" preload="metadata" controls />
                </div>
                @elseif($post->type == "list")
                <div class="row position-relative border-bottom">
                    <div class="list">
                        @php
                            $output = explode(";",$post->url);
                            $size = sizeof($output);
                            if($size >= 4){
                                $data = array_chunk($output,4)[0];
                            }else{
                                 $data = array_chunk($output,$size)[0];
                            }
                        @endphp
                        @foreach($data as $img)
                            @if($img != "")
                            <img class="column responsive" src="{{asset($img)}}" alt="Card image cap">
                            @endif
                        @endforeach
                        <div class="overlay-div">
                             <div class="overlay-effect"></div>
                             <p class="overlay-text">More..</p>
                        </div>
                    </div>
                </div>
                @endif
            </div>
            
        </div>
        <div class="card-body pt-2 pb-0">
            @if(isset($bookmark))
            <div class="float-right">
                <a href="#" data-id="{{$post->id}}" onclick="unsave({{$post->id}})" class="badge badge-pill display-block badge-danger position-relative badge-center-1 text-white"><i class="simple-icon-trash"></i> UnSave</a>
            </div>
            @endif
            <div class="float-right">
                <a target="_blank" href="{{config('adsify.norefer').$post->post}}" class="badge badge-pill display-block badge-primary position-relative badge-center">View Post</a>
            </div>
            @if(Auth::user()->subscription->status_code == 302)
            <div class="float-right">
                <a href="{{url('/journal/post/'.$post->id)}}" class="badge badge-pill display-block badge-success position-relative badge-center-2">View Details</a>
            </div>
            @else
            <div class="float-right">
                <a href="#" onclick="gosubscribe('details')" class="badge badge-pill display-block badge-success position-relative badge-center-2">View Details</a>
            </div>
            @endif
            <div class="row">
                <div class="d-flex flex-row mb-2 pb-2 border-bottom">
                    <a href="#">
                        <img alt="Page Picture" src="{{ asset($post->page->logo_src) }}" class="img-thumbnail border-0 rounded-circle list-thumbnail align-self-center shadow xsmall">
                    </a>
                    <div class="pl-3 pt-2">
                        <p class="list-item-heading mb-0 truncate">{{ $post->page->name }}</p>
                        <span class="text-muted text-small mb-0 font-weight-light">{{date('d M Y', strtotime($post->created_at))}}</span>
                    </div>
                </div>
            </div>
            <p class="mb-3 short-text">
                {!! $post->text !!}
            </p>
        </div>
        <div class="card-footer stats p-1">
            <div class="col-12 mb-2 ">
                <div class="row text-center text-muted text-small mb-0 font-weight-light">
                    @if($post->type == 'image' || $post->type == 'list')
                    <div class="col-4 border-right">
                        <span style="">{{ Utils::nbr($post->stats->likes) }} </span>
                        <div class="clearfix"></div>
                        <i class="xicon like" style=""></i>
                    </div>
                    <div class="col-4 border-right">
                        <span>{{  Utils::nbr($post->stats->comments) }} </span>
                        <div class="clearfix"></div>
                        <i class="xicon comment"></i>
                    </div>
                    <div class="col-4">
                        <span>{{  Utils::nbr($post->stats->shares) }} </span>
                        <div class="clearfix"></div>
                        <i class="xicon share"></i>
                    </div>
                    @elseif($post->type == 'video')
                    <div class="col-3 border-right">
                        <span>{{  Utils::nbr($post->stats->likes) }} </span>
                        <div class="clearfix"></div>
                        <i class="xicon like"></i>
                    </div>
                    <div class="col-3 border-right">
                        <span>{{  Utils::nbr($post->stats->comments) }} </span>
                        <div class="clearfix"></div>
                        <i class="xicon comment"></i>
                    </div>
                    <div class="col-3 border-right">
                        <span>{{  Utils::nbr($post->stats->shares) }} </span>
                        <div class="clearfix"></div>
                        <i class="xicon share"></i>
                    </div>
                    <div class="col-3">
                        <span>{{  Utils::nbr($post->stats->views) }}</span>
                        <div class="clearfix"></div>
                        <i class="xicon view"></i>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endforeach
</div>
@if(isset($journal))
    @if($journal != '2')
    <div class="text-center col-md-12">
        <nav class="mt-4 mb-3">
            <ul class="pagination justify-content-center mb-0">
                {{ $posts->render() }}
            </ul>
        </nav>
    </div>
    @endif
@endif