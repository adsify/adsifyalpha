@if(session()->has('success'))
<div data-notify="container" class="col-12 col-sm-3 alert alert-success animated fadeInDown" role="alert" data-notify-position="top-center" >
	<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
    <span data-notify="title">{{ session()->get('success') }}</span> 
</div>
@elseif(session()->has('error'))
<div data-notify="container" class="col-12 col-sm-3 alert alert-danger animated fadeInDown" role="alert" data-notify-position="top-center" >
	<button type="button"  aria-hidden="true" class="close" data-notify="dismiss">×</button>
    <span data-notify="title">{{ session()->get('error') }}</span> 
</div>
@elseif(session()->has('warning'))
<div data-notify="container" class="col-12 col-sm-3 alert alert-warning animated fadeInDown" role="alert" data-notify-position="top-center" >
	<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
    <span data-notify="title">{{ session()->get('warning') }}</span> 
</div>
@endif

