<div class="sidebar" style="opacity: 1;">
    <div class="main-menu default-transition">
        <div class="scroll ps border-right border-white" style="height: 430px;">
            <ul class="list-unstyled">
                @if(!isset($journal))
                <li>
                    <a href="{{url('/journal')}}">
                        <i class="arty-journal"></i> <span>My Ads</span>
                    </a>
                </li>
                
                <li>
                    <a href="#filter">
                        <i class="glyph-icon simple-icon-equalizer"></i><span> Filter </span> 
                    </a>
                </li>

                <li class="active" style="display:none;">
                    <a href="#"  >
                        <i class="arty-journal"></i> <span>My Ads</span>
                    </a>
                </li>
                @else
                <li class="active">
                    <a href="{{url('/journal')}}" >
                        <i class="arty-journal"></i> <span>My Ads</span>
                    </a>
                </li>
                <li>
                    <a href="#filter">
                        <i class="glyph-icon simple-icon-equalizer"></i> <span> Filter </span>
                    </a>
                </li>
                @endif
                <li class="@if(isset($page)) active @endif" >
                    <a href="{{url('journal/pages')}}">
                        <i class="iconsmind-Books-2"></i> <span> Pages </span>
                    </a>
                </li>
                
                <li class="@if(isset($bookmark)) active @endif">
                    <a href="{{url('journal/bookmarks')}}" class="icon-menu-item">
                        <i class="iconsmind-Bookmark"></i>
                        <span>Bookmarks</span>
                    </a>
                </li>
            </ul>
        <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
    </div>

    <div class="sub-menu default-transition" >
        <div class="ps" style="height: 626px;">
            <form action="{{url('/journal/filter')}}" method="post">
                @if(Auth::user()->subscription->status_code == 302)
                    @csrf
                @endif
                <ul class="list-unstyled" data-link="filter" style="display: block;">
                    @if(isset($filter))
                        <div class="col input-group mb-4 mr-sm-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">PAGE</div>
                            </div>
                            <select class="form-control select2-single select2-hidden-accessible" name="page" tabindex="-1" aria-hidden="true">
                                <option value="">All Pages</option>
                                @foreach($filter as $page)
                                <option value="{{$page->id}}" @if($_COOKIE['page'] == $page->id) selected @endif>{{$page->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        @if(isset($countries))
                        <div class="col input-group mb-4 mr-sm-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">COUNTRY</div>
                            </div>
                            
                            <select name="country" class="form-control select2-single select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                <option value="">All Countries</option>
                                @foreach($countries as $c)
                                <option value="{{$c->id}}" @if($_COOKIE['country'] == $c->id) selected @endif>{{$c->name}}</option>
                                @endforeach
                            </select>
                            
                        </div>
                        @endif
                        <div class="col input-group mb-4 mr-sm-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">SORT BY</div>
                            </div>
                            <select name="sort" class="form-control select2-single select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                <option value="">Any</option>
                                <option value="likes" @if($_COOKIE['sort'] == 'likes') selected @endif>LIKES</option>
                                <option value="comments" @if($_COOKIE['sort'] == 'comments') selected @endif>COMMENTS</option>
                                <option value="shares" @if($_COOKIE['sort'] == 'shares') selected @endif>SHARES</option>
                            </select>
                        </div>
                        <div class="col input-group mb-4 mr-sm-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">POST TYPE</div>
                            </div>
                            <select name="type" class="form-control select2-single select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                <option value="">Any</option>
                                <option value="image" @if($_COOKIE['type'] == 'image') selected @endif>IMAGE</option>
                                <option value="video" @if($_COOKIE['type'] == 'video') selected @endif>VIDEO</option>
                                <option value="list" @if($_COOKIE['type'] == 'list') selected @endif>COLLECTION</option>
                            </select>
                        </div>
                        <div class="col input-group mb-4 mr-sm-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">POST STATUS</div>
                            </div>
                            <select name="status" class="form-control select2-single select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                <option value="">Any</option>
                                <option value="111" @if($_COOKIE['status'] == '111') selected @endif>NEW</option>
                                <option value="112" @if($_COOKIE['status'] == '112') selected @endif>RUNNING</option>
                                <option value="113" @if($_COOKIE['status'] == '113') selected @endif>STOPPED</option>
                            </select>
                        </div>
                        <div class="col mr-sm-2">
                            <div class="input-group date">
                                <span class="input-group-text input-group-append input-group-addon">
                                    DATE FROM
                                </span>
                                <input type="text" id="date" onclick="setDate()" name="date" value="@if($_COOKIE['date'] != '0') {{$_COOKIE['date']}} @endif" autocomplete="off" class="form-control">
                            </div>
                        </div>
                        @if(Auth::user()->subscription->status_code == 302)
                        <div class="row p-3">
                            <button type="submit" class="btn btn-primary btn-block mr-2 ml-2">FILTER NOW</button>
                        </div>
                        <div class="p-1 text-center">
                            <a href="{{url('/journal/filter/reset')}}" class="btn btn-danger btn-sm">Reset Filter</a>
                        </div>
                        @else
                        <div class="row p-3">
                            <button type="button" onclick="gosubscribe('filter')" class="btn btn-primary btn-block mr-2 ml-2 mb-1">FILTER NOW</button>
                        </div>
                        @endif
                    @else
                    <p class="col">There is no filter for this page</p>
                    @endif
                </ul>
            </form>

            <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
                <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
            </div>
            <div class="ps__rail-y" style="top: 0px; right: 0px;">
                <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div>
            </div>
        </div>
    </div>
</div>