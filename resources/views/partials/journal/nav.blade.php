<nav class="navbar fixed-top" style="opacity: 1;">
    <div class="d-flex align-items-center navbar-left">
        <a href="#" class="menu-button d-none d-md-block">
            <svg class="main" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 9 17">
                <rect x="0.48" y="0.5" width="7" height="1"></rect>
                <rect x="0.48" y="7.5" width="7" height="1"></rect>
                <rect x="0.48" y="15.5" width="7" height="1"></rect>
            </svg>
            <svg class="sub" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 17">
                <rect x="1.56" y="0.5" width="16" height="1"></rect>
                <rect x="1.56" y="7.5" width="16" height="1"></rect>
                <rect x="1.56" y="15.5" width="16" height="1"></rect>
            </svg>
        </a>

        <a href="#" class="menu-button-mobile d-xs-block d-sm-block d-md-none">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 17">
                <rect x="0.5" y="0.5" width="25" height="1"></rect>
                <rect x="0.5" y="7.5" width="25" height="1"></rect>
                <rect x="0.5" y="15.5" width="25" height="1"></rect>
            </svg>
        </a>

    </div>

    <a class="navbar-logo" href="#">
        <span class="logo d-none d-xs-block"></span>
        <span class="logo-mobile d-block d-xs-none"></span>
    </a>

    <div class="navbar-right">
        <div class="header-icons d-inline-block align-middle">

            @if(Auth::user()->subscription->status_code == 301)
            <a class="btn btn-sm btn-primary mr-2 d-none d-md-inline-block mb-2" href="{{url('/journal/subscription')}}">&nbsp;<span>{{session()->get('trial')}} Days Left</span>&nbsp;</a>
            @endif

            <div class="position-relative d-inline-block">
                <button class="header-icon btn btn-empty" type="button" id="iconMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="simple-icon-settings"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right mt-3 position-absolute" id="iconMenuDropdown">

                    <a href="{{url('journal/profile')}}" class="icon-menu-item">
                        <i class="iconsmind-MaleFemale d-block"></i>
                        <span>Profile</span>
                    </a>
                    
                    <a href="{{url('journal/subscription')}}" class="icon-menu-item">
                        <i class="arty-subscription"></i>
                        <span>Subscription</span>
                    </a>
                    
                    @if(Auth::user()->subscription->status_code == 302)
                    <a href="{{url('journal/tickets')}}" class="icon-menu-item">
                        <i class="iconsmind-Support"></i>
                        <span>Support</span>
                    </a>
                    @else
                    <a href="#" onclick="gosubscribe('support')" class="icon-menu-item">
                        <i class="iconsmind-Support"></i>
                        <span>Support</span>
                    </a>
                    @endif

                    <a href="{{url('/faq')}}" class="icon-menu-item">
                        <i class="arty-faq"></i><br>
                        <span>FAQ</span>
                    </a>
                    
                    @if(Auth::user()->subscription->status_code == 302)
                    <a href="#" class="icon-menu-item" id="themeButton">
                        <i id="sun" class="iconsmind-Sun"></i>
                        <i id="moon" class="iconsmind-Half-Moon"></i>
                        <br>
                        <span id="themetxt">Night Mode</span>
                    </a>
                    @else
                    <a href="#" onclick="gosubscribe('night')" class="icon-menu-item" >
                        <i class="iconsmind-Half-Moon"></i>
                        <br>
                        <span id="themetxt">Night Mode</span>
                    </a>
                    @endif

                    @if(Auth::user()->is_admin == 1)
                    <a href="{{url('/admin')}}" class="icon-menu-item">
                        <i class="arty-admin"></i><br>
                        <span>ADMIN</span>
                    </a>
                    @endif

                </div>
                <button class="header-icon btn btn-empty" type="button" onclick="event.preventDefault();
                               document.getElementById('logout-form').submit();">
                    <i class="sicon arty-logout"></i>
                </button>
                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                  @csrf
                </form>
            </div>
          
        </div>
    
    </div>
</nav>