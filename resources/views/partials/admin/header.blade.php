<header class="topbar">
    <div Class="container">
        <nav class="navbar top-navbar navbar-expand-md navbar-light">
            <div class="navbar-header">
                <a class="navbar-brand" href="{{url('admin/')}}">
                    <b>
                        <img src="{{asset('assets/img/logo-black.svg')}}" alt="homepage" class="dark-logo"/>
                    </b>
                </a>
            </div>
            <div class="top-bar-main">
                <div class="float-left ml-5">
                    <ul class="navbar-nav">
                        <li class="nav-item ">
                            <a class="nav-link navbar-toggler sidebartoggler waves-effect waves-dark float-right" href="javascript:void(0)"><span class="navbar-toggler-icon"></span></a>
                        </li>
                        <li class="nav-item ">
                            @if(config('adsify.maintenance') == 1)
                             <a class="nav-link dropdown-toggle waves-effect waves-dark text-danger" href="{{url('admin/settings')}}" >Maintenace Mode : ON</a>
                            @endif
                        </li>
                    </ul>
                </div>
                <div class="float-right pr-3">
                    <ul class="navbar-nav my-lg-0 float-right">
                        <li class="nav-item dropdown u-pro">
                            <a class="nav-link dropdown-toggle waves-effect waves-dark" href="#" onclick="event.preventDefault();
                           document.getElementById('logout-form').submit();">Log out</a>
                        </li>
                    </ul>
                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                      @csrf
                    </form>
                </div>
                <div class="clearfix"></div>
            </div>
        </nav>
    </div>
</header>
