<div class="modal fade log-profile" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modle-big">
      <div class="modal-content">
       <div class="modal-body p-2 pt-3">
        <div class="pl-3">
          <a href="#" @click="clearLog()" class="pull-right text-danger inline-block mr-2"><i class="fas fa-trash-alt ">
          </i> Clear Logs </a>
          <a href="" data-toggle="modal"  data-target=".edit-profile" class="pull-right text-info inline-block mr-2 "> <i class="fas fa-pencil-alt"> </i> Edit </a>
          <h6>Profile Name : <span>@{{profile.name}}</span></h6>
          <h6>Description : </h6>
          <blockquote class="left-border pt-2 pb-2">@{{profile.proxy+"__"+profile.fb}}</blockquote>
        </div>
        <hr>
        <div class="col-lg-12">
            <h5>Logs</h5>
            <div class="table-responsive">
                <table class="table color-table primary-table">
                    <thead>
                    <tr>
                        <th>Message</th>
                        <th>Date</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr v-for="log in serverLogs">
                        <td>@{{log.message}}</td>
                        <td>@{{log.created_at}}</td>
                        <td>
                          <div v-if="log.type == 'image'">
                            <a :href="window.Laravel.url+'/storage/errors/'+log.message" target="_blank"><i class="fa fa-eye"></i></a>
                          </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="text-center">
                <button @click="prevPage" type="button" class="btn waves-effect waves-light btn-sm btn-rounded btn-primary">Previous</button>
                <button @click="nextPage" type="button" class="btn waves-effect waves-light btn-sm btn-rounded btn-primary">Next</button>
            </div>
        </div>
       </div>
      </div>
    </div>
</div>
@foreach($servers as $server)
  @php
    $color = "#79D622";
    if($server->status_code == 202){
      $color = "#fa7d03";
    }else if($server->status_code == 203){
      $color = "#F04545";
    }
  @endphp
  <div class="col-lg-4 col-md-6">
    <div class="card">
      <div class="card-body">
        <div class="d-flex no-block" >
            <div class="ml-auto text-light-blue"> 
              <a href="#" onclick="confirm('{{url("/admin/server/log/clear/".$server->id)}}')" class="pull-left text-primary inline-block mr-2 "><i class="fas fa-info "></i> Clear All Logs </a>
              <a href="#" onclick="confirm('{{url("/admin/server/remove/".$server->id)}}')" class="pull-left text-danger inline-block mr-2 "> <i class="fas fa-trash-alt "></i> Remove </a>
            </div>
        </div>
        <div class="d-flex pt-3 pb-3 no-block">
          <div class="align-self-center mr-3 knob-icon">
            <div style="display:inline;width:70px;height:70px;">
                 <input class="dial" data-plugin="knob" data-width="70" data-height="70" data-linecap="round" data-fgcolor="{{$color}}" data-thickness=".2" value="100" style="width: 39px; height: 23px; position: absolute; vertical-align: middle; margin-top: 23px; margin-left: -54px; border: 0px; background: none; font: bold 14px Arial; text-align: center; color: {{$color}} ; padding: 0px; -webkit-appearance: none;">
            </div>
            <i class="flaticon-tool" style="color: {{$color}}"></i> 
          </div>
          <div class="align-slef-center mr-auto">
            <h2 class="m-b-0 text-uppercase font-18 font-medium lp-5">{{ $server->name }}</h2>
            <h6 class="text-light m-b-0">Status <strong>{{strtoupper($server->status->name)}}</strong></h6>
          </div>
        </div>
        <div class="text-center font-18">
          {{$server->description}}
        </div>
        <div class="text-center font-18">
            Total Profiles : <span class="text-primary font-20">{{sizeof($server->profiles)}}</span>
        </div>
      </div>
    </div>
  </div>

  <div class="col-lg-8 col-md-12">
    <div class="card">
      <div class="card-body pt-3 pb-4">
        <!--h4 class="card-title  pull-left text-uppercase m-b-25">Ser</h4-->
        <ul class="nav nav-pills nav-pills-tablet pull-right m-t-0 mb-2">
            <li class=" nav-item">
              <a href="#stats-{{$server->id}}" class="nav-link active show" data-toggle="tab" aria-expanded="false">Stats</a>
            </li>
            <li class="nav-item">
              <a href="#profiles-{{$server->id}}" class="nav-link" data-toggle="tab" aria-expanded="false">Profiles</a>
            </li>
        </ul>
        <div class="clearfix"></div>
        <div class="tab-content br-n pn">
          <div id="stats-{{$server->id}}" class="tab-pane active show">
            <div class="row panel-main m-b-10 no-block panel-refresh bordered-css pt-5 pb-4">
              <div class="col-lg-4 col-md-12 ">
                <h5 class="card-title align-self-center">CPU</h5>
                <div class="block m-t-10 ">
                  <div class="cpu-div-left"> <span class="block font-20 text-pink">{{$server->used_pro}}%</span> Using </div>
                  <div class="cpu-div-right pl-4"> <span class="block font-20 text-primary">{{$server->count_pro}}.</span> CPUs </div>
                  <div class="clearfix"></div>
                </div>
              </div>
              <div class="col-lg-4 col-md-12 bordered-left-light">
                <h5 class="card-title align-self-center">Memory</h5>
                <div class="block m-t-10">
                  <div class="cpu-div-left"> <span class="block font-20 text-pink">{{$server->used_memory}}%</span> Using </div>
                  <div class="cpu-div-right pl-4"> <span class="block font-20 text-primary">{{$server->free_memory}} Gb</span> Available </div>
                  <div class="clearfix"></div>
                </div>
              </div>
              <div class="col-lg-4 col-md-12 bordered-left-light text-center">
                <h5 class="card-title align-self-center">Last Activity</h5>
                <div class="block m-t-10 text-center">
                  <div class=""> <span class="block font-20 text-primary">{{$server->last_activity}}</span></div>
                  <div class="clearfix"></div>
                </div>
              </div>
            </div>
          </div>
          <div id="profiles-{{$server->id}}" class="tab-pane">
             @if(sizeof($server->profiles) > 0)
              <div class="owl-container">
                <div class="owl-carousel owl-theme owl-loaded">
                  <div class="owl-stage-outer">
                      <div class="owl-stage">
                     
                        @foreach($server->profiles as $profile)
                          <div class="owl-item border-right">
                            <div class="p-2">
                              @php
                                $scolor = "#4eff00";
                                if($profile->status_code == 202){
                                  $scolor = "#fa7d03";
                                }else if($profile->status_code == 203){
                                  $scolor = "#F04545";
                                }
                              @endphp
                              <div class="status" style="box-shadow: inset 0 0 50px {{$scolor}};"></div>
                              <ul class="list-unstyled mt-2 mb-1">
                                <li class="text-center font-18">{{$profile->name}}</li>
                                <li class="font-16 text-primary text-center">{{$profile->last_activity}}</li>
                                <li><span class="font-bold">Proxy:</span> {{$profile->proxy}}</li>
                                <li><span class="font-bold">Fb:</span> {{$profile->fb}}</li>
                              </ul>
                              <div class="row text-center">
                                <div class="col-6 text-center">
                                  <a href="" data-toggle="modal"  data-target=".log-profile" @click="getLog({{$profile->id}})" class="text-info">
                                    @if($profile->log == 1)
                                    <div class="notify" style="top:15px;right:62px;"><span class="heartbit"></span> <span class="point"></span></div>
                                    @endif
                                    <i class="fas fa-info "></i>
                                    Logs
                                  </a>
                                </div>
                                <div class="col-6 text-center">
                                  <a href="#" onclick="confirm('{{url("/admin/servers/profile/remove/".$profile->id)}}')" class="text-danger"> <i class="fas fa-trash-alt "></i> Remove </a>
                                </div>
                              </div>
                            </div>
                          </div>
                        @endforeach
                      </div>
                  </div>
                </div>
              </div>
              @else
                <div class="text-center p-5 mt-4">
                    <h2>No Profiles Added Yet..</h2>
                </div>
              @endif
          </div>

        </div>
    </div>
  </div>
  
</div>
@endforeach