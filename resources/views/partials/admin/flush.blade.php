<div class="text-center">
	@if(session()->has('success'))
	<div class="alert alert-success animated fadeInDown font-16">{{ session()->get('success') }}
	    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><i class="mdi mdi-close font-18"></i></button>
	</div>
	@elseif(session()->has('error'))
	<div class="alert alert-danger animated fadeInDown font-16">{{ session()->get('error') }}
	    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><i class="mdi mdi-close font-18"></i></button>
	</div>
	@elseif(session()->has('warning'))
	<div class="alert alert-warning animated fadeInDown font-16">{{ session()->get('warning') }}
	    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><i class="mdi mdi-close font-18"></i></button>
	</div>
	@endif
</div>


