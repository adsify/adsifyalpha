<aside class="left-sidebar">
    <ul class="nav-bar navbar-inverse hidden-xs-down">
        <li class="nav-item">
            <a class="nav-link navbar-toggler sidebartoggler  waves-effect waves-dark float-right" href="javascript:void(0)">
                <span class="navbar-toggler-icon"></span></a>
        </li>
    </ul>
    <div class="scroll-sidebar">
        <nav class="sidebar-nav ">
            <ul id="sidebarnav">
                <li class="clearfix"></li>
                    
                <li><a href="{{url('admin/servers')}}" ><i class="arty-servers"></i><span class="hide-menu">Servers</span></a>
                </li>
                <li><hr></li>
                <li class="">
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                        <i class="arty-journal"></i><span class="hide-menu">Pages</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{url('admin/pages')}}">All Pages</a></li>
                        <!--li><a href="{{url('admin/pages/featured')}}">Featured</a></li-->
                    </ul>
                </li>
                <li><hr></li>
                <li class="">
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                        <i class="fa fa-users"></i><span class="hide-menu">Users</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{url('admin/users/active')}}">Active</a></li>
                        <li><a href="{{url('admin/users/suspend')}}">Suspended</a></li>
                    </ul>
                </li>
                <li><hr></li>
                <li class="">
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                        <i class="arty-subscription"></i><span class="hide-menu">Subscriptions</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{url('admin/subscriptions/billed')}}">Billed</a></li>
                        <li><a href="{{url('admin/subscriptions/unbilled')}}">UnBilled</a></li>
                        <li><a href="{{url('admin/subscriptions/suspended')}}">Suspended</a></li>
                        <li><a href="{{url('admin/subscriptions/free')}}">Free</a></li>
                    </ul>
                </li>
                <li><hr></li>
                <li class="">
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                        <i class="flaticon-restaurant"></i><span class="hide-menu">Payments</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{url('admin/payments/plans')}}">Plans</a></li>
                        <li><a href="{{url('admin/payments/manual')}}">Manual Payment</a></li>
                    </ul>
                </li>
                <li><hr></li>
                <li class="">
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                        <i class="flaticon-mail"></i><span class="hide-menu">Support</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{url('admin/tickets/pending')}}">Pending</a></li>
                        <li><a href="{{url('admin/tickets/solved')}}">Solved</a></li>
                        <li><a href="{{url('admin/tickets/canceled')}}">Canceled</a></li>
                    </ul>
                </li>
                <li><hr></li>
                <li><a href="{{url('/journal')}}"><i class="fas fa-book"></i><span class="hide-menu">Journal</span></a>
                </li>
                <li><hr></li>
                <li><a href="{{url('/admin/settings')}}"><i class="flaticon-desktop-computer-screen-with-rising-graph"></i><span class="hide-menu">Settings</span></a>
                </li>
            </ul>
        </nav>
    </div>
</aside>