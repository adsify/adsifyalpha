@extends('layouts.nsmaster')

@section('body')

<center>
  <section class="play-price d-flex flex-column align-items-center" style="margin-top: 50px;">
      <div class="price-wrapper flex-wrap">
        <div class="price-item d-flex flex-column">
           <form method="POST" action="{{ route('register') }}">
            @csrf
            <div class="price-item__header d-flex flex-column align-items-center {{ $errors->has('email') ? ' is-invalid' : '' }}" style="padding: 10px 30px;">
              <p class="price-item__price-type title">{{ __('Sign up') }}</p>
              <span class="input-global input-focus">
                <input id="name" type="text" name="name" value="{{ old('name') }}" required autofocus>
                <label for="name">
                  <span>{{ __('First name') }}</span>
                </label>
              </span>
              <span class="input-global input-focus">
                <input id="last"  type="text" name="last" value="{{ old('last') }}" required autofocus>
                <label for="last">
                  <span>{{ __('Last name') }}</span>
                </label>
              </span>
              <span class="input-global input-focus">
                @if ($errors->has('email'))
                  <span class="invalid">
                      <strong>{{ $errors->first('email') }}</strong>
                  </span>
                @endif
                <input id="userMail" class="" type="email" name="email" value="{{ old('email') }}" required autofocus>
                <label for="userMail">
                  <span>{{ __('E-Mail Address') }}</span>
                </label>
              </span>
              <span class="input-global input-focus">
                @if ($errors->has('password'))
                  <span class="invalid">
                      <strong>{{ $errors->first('password') }}</strong>
                  </span>
                @endif
                <input id="pass" type="password" name="password" required placeholder="" pattern="[A-Za-z0-9]{8,}" title="The password length should be more than 8 characters">
                <label for="pass">
                  <span>{{ __('Password') }}</span>
                </label>
              </span>
              <span class="input-global input-focus">
                @if ($errors->has('password_confirmation'))
                  <span class="invalid">
                      <strong>{{ $errors->first('password_confirmation') }}</strong>
                  </span>
                @endif
                <input id="confirm" type="password" name="password_confirmation" required placeholder="">
                <label for="confirm">
                  <span>{{ __('Confirm Password') }}</span>
                  @if ($errors->has('password_confirmation'))
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('password_confirmation') }}</strong>
                      </span>
                  @endif
                </label>
              </span>
            </div>
            <div class="price-item__footer d-flex flex-column align-items-center justify-content-between {{ $errors->has('email') ? ' is-invalid' : '' }}" style="padding: 20px 30px;">
              <span class="checkbox">
                <input type="checkbox" name="agree" id="userChecked" required="" >
                <label for="userChecked" class="font-18">
                  <span class=""> I agree to the <a href="{{url('/terms')}}" target="_blank" class="d-inline"><u>Terms and Conditions</u></a> </span>
                </label>
              </span>
              <div class="submit-block text-center">
                <input type="submit" class="btn btn-third" value="{{ __('Sign up') }}">
                <span>Already registred on Adsify ? <a href="{{url('login')}}">Log in</a> </span>
              </div>
            </div>
          </form>
        </div>
      </div>
  </section>
</center>
@endsection