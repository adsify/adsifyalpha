@extends('layouts.nsmaster')

@section('body')

<center>
  <section class="play-price d-flex flex-column align-items-center" style="margin-top: 50px;">
      <div class="price-wrapper flex-wrap">
        <div class="price-item d-flex flex-column">
           
           <form method="POST" action="{{ route('password.email') }}">
            @csrf
            <div class="price-item__header d-flex flex-column align-items-center {{ $errors->has('email') ? ' is-invalid' : '' }}" style="padding: 10px 30px;">
              <p class="price-item__price-type title">{{ __('Forgot Password') }}</p>
              <span class="input-global">
                @if ($errors->has('email'))
                  <span class="invalid">
                      <strong>{{ $errors->first('email') }}</strong>
                  </span>
                @endif
                <input id="userMail" class="" type="email" name="email" value="{{ old('email') }}" required autofocus>
                <label for="userMail">
                  <span>{{ __('E-Mail Address') }}</span>
                </label>
              </span>
            </div>
            <div class="price-item__footer d-flex flex-column align-items-center justify-content-between {{ $errors->has('email') ? ' is-invalid' : '' }}" style="padding: 20px 30px;">
              <div class="submit-block text-center">
                <input type="submit" class="btn btn-third" value="{{ __('Reset Password') }}">
                <span>Not registered on Adsify yet? <a href="{{url('register')}}">Sign up</a> </span>
              </div>
            </div>
          </form>
        </div>
      </div>
  </section>
</center>
@endsection