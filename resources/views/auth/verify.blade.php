@extends('layouts.nsmaster')

@section('body')

<center>
  <section class="play-price d-flex flex-column align-items-center" style="margin-top: 50px;">
      <div class="price-wrapper flex-wrap">
        <div class="price-item d-flex flex-column">
           
           <form method="" action="">
            <div class="price-item__header d-flex flex-column align-items-center" style="padding: 10px 30px;">
              <p class="price-item__price-type title">{{ __('Verify Email') }} <br> Address</p>
            </div>
            <div class="price-item__footer d-flex flex-column text-left" style="padding: 20px 30px;">
              <p>
                {{ __('Before proceeding, please check your email for a verification link.') }}
                        {{ __('If you did not receive the email') }}, <u><a href="{{ route('verification.resend') }}">{{ __('Click here to Request Another') }}</a></u>.
              </p>
              <br>
              <p>
                If you want to cancel this registration please <u> <a href="#" onclick="event.preventDefault();document.getElementById('logout-form').submit();" >Click here.</a> </u>
                    
                    
              </p>
            </div>
          </form>
          <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
            @csrf
          </form>
        </div>
      </div>
  </section>
</center>
@endsection