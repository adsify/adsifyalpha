import Journal from "./components/journal/Main.vue";
import Login from './components/auth/Login.vue';
import Pages from './components/journal/Pages.vue';
import Posts from './components/journal/Posts.vue';
import PostDetails from './components/journal/Details.vue';
import Profile from './components/journal/Profile.vue';
import Tickets from './components/journal/Tickets.vue';
import Chat from './components/journal/Chat.vue';
import Charts from './components/journal/Charts.vue';

export const routes = [
    {
        path: "/spa/",
        meta: {
            requiresAuth: true
        }
    },
    {
        path: "/spa/journal",
        component: Journal,
        meta: {
            requiresAuth: true
        },
        children: [
            {
                path: '/',
                component: Posts
            },
            {
                path: ':id',
                component: PostDetails
            },
            {
                path: '/spa/pages',
                component: Pages
            },
            {
                path: '/spa/support',
                component: Tickets
            },
            {
                path: '/spa/support/:id',
                component: Chat
            },
            {
                path: '/spa/profile',
                component: Profile
            },
            {
                path: '/spa/charts',
                component: Charts
            }
        ]
    },
    {
        path: '/spa/login',
        component: Login
    }

];



  