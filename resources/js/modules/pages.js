export default {
    state: {
        pages: [],
        select: [],
        archived:[],
    },
    getters: {
        pages(state){
            return state.pages;
        },
        select(state){
            return state.select;
        },
        archived(state){
            return state.archived;
        }
    },
    mutations: {
        updatePages(state,payload){
            state.pages = payload;
            state.select = payload;
        },
        updateArchiveds(state,payload){
            state.archived = payload;
        },
        updateSelect(state,payload){
            state.select = payload;
        },
        archivePage(state,page){
            state.archived.push(page);
            state.pages = state.pages.filter(p => p.id !== page.id);
            state.select = state.select.filter(p => p.id !== page.id);
        },
        unarchivePage(state,page){
            state.archived = state.archived.filter(p => p.id !== page.id);
            state.pages.push(page);
            state.select = state.pages;
        }
    },
    actions: {
        getPages(context){
            axios.get('/api/spa/pages', {headers: {'Authorization': `Bearer ${context.rootState.currentUser.token}`}})
            .then((response) => {
                context.commit('updatePages', response.data.pages);
                context.commit('updateArchiveds', response.data.archived);
            });
        },
        filterPages(context, type){
            let filtered = context.state.pages;
            if(type != "all"){
                filtered = context.state.pages.filter(p => p.status_code == type);
                context.commit('updateSelect', filtered);
            }else{
                context.state.select = context.state.pages;
            }
        },
        archivePage(context,page){
            axios.post('/api/spa/pages/archive', {id: page.id}, {headers: {'Authorization': `Bearer ${context.rootState.currentUser.token}`}})
            .then((response) => {
                context.commit('archivePage', page);
                context.commit('deletePosts', page.id);
                context.commit('updateAlert', response.data.message);
            });
        },
        unarchivePage(context,page){
            axios.post('/api/spa/pages/unarchive', {id: page.id}, {headers: {'Authorization': `Bearer ${context.rootState.currentUser.token}`}})
            .then((response) => {
                context.commit('unarchivePage', page);
                context.rootState.reload = true;
                context.commit('updateAlert', response.data.message);
            });
        }
    }
}