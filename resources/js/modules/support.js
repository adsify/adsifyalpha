export default {
    state: {
        tickets: [],
        categories: [],
    },
    getters: {
        tickets(state){
            return state.tickets;
        },
        categories(state){
            return state.categories;
        },
    },
    mutations: {
        updateTickets(state, payload){
            state.tickets = payload;
        },
        updateCategories(state, payload){
            state.categories = payload;
        },
        addTicket(state, ticket){
            state.tickets.push(ticket);
        },
    },
    actions: {
        getTickets(context){
            axios.get('/api/spa/tickets', {headers: {'Authorization': `Bearer ${context.rootState.currentUser.token}`}})
            .then((response) => {
                context.commit('updateTickets', response.data.tickets);
                context.commit('updateCategories', response.data.categories);
            });
        },
        addTicket(context, ticket){
            context.commit('addTicket',ticket);
        },
    }
}