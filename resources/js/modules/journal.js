export default {
    state: {
        posts: [],
        filter: [],
    },
    getters: {
        posts(state){
            return state.filter;
        },
    },
    mutations: {
        setFilter(state,filter){
            let filtered = state.posts;
            if(filter.page != ''){
                filtered = filtered.filter(p => p.page_id == filter.page);
            }
            if(filter.date != ''){
                console.log(filter.date);
                filtered = filtered.filter(p => new Date(p.updated_at) > new Date(filter.date));
            }
            if(filter.sort != ''){
                
                if(filter.sort == 'likes'){
                    filtered = filtered.sort((a,b) => b.stats.likes - a.stats.likes);
                }else if(filter.sort = 'comments'){
                    filtered = filtered.sort((a,b) => b.stats.comments - a.stats.comments);
                }else{
                    filtered = filtered.sort((a,b) => b.stats.shares - a.stats.shares);
                }
            }
            if(filter.status != ''){
                filtered = filtered.filter(p => p.status_code == filter.status);
            }
            if(filter.type != ''){
                filtered = filtered.filter(p => p.type == filter.type);
            }
            state.filter = filtered;
        },
        resetFilter(state){
            state.filter = state.posts;
        },
        updatePosts(state,payload){
            state.posts = payload;
            state.filter = payload;
        },
        deletePosts(state,pageid){
            state.posts = state.posts.filter(p => p.page_id != pageid);
            state.filter = state.posts;
        }
    },
    actions: {
        getPosts(context){
            axios.get('/api/spa/posts', {headers: {'Authorization': `Bearer ${context.rootState.currentUser.token}`}})
            .then((response) => {
                context.commit('updatePosts', response.data.posts);
            });
        },
        filterPosts(context,filter){
            context.commit('setFilter', filter);
        },
        resetFilter(context){
            context.commit('resetFilter');
        },
    }
}