export default {
    state: {
        stats: {
            users: {
                dates: ["January","February","Mars","April","May","June","July","August","September","October","November","December"],
                values: {
                    name: 'Users',
                    data: [0,0,0,0,0,0,0,0,0,0,0,0]
                }
            },
            pages: '',
        },
    },
    getters: {
        userStats(state){
            return state.stats.users;
        },
    },
    mutations: {
        userStats(state, payload){
            state.stats.users.values.data = [0,0,0,0,0,0,0,0,0,0,0,0]
            payload.forEach(user => {
                state.stats.users.values.data[parseInt(user.login_month)-1]= user.login_num;
            });
        },
    },
    actions: {
        userStats(context,year){
            return new Promise((resolve,reject) => {
                axios.post('/api/spa/stats/users',{year: year}, {headers: {'Authorization':`Bearer ${context.rootState.currentUser.token}`}})
                .then((response) => {
                    context.commit('userStats',response.data);
                    resolve()
                });
            })
        },
    }
}