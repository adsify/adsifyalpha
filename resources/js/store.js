import {getLocalUser} from './helpers/auth';
import pages from './modules/pages';
import support from './modules/support';
import charts from './modules/charts';
import journal from './modules/journal';

const user = getLocalUser();

export default{
    modules: {
        pages,
        support,
        charts,
        journal,
    },
	state: {
        currentUser: user,
        isLoggedIn: !!user,
        loading: false,
        auth_error: null,
        alert: false,
        reload: false,
	},
	getters: {
		isLoading(state){
            return state.loading;
        },
        isLoggedIn(state){
            return state.isLoggedIn;
        },
        currentUser(state){
            return state.currentUser;
        },
        authError(state){
            return state.auth_error;
        },
        alert(state){
            return state.alert;
        },
	},
	mutations: {
        login(state){
            state.loading = true;
            state.auth_error = null;
        },
        loginSuccess(state, payload){
            state.auth_error = null;
            state.isLoggedIn = true;
            state.loading = false;
            console.log(payload.access_token);
            state.currentUser = Object.assign({}, payload.user, {token: payload.access_token});
            localStorage.setItem("user", JSON.stringify(state.currentUser));
            axios.defaults.headers.common["Authorization"] = `Bearer ${state.currentUser.token}`
        },
        loginFailed(state, payload){
            state.loading = false;
            state.auth_error = payload;
        },
        logout(state) {
            localStorage.removeItem("user");
            state.isLoggedIn = false;
            state.currentUser = null;
        },
        updateAlert(state,text){
            state.alert = text;
        },
    },
	actions: {
        login(context){
            context.commit("login");
        },
        showAlert(context, text){
            context.commit('updateAlert', text);
        },
        closeAlert(context){
            context.commit('updateAlert', false);
        },
    }
}