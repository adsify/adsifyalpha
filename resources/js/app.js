require("./bootstrap");
import Vue from "vue";
import Vuex from "vuex";
import VueRouter from "vue-router";
import {routes} from './routes';
import StoreData from './store';
import MainApp from './components/MainApp.vue';
import {initialize} from './helpers/general';
import BootstrapVue from 'bootstrap-vue'

Vue.use(VueRouter);
Vue.use(Vuex);
Vue.use(BootstrapVue);

const store = new Vuex.Store(StoreData);

const router = new VueRouter({
    routes,
    mode: 'history'
});

initialize(store, router);

const app = new Vue({
    el: "#app",
    router,
    store,
    components: {MainApp},
    computed: {
        currentUser(){
            return this.$store.getters.currentUser;
        }
    }
});
