export function initialize(store, router){
    router.beforeEach((to,from,next) => {
        const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
        const currentUser = store.getters.currentUser;
        if(requiresAuth && !currentUser){
            NProgress.done()
            next('/spa/login');
        }else if(to.path == '/spa/login' && currentUser ){
            next('/spa/journal');
        }else{
            axios.interceptors.response.use(null, (error) => {
                if(error.response.status == 401){
                    store.commit('logout');
                    router.push('/spa/login');
                    NProgress.done()
                }
                return Promise.reject(error);
            });
        
            axios.interceptors.request.use(config => {
                NProgress.start()
                return config
            });
              
            axios.interceptors.response.use(response => {
                NProgress.done()
                return response
            });
            next();
        }
    });

    /*if (store.getters.currentUser) {
        setAuthorization(store.state.currentUser.token);
    }*/
}

export function setAuthorization(token) {
    axios.defaults.headers.common["Authorization"] = `Bearer ${token}`
}