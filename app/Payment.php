<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    public function user(){
    	return $this->belongsTo('App\User');
    }

    public function status(){
        return $this->belongsTo('App\Statu', 'status_code' ,'code');
    }
}
