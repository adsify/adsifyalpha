<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Task;

class ProcessTasks implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $tasks = Task::where('mode','=','server')->limit(5)->get();
        if(sizeof($tasks) > 0 ){
            $data = [];
            foreach($tasks as $task){
                $country_link = $task->country;
                $country_id = "0";
                if(strpos($country_link,"=")){
                    $country_id = explode("=",$country_link)[1];
                }
                $page = $task->page;
                $posts = $page->postsIds;
                $row = ['id' => $task->id , 'page' => $page->id , 'name' => $page->name , 'country' => ['link' => $country_link , 'id' => $country_id ], 'posts' => $posts];
                array_push($data, $row);
                $task->delete();
            }
            $json = json_encode($data);
            echo $json;
        }else{
            echo "0";
        }
    }
}
