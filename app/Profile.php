<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Profile extends Model
{
	use SoftDeletes;

    protected $dates = ['deleted_at'];
    
    public function server(){
    	return $this->belongsTo('App\Server');
    }

    public function logs(){
    	return $this->hasMany('App\Log');
    }
}
