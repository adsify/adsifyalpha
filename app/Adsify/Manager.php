<?php
namespace App\Adsify;

use App\User;
use Auth;
use App\Subscription;
use App\Payment;
use App\Statu;
use App\Log;
use Carbon\Carbon;
use Illuminate\Http\Request;

class Manager{

	public function __construct()
	{

	}

	public static function updateSubscription($user,$status,$recurring_id = "0",$service = "0"){
		try{
			if($recurring_id != "0"){
				$user->subscription->recurring_id = $recurring_id;
			}

			if($status == 302){
				if($user->subscription->service == "free"){
					Manager::unarchivePages($user);
				}
				if($service == "payment"){
					$today = Carbon::now();
					$user->subscription->last_payment = $today;
					$service = "paypal";
				}else if($service == "reactive"){
					if($user->subscription->last_payment == null){
						$service = "pending";
					}else{
						$service = "paypal";
					}
				}if($service == "cancel"){
					if($user->subscription->service == "pending"){
						$status = 301;
						$service = "free";
						Manager::archivePages($user);
					}
				}
			}else if($status == 301){
				if($service == "by admin"){
					Manager::archivePages($user);
				}
			}else if($status == 900){
				Manager::archivePages($user);
			}else if($status == 303){
				Manager::archivePages($user);
				$service = "free";
				$user->subscription->last_payment = null;
			}
			$user->subscription->status_code = $status;
			if($service == "archive"){
				Manager::archivePages($user);
				$service = "free";
			}
			if($service != "0"){
				$user->subscription->service = $service;
			}
			$user->subscription->save(); 

		}catch(Exception $ex){
            Manager::addLog(0,501,$ex->getMessage());
            return redirect('journal')->with('warning','Something went wrong! Please contact support.');
        }
	}

	public static function addPayment($user,$status,$amount,$service,$txn_id,$event){
		$payment = new Payment();
        $payment->user_id = $user->id;
        $payment->status_code = $status;
        $payment->amount = $amount;
        $payment->service = $service;
        $payment->txn_id = $txn_id;
        $payment->title = "Adsify subscription ".$event;
        $payment->save();
	}

	public static function addLog($server,$status,$message){
		if($server == 0){
			$server = 1;
		}
		$log = new Log();
        $log->profile_id = $server;
        $log->code = $status;
        $log->message = $message;
        $log->save();
	}

    public static function archivePages($user){
    	try{
	        $pages = $user->pages;
	        if(!$pages)
	        	return;
	        $x = sizeof($pages);
	        if($x > config('adsify.free')){
	            $y = config('adsify.free');
	            foreach ($pages as $page) {
	                $user->archive_pages()->attach($page->id);
	                $user->pages()->detach($page->id);
	                if(count($page->users) == 0){
			            $page->status_code = 104;
			            $page->save();
			        }
	                if(++$y == $x){
	                    break;
	                }
	            }
	        }
        }catch(Exception $ex){
        	Manager::addLog(0,501,$ex->getMessage());
		}
    }

    public static function archivePagesAll($user){
    	$pages = $user->pages;
    	if(!$pages)
        	return;
        foreach($pages as $page){
        	$user->pages()->detach($page->id);
        	if(!$user->archive_pages->contains($page->id)){
	            $user->archive_pages()->attach($page->id);
	        }
        	if(count($page->users) == 0){
	            $page->status_code = 104;
	            $page->save();
	        }
        }
    }

	public static function unarchivePages($user){
		try{
			$pages = $user->archive_pages;
			if(!$pages)
	        	return;
	        foreach ($pages as $page) {
	            $user->archive_pages()->detach($page->id);
	            if(!$user->pages->contains($page->id)){
		            $user->pages()->attach($page->id);
		        }
	            if($page->status_code == 104){
	            	$page->status_code = 102;
	            	$page->save();
	            }
	        }
		}catch(Exception $ex){
			Manager::addLog(0,501,$ex->getMessage());
		}
    }

    public static function save_activity(Request $request,$message){
        activity()
           ->causedBy(Auth::user())
           ->withProperties(['ip' => $request->ip(),
                            'agent' => $request->header('User-Agent')])
           ->log($message);
    }

    public static function updateDotEnv($key, $newValue, $delim=''){

	    $path = base_path('.env');
	    // get old value from current env
	    $oldValue = env($key);

	    // was there any change?
	    if ($oldValue === $newValue) {
	        return;
	    }

	    // rewrite file content with changed data
	    if (file_exists($path)) {
	        // replace current value with new value 
	        file_put_contents(
	            $path, str_replace(
	                $key.'='.$delim.$oldValue.$delim, 
	                $key.'='.$delim.$newValue.$delim, 
	                file_get_contents($path)
	            )
	        );
	    }
	}
}
