<?php
namespace App\Adsify;

use Carbon\Carbon;

class Utils{

	public static function count_days($create,$update){
        $created = new Carbon($create);
        $updated = new Carbon($update);
        $difference = ($created->diff($updated)->days < 1)
            ? '1'
            : $created->diffInDays($updated);
        return $difference;
    }

    public static function nbr($n, $precision = 1 ) {
	    $n = (int) $n;
	    if ($n < 900) {
	        $n_format = number_format($n, $precision);
	        $suffix = '';
	    } else if ($n < 900000) {
	        $n_format = number_format($n / 1000, $precision);
	        $suffix = 'K';
	    } else if ($n < 900000000) {
	        $n_format = number_format($n / 1000000, $precision);
	        $suffix = 'M';
	    }
	    if ( $precision > 0 ) {
	        $dotzero = '.' . str_repeat( '0', $precision );
	        $n_format = str_replace( $dotzero, '', $n_format );
	    }
	    return $n_format . $suffix;
	}
	
}