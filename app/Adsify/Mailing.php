<?php 
namespace App\Adsify;

use Mail;

class Mailing{

	public static function welcome($params){
    	$subject = "Welcome to Adsify";
    	$data = [
			'header' => "Thank you for joining Adsify",
			'subheader' => "Hello ".$params['name'].",",
			'text' => "Welcome! Adsify is one of the most innovative platforms on the web. We are here to help you succeed, and look forward to helping you more.<br>Our support team is available to assist with all your questions and technical issues. We have a lot of great resources and features that we are excited to share with you in our platform.<br>Hit the button below to start using our platform with your free subscription.",
			'btn' => "Get started",
			'url' => url('/journal'),
		];
        $mail = new Mailing();
        $mail->mail($params,$subject,$data);
    }

    public static function ticket($params,$event){
    	$data = [];
    	$subject = "";
    	if($event == "created"){
    		$subject = "Support Ticket Created - Adsify";
    		$data = [
    			'header' => 'Ticket Created',
    			'subheader' => "Hello ".$params['name'].',',
    			'text' => "A support ticket <b><#".$params['id']."></b> has been created to address your concern.<br>Providing you technical assistance is our highest priority, and we will be working on a response to you within 48 business hours.<br> Adsify and our technical team value your input. Please let us know of any feedback that helps us improve our support of you.",
    			'btn' => 'View Ticket',
    			'url' => url('/')."/journal/ticket/".$params['id'],
    		];
    	}else if($event == "admin"){
            $subject = "Ticket Created by Admins of Adsify";
            $data = [
                'header' => 'Ticket Created',
                'subheader' => "Hello ".$params['name'].',',
                'text' => "A Ticket <b><#".$params['id']."></b> has been created to provide you with technical assistance in a high priority, please respond to us within 48 business hours.<br> Adsify and our technical team value your input.",
                'btn' => 'View Ticket',
                'url' => url('/')."journal/ticket/".$params['id'],
            ];
        }else if($event == "message"){
            $subject = "Message sent from Admins of Adsify";
            $data = [
                'header' => $params['title']." - Adsify",
                'subheader' => "Hello ".$params['name'].',',
                'text' => $params['message'],
                'btn' => 'Journal',
                'url' => url('/journal'),
            ];
        }else if($event == "solved"){
    		$subject = "Support Ticket Solved - Adsify";
    		$data = [
    			'header' => 'Ticket Solved',
    			'subheader' => "Hello ".$params['name'].',',
    			'text' => "Your Ticket <b><#".$params['id']."></b> has been resolved, and the status of the support request has been changed to closed.<br>However, if you feel as though it has not been resolved, please send an email to <b>".config('adsify.support')."</b> within the next 72 hours to reopen the ticket. Adsify and our technical team value your input.",
    			'btn' => 'View Ticket',
    			'url' => url('/')."journal/ticket/".$params['id'],
    		];
    	}else if($event == "canceled"){
    		$subject = "Support Ticket Canceled - Adsify";
    		$data = [
    			'header' => 'Ticket Canceled',
    			'subheader' => "Hello ".$params['name'],
    			'text' => "Your Ticket <b><#".$params['id']."></b> has been cancelled, and the status of the support request has been changed from open to closed.<br>If you feel as though it has not been resolved, please send an email to <b>".config('adsify.support')."</b> within the next 72 hours to reopen the ticket. Adsify and our technical team value your input.<br> Please let us know of any feedback that helps us improve our support of you.",
    			'btn' => 'View Ticket',
    			'url' => url('/')."journal/ticket/".$params['id'],
    		];
    	}else if($event == "respond"){
    		$subject = "You received a Response - Adsify";
    		$data = [
    			'header' => 'Your Ticket is Answered!',
    			'subheader' => "Hello ".$params['name'],
    			'text' => "Your Ticket <b><#".$params['id']."></b> has been answered.<br> If you don't reply to your ticket within the next 72 hours, it will be closed automatically.<br> Adsify and our technical team value your input. Please let us know of any feedback that helps us improve our support of you.",
    			'btn' => 'View Ticket',
    			'url' => url('/')."journal/ticket/".$params['id'],
    		];
    	}
        $mail = new Mailing();
        $mail->mail($params,$subject,$data);
    }

    public static function subscription($params,$event){
    	$data = [];
    	$subject = "";
    	if($event == "created"){
    		$subject = "Woohoo! Your Subscription Has Been Activated.";
    		$data = [
    			'header' => 'Adsify Subscription Activated',
    			'subheader' => "Hello ".$params['name'].",",
    			'text' => "My name is Mouad and I'm the CEO of Adsify, the best platform to keep you updated about your competitor pages. <br> I want to personally congratulate you on making the best investment in your eCom journey and help us also improve and make things more and even better for you guys who use our platform! <br> Our technical team is here to ensure the highest functionality of Adsify, and we hope you enjoy your subscription.<br>Adsify Premium features gives you :<br><br><p><b> <span style='color:#00b7fa'>✔</span> Unlimited pages to add.</b> </p><p><b> <span style='color:#00b7fa'>✔</span> Filter your journal ads.</b> </p><p><b> <span style='color:#00b7fa'>✔</span> See more details and stats about ads.</b> </p><p><b> <span style='color:#00b7fa'>✔</span> Your pages data will have high priority to be collected first. </b> </p><p><b> <span style='color:#00b7fa'>✔</span> Archive the dead pages to not be displayed in your journal.</b> </p><p><b> <span style='color:#00b7fa'>✔</span> Access Our support platform.</b> </p><p><b> <span style='color:#00b7fa'>✔</span> Night/Light Mode avalaible.</b> </p><br> Click the button below to login and start adding pages NOW.",
    			'btn' => 'Get Started',
    			'url' => url('/journal'),
    		];
    	}else if($event == "paid"){
    		$subject = "Adsify Subscription Paid";
    		$data = [
    			'header' => 'Thank you for making your payment!',
    			'subheader' => "Hello ".$params['name'].",",
    			'text' => "Your subscription payment has been received. Thank you for making your payment. <br> Be sure to let us know if you would like to update your subscription preferences.",
    			'btn' => 'Get Started',
    			'url' => url('/journal'),
    		];
    	}else if($event == "canceled"){
    		$subject = "Adsify Subscription Has been Canceled";
    		$data = [
    			'header' => 'Adsify Subscription Canceled',
    			'subheader' => "Hello ".$params['name'].",",
    			'text' => "Your Adsify subscription has been canceled. We are sorry to see you go. We welcome you back at any time to try Adsify again. Adsify Pro Version gives you more options and keeps you updated on everything new happening with your pages. We would love for you to give Adsify another try. If you would like to provide any feedback to your Adsify experience, please respond to this email and identify what type of feedback you are providing, and why.",
    			'btn' => 'View Subscription',
    			'url' => url('/')."/journal/subscription",
    		];
    	}else if($event == "suspended"){
    		$subject = "Adsify Subscription Has Been Suspended";
    		$data = [
    			'header' => 'Adsify Subscription Suspended',
    			'subheader' => "Hello ".$params['name'].",",
    			'text' => "Hello, due to lack of payment, your Adsify subscription has been temporarily suspended. At this time, you will not be able to use your Adsify subscription. Please contact us immediately in order to ensure prompt payment of your subscription, and your continued satisfaction with your Adsify account.",
    			'btn' => 'View Subscription',
    			'url' => url('/')."/journal/subscription",
    		];
    	}else if($event == "reactivated"){
    		$subject = "Adsify Subscription Re-activated";
    		$data = [
    			'header' => 'Adsify Subscription Re-activated',
    			'subheader' => "Hello ".$params['name'].",",
    			'text' => "Welcome back to Adsify! Your subscription has been re-activated. Our technical team is here to ensure the highest functionality of Adsify, and we hope you enjoy your subscription.Be sure to let us know if you would like to update your subscription preferences.",
    			'btn' => 'View Subscription',
    			'url' => url('/')."/journal/subscription",
    		];
    	}
        $mail = new Mailing();
        $mail->mail($params,$subject,$data);
    }

    public static function customMail($params,$subject,$data){
        $mail = new Mailing();
        $mail->mail($params,$subject,$data);
        echo "Sent <br>";
    }

    public static function userNotification($params,$event){
        $data = [];
        $subject = "";
        if($event == "suspend"){
            $subject = "Adsify Account Suspended";
            $data = [
                'header' => 'Account Suspension',
                'subheader' => "Hello ".$params['name'].",",
                'text' => "At Adsify, we take the activities and reporting of our subscriptions very seriously. We treat every report the same, and follow the processes to ensure that a comprehensive review of all issues has been conducted, and to determine what action is necessary. At this time, this user has been banned from Adsify.<br>Users will be banned when users behave inappropriately, perform disrespectful actions, or do anything generally considered as being against the guidelines of Adsify for a continuous amount of time.<br>If you would like a review of the user ban, please respond to this email and provide any feedback that you feel would be pertinent to this ban.",
                'btn' => 'Banned',
                'url' => url('/')."/banned",
            ];
        }else if($event == "activate"){
            $subject = "Adsify Account Re-activated";
            $data = [
                'header' => 'Welcome back',
                'subheader' => "Hello ".$params['name'].",",
                'text' => "Welcome back to Adsify! Your account has been re-activated. Our technical team is here to ensure the highest functionality of Adsify, and we hope you enjoy your subscription.<br>Be sure to let us know if you would like to update your subscription preferences.",
                'btn' => 'View Features',
                'url' => url('/')."/journal/subscription",
            ];
        }else if($event == "info"){
            $subject = "Adsify Notificaiton";
            $data = [
                'header' => $params['update'],
                'subheader' => "Hello ".$params['name'].",",
                'text' => "Congratulations! You have asked to have your user information updated. At time, the information associated with your user profile has been updated to reflect your preferences. You are able to make these changes at any time.<br><br>Changes occured : ".$params['update']."<br><br>If that was not you, please contact our support : ". config('adsify.support'),
                'btn' => 'View Profile',
                'url' => url('/journal/profile'),
            ];
        }else if($event == "mailv"){
            $subject = "Adsify Email Confirmation";
            $data = [
                'header' => "Confirm your new email",
                'subheader' => "Hello ".$params['name'].",",
                'text' => "Please click the button below to verify your new email address.",
                'btn' => 'Confirm email',
                'url' => url('/')."/journal/profile/email?token=".$params['token'],
            ];
        }else if($event == "mails"){
            $subject = "Adsify email confirmation";
            $data = [
                'header' => "Your Profile email is updated",
                'subheader' => "Hi ".$params['name'].",",
                'text' => "Congratulations! You have asked to have your user information updated. At time, the information associated with your user profile has been updated to reflect your preferences.",
                'btn' => 'View Journal',
                'url' => url('/journal'),
            ];
        }
        $mail = new Mailing();
        $mail->mail($params,$subject,$data);
    }

    public static function adminNotification($event, $profile = ""){
        $message = "";
        $channel = "";
        if($event == "server"){
            $message = "A Server [ ".$profile." ] is in Danger, Help!";
            $channel = "@adsify_pub";
        }else if($event == "money"){
            $message = "New Subscriber Has Joined Adsify.";
            $channel = "@adsify_pub";
        }else if($event == "support"){
            $message = "New Ticket Created";
            $channel = "@adsifysupport";
        }
        
        $params = [
            'chat_id' => $channel,
            'text' => $message,
        ];
        $ch = curl_init("https://api.telegram.org/bot728146881:AAH4PStKg3goTqhqUBAxhU6hNgcqUuq5O20/sendMessage");
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close($ch);
    }

    public function mail($params,$subject,$data){
        $html = Mailing::mail_html($data);
        $array_data = array(
            'from'=> "Adsify" .'<'.config('adsify.mail').'>',
            'to'=>$params['name'].'<'.$params['mail'].'>',
            'subject'=>$subject,
            'html'=>$html,
        );
        $session = curl_init(env('MAILGUN_ENDPOINT').'/messages');
        curl_setopt($session, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($session, CURLOPT_USERPWD, 'api:'.env('MAILGUN_SECRET'));
        curl_setopt($session, CURLOPT_POST, true);
        curl_setopt($session, CURLOPT_POSTFIELDS, $array_data);
        curl_setopt($session, CURLOPT_HEADER, false);
        curl_setopt($session, CURLOPT_ENCODING, 'UTF-8');
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($session, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($session);
        curl_close($session);
        $results = json_decode($response, true);
        return $results;
    }

   public static function mail_html($data){
        $html = "<body style='background-color:#24325c'>
        <table style=\"margin: 0;padding:0;border-collapse:collapse;border-spacing:0px;background-repeat:repeat;background-position:center top;width:100%;height:100%;background-image:url('https://my.stripo.email/content/guids/CABINET_d93b3993b2266d7e846d711243bb30b6/images/78261545211792246.png')\"><tr>
            <table align='center' border='0' cellpadding='0' cellspacing='0' width='600' style='border-collapse:collapse;border-spacing:0px;background-color:#f8f8f8;border-radius:15px 15px 15px 15px; margin-top:40px;margin-bottom:40px;'>
                <tr>
                    <td align='center' bgcolor='#ffffff' style='padding: 40px 0 30px 0;border-radius:15px 15px 0px 0px;'>
                        <img src='https://adsify.io/assets/img/logo.png' alt='Adsify' height='100' style='display: block;' />
                    </td>
                </tr>
                <tr>
                    <td style='padding: 40px 30px 40px 30px;'>
                     <table border='0' cellpadding='0' cellspacing='0' width='100%'>
                         <tr>
                          <td style='color: #153643;text-align:center; font-family: Arial, sans-serif; font-size: 24px;padding-bottom:20px;'>
                             <b>".$data['header']."</b>
                          </td>
                         </tr>
                         <tr>
                          <td style='color:#8e959c;font-size:14px;line-height:21px;font-family:sans-serif;padding-bottom:20px;'>
                          ".$data['subheader']."<br><br>
                          ".$data['text'].".
                          </td>
                         </tr>
                         <tr>
                            <td style='text-align:center;padding:30px;padding-bottom:0px;'>
                                <a href='".$data['url']."' style='border-radius:4px;display:inline-block;font-size:12px;font-weight:bold;line-height:22px;padding:10px 20px;text-align:center;text-decoration:none!important;color:#fff!important;border:1px solid rgba(0,0,0,0.25);background-color:#35b463;font-family:sans-serif;'>".$data['btn']."</a>
                            </td>
                         </tr>
                     </table>
                    </td>
                </tr>
                <tr>
                    <td bgcolor='#ddf3ff' style='border-radius:0px 0px 15px 15px;padding: 30px;color: #191919;text-align:center; font-family: Arial, sans-serif; font-size: 14px;'>
                         &reg; Adsify, Copyright 2019<br/>
                    </td>
                 </tr>
            </table>
          </tr>
        </table>
        </body>";
        return $html;
    }
    

}