<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //
    public $incrementing = false;

    public function page(){
    	return $this->belongsTo('App\Page');
    }

    public function countries(){
        return $this->belongsToMany('App\Country', 'post_country', 'post_id', 'country_id');
    }

    public function stats(){
    	return $this->hasOne('App\Stat');
    }

    public function status(){
        return $this->belongsTo('App\Statu', 'status_code' ,'code');
    }

    public function users(){
        return $this->belongsToMany('App\User','bookmarks','post_id','user_id');
    }
}
