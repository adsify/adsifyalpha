<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewPage extends Model
{
	protected $table = "newpages";

    public function page(){
    	return $this->belongsTo('App\Page');
    }
}
