<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agreement extends Model
{
	public $incrementing = false;
	
    public function user(){
    	return $this->belongsTo('App\User');
    }
}
