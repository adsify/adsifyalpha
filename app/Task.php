<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    public function page(){
    	return $this->belongsTo('App\Page');
    }
}
