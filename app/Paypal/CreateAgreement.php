<?php 
namespace App\Paypal;

use PayPal\Api\Agreement;
use PayPal\Api\Payer;
use PayPal\Api\Plan;
use PayPal\Api\ShippingAddress;
use Carbon\Carbon;
use App\Invoice;
use PayPal\Api\AgreementStateDescriptor;


class CreateAgreement extends Paypal
{
	public function create($id){
		$agreement = new Agreement();
		try{
			$date = Carbon::now()->addMinutes(70)->toIso8601ZuluString();
			$agreement->setName('Adsify Agreement')
			    ->setDescription('Adisfy Premium Subscription')
			    ->setStartDate($date);
			$plan = new Plan();
			$plan->setId($id);
			$agreement->setPlan($plan);
			$payer = new Payer();
			$payer->setPaymentMethod('paypal');
			$agreement->setPayer($payer);
		    $agreement = $agreement->create($this->apiContext);
		    $approvalUrl = $agreement->getApprovalLink();
		}catch(Exception $ex){
			echo $ex->getMessage();
		}
		return redirect($approvalUrl);
	}

	public function execute($token){
		try{
		    $agreement = new Agreement();
	        $agreement = $agreement->execute($token, $this->apiContext);
	        $agreement_id = $agreement->getId();
        }catch(Exception $ex){
			echo $ex->getMessage();
		}
        return $agreement_id;
	}

	public function cancel($id){
		try{
			$agreementStateDescriptor = new AgreementStateDescriptor();
	        $agreementStateDescriptor->setNote("Suspending agreement");
	        $createdAgreement = $this->getAgreement($id);
	        $createdAgreement->suspend($agreementStateDescriptor, $this->apiContext);
	        $agreement = Agreement::get($createdAgreement->getId(), $this->apiContext);
        }catch(Exception $ex){
			echo $ex->getMessage();
		}
        return $agreement;
	}

	public function reactivate($id){
		try{
			$agreementStateDescriptor = new AgreementStateDescriptor();
			$agreementStateDescriptor->setNote("Reactivating agreement");
			$suspendedAgreement = $this->getAgreement($id);
		    $suspendedAgreement->reActivate($agreementStateDescriptor, $this->apiContext);
		    $agreement = Agreement::get($suspendedAgreement->getId(), $this->apiContext);
	    }catch(Exception $ex){
			echo $ex->getMessage();
		}
		return $agreement;
	}

	public function getAgreement($id){
	    $agreement = Agreement::get($id, $this->apiContext);
		return $agreement;
	}

}