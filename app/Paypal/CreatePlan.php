<?php 
namespace App\Paypal;

use Illuminate\Http\Request;
use PayPal\Api\ChargeModel;
use PayPal\Api\Currency;
use PayPal\Api\MerchantPreferences;
use PayPal\Api\PaymentDefinition;
use PayPal\Api\Patch;
use PayPal\Api\PatchRequest;
use PayPal\Api\Plan;
use PayPal\Common\PayPalModel;


class CreatePlan extends Paypal
{
	
	public function create($request)
	{
		$plan = new Plan();
		$plan->setName($request->input('name'))
			->setDescription('Adsify Premium Plan')
			->setType('infinite');

		$paymentDefinition = new PaymentDefinition();

		$paymentDefinition->setName('Regular Payments')
		    ->setType('REGULAR')
		    ->setFrequency('MONTH')
		    ->setFrequencyInterval("1")
		    ->setCycles("0")
		    ->setAmount(new Currency(array('value' => $request->input('price'), 'currency' => 'USD')));
		
		$merchantPreferences = new MerchantPreferences();

		$merchantPreferences->setReturnUrl(config('services.paypal.url.agreement.success'))
		    ->setCancelUrl(config('services.paypal.url.agreement.failure'))
		    ->setAutoBillAmount("yes")
		    ->setInitialFailAmountAction("CANCEL")
		    ->setMaxFailAttempts("1")
		    ->setSetupFee(new Currency(array('value' => 0, 'currency' => 'USD')));


		$plan->setPaymentDefinitions(array($paymentDefinition));

		$plan->setMerchantPreferences($merchantPreferences);

		try {
		    $plan = $plan->create($this->apiContext);
		    //$plan = $this->activate($plan->getId());
		} catch (Exception $ex) {
			echo $ex->getMessage();
			return;
		}
		
		return $plan;
	}

	public function activate($id){
		$createdPlan = $this->getPlan($id);
		try {
		    $patch = new Patch();

		    $value = new PayPalModel('{
			       "state":"ACTIVE"
			     }');

		    $patch->setOp('replace')
		        ->setPath('/')
		        ->setValue($value);
		    $patchRequest = new PatchRequest();
		    $patchRequest->addPatch($patch);

		    $createdPlan->update($patchRequest, $this->apiContext);

		    $plan = Plan::get($createdPlan->getId(), $this->apiContext);
		} catch (Exception $ex) {
			echo $ex->getMessage();
		}
		return $plan;
		
	}

	public function unactivate($id){
		$createdPlan = $this->getPlan($id);
		try {
		    $patch = new Patch();

		    $value = new PayPalModel('{
			       "state":"INACTIVE"
			     }');

		    $patch->setOp('replace')
		        ->setPath('/')
		        ->setValue($value);
		    $patchRequest = new PatchRequest();
		    $patchRequest->addPatch($patch);

		    $createdPlan->update($patchRequest, $this->apiContext);

		    $plan = Plan::get($createdPlan->getId(), $this->apiContext);
		} catch (Exception $ex) {
			echo $ex->getMessage();
		}
		return $plan;
		
	}

	public function showList(){
		$params = ['page_size' => '20','status' => 'active' ];
    	$planList = Plan::all($params, $this->apiContext);
    	return $planList->plans;
	}

	public function getPlan($id){
	    $plan = Plan::get($id, $this->apiContext);
	    return $plan;
	}

	public function delete($id){
		$createdPlan = Plan::get($id, $this->apiContext);
		$result = $createdPlan->delete($this->apiContext);
		return $result;
	}
}