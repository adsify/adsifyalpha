<?php 
namespace App\Paypal;

use PayPal\Rest\ApiContext;
use Illuminate\Support\Facades\App;

class Paypal{
	
	protected $apiContext;

	public function __construct()
	{	
		/*$this->apiContext = new ApiContext(
			new \PayPal\Auth\OAuthTokenCredential(
				config('services.paypal.sandbox.id'),
				config('services.paypal.sandbox.secret')
			)
		);*/

		$this->apiContext = new ApiContext(
			new \PayPal\Auth\OAuthTokenCredential(
				config('services.paypal.live.id'),
				config('services.paypal.live.secret')
			)
		);
		
		$this->apiContext->setConfig(
		    array(
		        'mode' => config('services.paypal.mode'),
		    )
		);
		
	}
    	
}