<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Server extends Model
{

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function pages(){
        return $this->belongsToMany('App\Page', 'server_pages', 'server_id', 'page_id')->select(['id', 'name']);
    }

    public function status(){
        return $this->belongsTo('App\Statu', 'status_code' ,'code');
    }

    public function profiles(){
        return $this->hasMany('App\Profile');
    }
}
