<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    public function posts(){
        return $this->belongsToMany('App\Post', 'post_country', 'country_id', 'post_id');
    }
}
