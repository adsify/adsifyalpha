<?php

namespace App\Policies;

use App\User;
use App\Page;
use Illuminate\Auth\Access\HandlesAuthorization;

class JournalPolicy
{
    use HandlesAuthorization;

    public function before($user, $ability){
        if($user->subscription->status_code != 302){
            return false;
        }
    }

    /**
     * Determine whether the user can view the ticket.
     *
     * @param  \App\User  $user
     * @param  \App\Page  $page
     * @return mixed
     */
    public function view(User $user, Page $page)
    {

        foreach($user->pages as $up){
            if($up->id == $page->id){
                return true;
            }
        }

        return false;
    }
     
}
