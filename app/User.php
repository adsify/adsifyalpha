<?php

namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
//implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'last', 'email', 'password', 
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function pages(){
        return $this->belongsToMany('App\Page', 'page_user', 'user_id','page_id');
    }

    public function subscription(){
        return $this->hasOne('App\Subscription');
    }

    public function archive_pages(){
        return $this->belongsToMany('App\Page', 'archive_page_user', 'user_id','page_id');
    }

    public function activity_logs(){
        return $this->hasMany('App\Activity_Log');
    }

    public function tickets(){
        return $this->hasMany('App\Ticket');
    }

    public function comments(){
        return $this->hasMany(Comment::class);
    }

    public function payments(){
        return $this->hasMany('App\Payment');
    }

    public function updator(){
        return $this->hasOne('App\Updator');
    }

    public function favorites(){
        return $this->belongsToMany('App\Post','bookmarks','user_id','post_id');
    }

    public function agreement(){
        return $this->hasOne('App\Agreement');
    }
}
