<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Page extends Model
{
    use SoftDeletes;
    public $incrementing = false;
    protected $hidden = array('pivot');

    public function posts(){
        return $this->hasMany('App\Post');
    }

    public function users(){
        return $this->belongsToMany('App\User', 'page_user', 'page_id', 'user_id');
    }

    public function servers(){
        return $this->belongsToMany('App\Server', 'server_pages', 'page_id', 'server_id');
    }

    public function status(){
        return $this->belongsTo('App\Statu', 'status_code' ,'code');
    }

    public function archive_users(){
        return $this->belongsToMany('App\User', 'archive_page_user', 'page_id','user_id');
    }

    public function postsIds(){
        return $this->hasMany('App\Post')->select(['id']);
    }

    public function similars(){
        return $this->hasMany('App\Similar');
    }

}
