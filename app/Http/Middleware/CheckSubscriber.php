<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Carbon\Carbon;
use App\Adsify\Manager;
use App\Paypal\CreateAgreement;

class CheckSubscriber
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        if(config('adsify.maintenance') == '1'){
            return redirect('maintenance');
        }else{
            $sub = Auth::user()->subscription;
            $user = Auth::user();
            if($sub->status_code == 900){
                return redirect('/banned');
            }else if($sub->status_code == 302){
                if($sub->service == "paypal"){
                    $days = $this->count_days($sub->last_payment);
                    if($days >= 31 && $days < 34){
                        Manager::updateSubscription($user,302,0,"pending");
                        session()->flash('warning', "Your payment is on verification , it may take 48 hours to verify. Please contact support if it takes any longer.");
                    }else if($days > 34){
                        Manager::updateSubscription($user,304,0,"free");
                        $sub->last_payment = null;
                        $sub->save();
                        session()->flash('warning', "You have no subscription activated at the moment,If you would like to support Adsify, Feel free to subscribe.");
                        return redirect('/journal/subscription');
                    }
                }else if($sub->service == "pending"){
                    $delay = $this->count_days($sub->updated_at);
                    if($delay >= config('adsify.delay')){
                        $agreement = new CreateAgreement();
                        $agreement->cancel($user->agreement->id);
                        $agr = $user->agreement;
                        $agr->state = "Cancel";
                        $agr->save();
                        Manager::updateSubscription($user,302,"0","cancel");
                        $params = [
                            'mail' => $user->email,
                            'name' => $user->name,
                        ];
                        Mailing::subscription($params,"canceled");
                        session()->flash('warning', "We have not received payment for your Adsify subscription. Please contact us immediately in order to ensure prompt payment of your subscription, and your continued satisfaction with your Adsify account.");
                    }else{
                         session()->flash('warning', "Your payment is on verification , it may take 48 hours to verify. Please contact support if it takes any longer.");
                    }
                }else if($sub->service == "cancel"){
                    $days = $this->count_days($sub->last_payment);
                    if($days >= 31){
                        Manager::updateSubscription($user,304,0,"archive");
                        $sub->last_payment = null;
                        $sub->save();
                        session()->flash('warning', "You have no subscription activated at the moment,If you would like to support Adsify, Feel free to subscribe.");
                        return redirect('/journal/subscription');
                    }
                }
            }else if($sub->status_code == 301){
                $days = $this->count_days($sub->updated_at);
                if($days >= config('adsify.trial')){
                    Manager::updateSubscription($user,304,0,"archive");
                    session()->flash('warning', "You have no subscription activated at the moment,If you would like to support Adsify, Feel free to subscribe.");
                    return redirect('/journal/subscription');
                }else{
                    $diff = config('adsify.trial') - $days;
                    session()->flash('trial',$diff);
                }
            }else if($sub->status_code == 304){
                session()->flash('warning', "You have no subscription activated at the moment,If you would like to support Adsify, Feel free to subscribe.");
                return redirect('/journal/subscription');
            }else if($sub->status_code == 303){
                session()->flash('warning', "Your account is locked, please contact support: support@adsify.io");
                return redirect('/journal/subscription');
            }
        }
        return $next($request);
    }

    public function count_days($updated){
        $now = Carbon::today();
        $updated = new Carbon($updated);
        $difference = $now->diffInDays($updated);
        return $difference;
    }

}
