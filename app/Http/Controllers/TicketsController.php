<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\mailers\AppMailer;
use App\Adsify\Mailing;
use App\Adsify\Manager;
use App\Category;
use App\Ticket;
use App\Comment;
use Auth;

class TicketsController extends Controller
{
    public function __construct(){
        $this->middleware('verified');
        $this->middleware('auth');
        $this->middleware('sub');
    }

    public function create(){
	    $categories = Category::all();
	    $tickets = Auth::user()->tickets()->paginate(10);
	    return view('journal.support', ['categories' => $categories , 'tickets' => $tickets]);
	}

	public function store(Request $request){
        try{
            $ticket = new Ticket();
            $ticket->title = $request->input('title');
            $ticket->user_id = Auth::user()->id;
            $ticket->ticket_id = strtoupper(str_random(10));
            $ticket->category_id = $request->input('category');
            $ticket->priority = $request->input('priority');
            $ticket->message = $request->input('message');
            $ticket->status_code = 601;
            $ticket->save();
            session()->flash('success', "A Ticket #-$ticket->ticket_id has been opened");
            
            $comment = Comment::create([
                'ticket_id' => $ticket->id,
                'user_id'    => Auth::user()->id,
                'comment'    => $request->input('message'),
            ]);

            $user = Auth::user();
            $params = [
                'mail' => $user->email,
                'name' => $user->name,
                'id' => $ticket->ticket_id,
            ];
            Mailing::ticket($params,"created");
            Mailing::adminNotification("support");
            return redirect('journal/tickets');
        }catch(Exception $ex){
            Manager::addLog(0,501,$ex->getMessage());
            return redirect('journal')->with('warning','Something went wrong! Please contact support.');
        }
	}

	public function show($id){
        try{
    	    $ticket = Ticket::find($id);
            $this->authorize('view', $ticket);
    	    $comments = $ticket->comments;
    	    return view('journal.support_detail', compact('ticket','comments'));
        }catch(Exception $ex){
            Manager::addLog(0,501,$ex->getMessage());
            return redirect('journal')->with('warning','Something went wrong! Please contact support.');
        }
	}

	public function postComment(Request $request){
        try{
            $comment = Comment::create([
                'ticket_id' => $request->input('ticket_id'),
                'user_id'    => Auth::user()->id,
                'comment'    => $request->input('comment'),
            ]);

            return redirect()->back()->with("success", "Your comment is submitted.");
        }catch(Exception $ex){
            Manager::addLog(0,501,$ex->getMessage());
            return redirect('journal')->with('warning','Something went wrong! Please contact support.');
        }
	}

    public function cancelTicket(Request $request){
        try{
            $ticket = Ticket::find($request->input('id'));
            $this->authorize('cancel', $ticket);
            $ticket->status_code = 603;
            $ticket->save();

            return redirect()->back()->with("warning", "Your Ticket : #$ticket->id has been canceled!");
        }catch(Exception $ex){
            Manager::addLog(0,501,$ex->getMessage());
            return redirect('journal')->with('warning','Something went wrong! Please contact support.');
        }
    }
}
