<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Srmklive\PayPal\Services\ExpressCheckout;
use Config;
use Auth;
use Validator;
use DOMDocument;
use DOMXpath;
use Mail;
use Carbon\Carbon;
use App\Adsify\Mailing;
use App\Adsify\Manager;
use App\Page;
use App\User;
use App\Post;
use App\Activity_Log;
use App\Subscription;
use App\Ticket;
use App\Task;
use App\Country;
use App\Updator;
use App\Log;
use App\NewPage;
use App\Server;
use App\Featured;

class UserController extends Controller
{
	public function __construct(){
		$this->middleware('auth');
        //$this->middleware('verified');
	}

	public function profile(){
        try{
            $user = Auth::user();
            return view('journal.profile',['user' => $user]);
        }catch(Exception $ex){
            Manager::addLog(0,501,$ex->getMessage());
            return redirect('journal')->with('warning','Something went wrong! Please contact support.');
        }
    }

    public function emailUpdate(Request $request){
        try{
            if($request->input('token') != null && $request->input('id') != null){
                $updator = Updator::find($request->input('id'));
                if($updator){
                    if($updator->token == $request->input('token')){
                        $user = Auth::user();
                        $newemail = $updator->email;
                        Manager::save_activity($request,"User email updated from $user->email to $newemail");
                        $params = [
                            'mail' => $user->email,
                            'name' => $user->name,
                            'update' => "Email Changed",
                        ];
                        Mailing::userNotification($params,'info');
                        $params2 = [
                            'mail' => $newemail,
                            'name' => $user->name,
                        ];
                        Mailing::userNotification($params2,'mails');
                        $user->email = $newemail;
                        $user->save();
                        $updator->delete();
                        return redirect('/journal/profile')->with('success','Email successfully updated');
                    }else{
                        return redirect('/journal/profile')->with('danger','Token invalid');
                    }
                }else{
                    return redirect('/journal/profile')->with('danger','Token invalid');
                }
            }
        }catch(Exception $ex){
            Manager::addLog(0,501,$ex->getMessage());
            return redirect('journal')->with('warning','Something went wrong! Please contact support.');
        }
    }

    public function emailCancel(){
        $updator = Auth::user()->updator;
        $updator->delete();
        return redirect('/journal/profile')->with('danger','Email update canceled');
    }

    public function profileUpdate(Request $request){

        $vnames = Validator::make(["first"=>Input::get("first"),"last"=>Input::get("last")],
            ["first"=>"required|string","last"=>"required|string"]);
        
        $vemail = Validator::make(["email"=>Input::get("email")],["email" => "required|email"]);

        $vp = Validator::make(["pass"=>Input::get("pass"),"new"=>Input::get("new"),"confirm"=>Input::get("confirm")],
            ["pass"=>"required|string","new"=>"required|string","confirm"=>"required|string"]);
        $user = Auth::user();
        $success = false;
        if($vnames->passes()){
            $up = false;
            if($user->name != $request->input('first')){
                $user->name = $request->input('first');
                $up = true;
            }
            if($user->last != $request->input('last')){
                $user->last = $request->input('last');
                $up = true;
            }
            $params = [
                'mail' => $user->email,
                'name' => $user->name,
                'update' => "First & Last name changed",
            ];
            if($up){
                Manager::save_activity($request,"User Name Updated");
                Mailing::userNotification($params,"info");
                $user->save();
                $success = true;
            }
            
        }

        if($vemail->passes()){
            if($user->email != $request->input('email')){
                $updator = new Updator();
                $updator->user_id = $user->id;
                $updator->email = $request->input('email');
                $updator->token = str_random(40);
                $updator->save();
                $params = [
                    'mail' => $updator->email,
                    'name' => $user->name,
                    'token' => $updator->token."&id=".$updator->id,
                ];
                Manager::save_activity($request,"Email Updated");
                Mailing::userNotification($params,"mailv");
                $success = true;
                session()->flash('warning','Please verify your new email to be valid!');
            }     
        }

        if(Input::get('pass') != null){
            if($vp->passes()){
                $pass = $request->input('pass');
                $new = $request->input('new');
                $confirm = $request->input('confirm');
                if (Hash::check($pass, $user->password)) {
                   if($new == $confirm){
                        $user->password = Hash::make($new);
                        $user->save();
                        $success = true;
                        $params = [
                            'mail' => $user->email,
                            'name' => $user->name,
                            'update' => "Password Changed",
                        ];
                        Manager::save_activity($request,"Password Updated");
                        Mailing::userNotification($params,"info");
                    }else{
                        $success = false;
                    }
                }else{
                    $success = false;
                }
            }else{
                $success = false;
            }
        }
        if($success){
            session()->flash('success','Profile updated successfully!');
        }else{
            session()->flash('warning','Informations you entered is wrong!');
        }
        return redirect('/journal/profile');
    }

    public function subscription(){
        try{
            $user = Auth::user();
            $sub = $user->subscription;
            $payments = $user->payments()->orderBy('created_at','desc')->get();
            return view('journal.subscription', ['sub' => $sub , 'user' => $user , 'payments' => $payments]);
        }catch(Exception $ex){
            Manager::addLog(0,501,$ex->getMessage());
            return redirect('journal')->with('warning','Something went wrong! Please contact support.');
        }
    }
}
