<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Srmklive\PayPal\Services\ExpressCheckout;
use App\Http\Resources\PageResource;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use Cookie;
use Config;
use Auth;
use Validator;
use DOMDocument;
use DOMXpath;
use App\Mail\AdsifyMail;
use Carbon\Carbon;
use App\Adsify\Mailing;
use App\Adsify\Manager;
use App\Page;
use App\User;
use App\Post;
use App\Activity_Log;
use App\Subscription;
use App\Ticket;
use App\Task;
use App\Country;
use App\Updator;
use App\Log;
use App\NewPage;
use App\Server;
use App\Featured;
use App\Similar;



class JournalController extends Controller
{

	public function __construct(){
		$this->middleware('auth');
        $this->middleware('sub');
        //$this->middleware('verified');
	}

    public function test(){

    }

    public function downloader($url,$name,$type){
        if($type == "image"){
            file_put_contents("storage/image/" . trim($name) . ".png", file_get_contents($url));
            return 'storage/image/'. $name . ".png";
        }else if($type == "video"){
            file_put_contents("storage/video/" . trim($name) . ".mp4", file_get_contents($url));
            return 'storage/video/'. $name . ".mp4";
        }
    }

    // ---------------- JOURNAL SECTION ---------------

    public function index(){
        try{
            $listposts = new Collection();
            $pages = Auth::user()->pages;
            $days = [];
            $countries = Country::all();
            if(count($pages) > 0){
                if(isset($_COOKIE["filter"]) && $_COOKIE["filter"] == "1"){
                    $listposts = $this->doFilter();
                }else{
                    foreach ($pages as $page) {
                        foreach($page->posts as $post){
                            $listposts->push($post);
                        }
                    }
                    $listposts = $listposts->sortByDesc('updated_at');
                }
                foreach($listposts as $post){
                    $days[$post->id] = $this->count_days($post->created_at,$post->stats->updated_at);
                }
                return view('journal.index', ['posts' => $listposts->paginate(30), 'days' => $days , 'countries' => $countries,'filter'=> $pages , 'journal' => '1']);
            }else{
                return view('journal.index', ['journal' => '1']);
            }
        }catch(Exception $ex){
            Manager::addLog(0,501,$ex->getMessage());
            return redirect('errors.500')->with('warning','Something went wrong! Please contact support.');
        }
    }

    public function details($id){
        try{
            $post = Post::find($id);
            $this->authorize("view",$post->page);
            $post['shop'] = $this->getOutgoingUrl($post->text);
            $days = $this->count_days($post->created_at,$post->stats->updated_at);
            return view('journal.details' , ['post' => $post , 'journal' => '1','days' => $days]);
        }catch(Exception $ex){
            Manager::addLog(0,501,$ex->getMessage());
            return redirect('journal')->with('warning','Something went wrong! Please contact support.');
        }
    }

    public function getOutgoingUrl($text){
        preg_match_all('/(https[a-zA-Z0-9:\/._-]*)|(www[a-zA-Z0-9:\/._-]*)/', $text,$matches);
        $url = "";
        if(sizeof($matches) > 0)
            if(sizeof($matches[1]) > 0)
                $url = $matches[1][0];
        return $url;
    }

    // ---------------- FILTER SECTION ------------

    public function resetFilter(){
        setcookie("filter", "0", time() + (86400 * 30 * 12), "/");
        setcookie("page", "0", time() + (86400 * 30 * 12), "/");
        setcookie("sort", "0", time() + (86400 * 30 * 12), "/");
        setcookie("date", "0", time() + (86400 * 30 * 12), "/");
        setcookie("country", "-1", time() + (86400 * 30 * 12), "/");
        setcookie("type", "0", time() + (86400 * 30 * 12), "/");
        setcookie("status", "0", time() + (86400 * 30 * 12), "/");
        return redirect('/journal');
    }

    public function filter(Request $request){
        try{
            $page = trim($request->input('page'));
            $sort = trim($request->input('sort'));
            $date = trim($request->input('date'));
            $country = trim($request->input('country'));
            $type = trim($request->input('type'));
            $status = trim($request->input('status'));
            $countries = Country::all();
            if($page == '' && $sort == '' && $date == '' && $country == '' && $type == '' && $status == ''){
                return $this->resetFilter();
            }

            $page = ($page == '') ? "0" : $page ;
            $sort = ($sort == '') ? "0" : $sort ;
            $date = ($date == '') ? "0" : $date ;
            $country = ($country == '') ? "-1" : $country ;
            $type = ($type == '') ? "0" : $type ;
            $status = ($status == '') ? "0" : $status ;

            setcookie("filter", "1", time() + (86400 * 30 * 12), "/");
            setcookie("page", $page, time() + (86400 * 30 * 12), "/");
            setcookie("sort", $sort, time() + (86400 * 30 * 12), "/");
            setcookie("date", $date, time() + (86400 * 30 * 12), "/");
            setcookie("country", $country, time() + (86400 * 30 * 12), "/");
            setcookie("type", $type, time() + (86400 * 30 * 12), "/");
            setcookie("status", $status, time() + (86400 * 30 * 12), "/");

            return redirect('/journal');
           // return view('journal.index', ['posts' => $posts, 'countries' => $countries,'days' => $days , 'filter'=> $pages , 'journal' => '2']);
        }catch(Exception $ex){
            Manager::addLog(0,501,$ex->getMessage());
            return redirect('journal')->with('warning','Something went wrong! Please contact support.');
        }
    }

    public function doFilter(){
        $page = $_COOKIE["page"];
        $sort = $_COOKIE["sort"];
        $date = $_COOKIE["date"];
        $country = $_COOKIE["country"];
        $type = $_COOKIE["type"];
        $status = $_COOKIE["status"];

        $posts = new Collection();
        if($page != '0'){
            if($country != '-1'){
                if($sort != '0'){
                    if($date != '0'){
                        $posts = $this->getPagePosts($page,$date,$sort,$country);
                    }else{
                        $posts = $this->getPagePosts($page,'',$sort,$country);
                    }
                }else if($date != '0'){
                    $posts = $this->getPagePosts($page,$date,'',$country);
                }else{
                    $posts = $this->getPagePosts($page,'','',$country);
                }
            }else if($sort != '0'){
                if($date != '0'){
                    $posts = $this->getPagePosts($page,$date,$sort);
                }else{
                    $posts = $this->getPagePosts($page,'',$sort);
                }
            }else if($date != '0'){
                $posts = $this->getPagePosts($page,$date);
            }else{
                $posts = $this->getPagePosts($page);
            }
        }else if($country != '-1'){
            if($sort != ''){
                if($date != '0'){
                    $posts = $this->getUserPosts($date,$sort,$country);
                }else{
                    $posts = $this->getUserPosts('',$sort,$country);
                }
            }else if($date != '0'){
                $posts = $this->getUserPosts($date,'',$country);
            }else{
                $posts = $this->getUserPosts('','',$country);
            }
        }else if($sort != '0'){
            if($date != '0'){
                $posts = $this->getUserPosts($date,$sort);
            }else{
                $posts = $this->getUserPosts('',$sort);
            }
        }else if($date != '0'){
            $posts = $this->getUserPosts($date);
        }

        if($type != '0'){
            if(sizeof($posts) == 0){
                $posts = $this->getUserPosts();
                $posts = $this->filterBy($posts,$type,"");
            }else{
                $posts = $this->filterBy($posts,$type,"");
            }
        }

        if($status != '0'){
            if(sizeof($posts) == 0){
                $posts = $this->getUserPosts();
                $posts = $this->filterBy($posts,"",$status);
            }else{
                $posts = $this->filterBy($posts,"",$status);
            }
        }
        return $posts;
    }

    public function getPagePosts($page,$date = '',$sort = '',$country_id = ''){
        $page = Page::find($page);
        $posts = $page->posts;
        if($country_id != ''){
            $posts = $posts->filter(function($post) use ($country_id){
                $countries = $post->countries->filter(function($country) use ($country_id){
                    if($country->id == $country_id)
                        return true;
                    else
                        return false;
                });
                if(sizeof($countries) > 0)
                    return true;
                else
                    return false;
            });
        }
        if($date != ''){
            $fdate = Carbon::parse($date);
            $posts = $posts->filter(function($post) use ($fdate){
                $pd = Carbon::parse($post->updated_at);
                $diff = $pd->greaterThan($fdate);
                if($pd->greaterThan($fdate) || $pd->equalTo($fdate)){
                    return true;
                }else{
                    return false;
                }
            });
        }
        if($sort != ''){
            $posts = $posts->sortByDesc(function($post) use($sort){
                if($sort == 'likes'){
                    return trim($post->stats->likes);
                }else if($sort == 'comments'){
                    return trim($post->stats->comments);
                }else{
                    return trim($post->stats->shares);
                }
            });
        }
        return $posts;
    }

    public function getUserPosts($date = '',$sort = '',$country_id = ''){
        $pages = Auth::user()->pages;
        $data = new Collection();
        if(count($pages) != 0){
            foreach ($pages as $page) {
                $posts = $page->posts;
                if($country_id != ''){
                    $posts = $posts->filter(function($post) use ($country_id){
                        $countries = $post->countries->filter(function($country) use ($country_id){
                            if($country->id == $country_id)
                                return true;
                            else
                                return false;
                        });
                        if(sizeof($countries) > 0)
                            return true;
                        else
                            return false;
                    });
                }
                if($date != ''){
                    $fdate = Carbon::parse($date);
                    $posts = $posts->filter(function($post) use ($fdate){
                        $pd = Carbon::parse($post->updated_at);
                        if($pd->greaterThan($fdate) || $pd->equalTo($fdate)){
                            return true;
                        }else{
                            return false;
                        }
                    });
                }
                $data = $data->merge($posts);
            }

            if($sort != ''){

                $data = $data->sortByDesc(function($post) use($sort){
                    if($sort == 'likes'){
                        return trim($post->stats->likes);
                    }else if($sort == 'comments'){
                        return trim($post->stats->comments);
                    }else{
                        return trim($post->stats->shares);
                    }
                });
            }
            return $data;
        }
        return 0;
    }

    public function filterBy($posts,$type,$status){
        if($type != ""){
            $posts = $posts->filter(function($post) use ($type){
                if($post->type == $type){
                    return true;
                }else{
                    return false;
                }

            });
        }
        if($status != ""){
            $posts = $posts->filter(function($post) use ($status){
                if($post->status_code == $status){
                    return true;
                }else{
                    return false;
                }

            });
        }
        return $posts;
    }

    // ------------- PAGES SECTION ---------------

    public function archivePage(Request $request){
        $page = Page::find($request->input('id'));
        $page->users()->detach(Auth::user()->id);
        if(!Auth::user()->archive_pages->contains($page->id)){
            $page->archive_users()->attach(Auth::user()->id);
        }
        if(count($page->users) == 0){
            $page->status_code = 104;
            $page->save();
        }
        return redirect('journal/pages');
    }

    public function unarchivePage(Request $request){
        $page = Auth::user()->archive_pages()->find($request->input('id'));
        Auth::user()->archive_pages()->detach($page->id);
        if(!Auth::user()->pages->contains($page->id)){
            Auth::user()->pages()->attach($page->id);
        }
        if($page->status_code == 104){
            $page->status_code = 102;
            $page->save();
        }
        return redirect('journal/pages');
    }

    public function showPages(){
        $listpages = Auth::user()->pages;
        $archives = Auth::user()->archive_pages()->paginate(20);
        $featureds = Featured::all();
        return view('journal.pages', ['pages' => $listpages , 'archives' => $archives , 'page' => '0','featureds' => $featureds]);
        try{
        }catch(Exception $ex){
            Manager::addLog(0,501,$ex->getMessage());
            return redirect('journal')->with('warning','Something went wrong! Please contact support.');
        }
    }

    public function getPages(){
        $listpages = Auth::user()->pages()->with('status')->with('posts')->get();
        return $listpages;
    }

    public function getSimilars($id){
        $page = Page::find($id);
        $similars = $page->similars;
        return $similars;
    }

    public function storeSimilar(Request $request){
        $user = Auth::user();
        $id = $request->input('id');
        $name = $request->input('name');
        $logo = $request->input('logo');
        $checkPage = Page::find($id);
        if(!$checkPage){
            $page = new Page();
            $page->id = $id;
            $page->name = $name;
            $page->logo_src = $logo;
            $page->status_code = 101;
            $page->save();
            $page->users()->attach($user->id);
        }else{
            $archives = $user->archive_pages;
            if($archives->contains($id)){
                session()->flash('warning', 'You already have this page archived..Please unarchive it.');
                    return redirect('journal/pages');
            }
            if(!$user->pages->contains($id)){
                $checkPage->users()->attach($user->id);
                if($checkPage->status_code == 104){
                    $checkPage->status_code = 102;
                    $checkPage->save();
                }
            }else{
                session()->flash('warning', 'You already have this page added..');
                return redirect('journal/pages');
            }
        }
        session()->flash('success', 'Similar Page Added Successfully');
        Manager::save_activity($request,"Add Similar Page");
        return redirect('journal/pages');
    }

    public function storePage(Request $request){
        try{
            $user = Auth::user();
            $total = sizeof($user->pages);
            $pagelink = $this->cleanLink($request->input('link'));

            if($total == config('adsify.free') && $user->subscription->status_code != 302){
                 session()->flash('warning', 'Sorry,'.config('adsify.free').' Pages is the limit for free users, you can add more pages when you subscribe.');
                return redirect('journal/pages');
            }
            $pagedata = $this->getpageinfo($pagelink);
            if($request->input('name')){
                $pagedata['name'] = $request->input('name');
            }

            if($pagedata == 0) {
                session()->flash('warning', 'Your Page is not found. Please check the link again or contact the support');
                return redirect('journal/pages');
            }else{
                $checkPage = Page::find($pagedata["id"]);
                $user = Auth::user();
                $page = new Page();
                if(!$checkPage){
                    //$pagedata = $this->getpagedata($pagelink);
                    $page->id = $pagedata['id'];
                    $page->name = $pagedata["name"];
                    if($this->getPageLogo($pagedata["id"])){
                        $page->logo_src = "storage/image/".$pagedata["id"].".png";
                    }else{
                        $page->logo_src = "storage/image/nopage.png";
                    }
                    $page->status_code = 101;
                    $page->save();
                    $page->users()->attach($user->id);

                    $newPage = new NewPage();
                    $newPage->page_id = $page->id;
                    $newPage->type  = "page";
                    $newPage->save();
                    if($user->subscription->status_code == 302){
                        $newPage = new NewPage();
                        $newPage->page_id = $page->id;
                        $newPage->type  = "similar";
                        $newPage->save();
                    }
                }else{
                    $archives = $user->archive_pages;
                    if($archives->contains($pagedata["id"])){
                         session()->flash('warning', 'You already have this page archived..Please unarchive it.');
                            return redirect('journal/pages');
                    }
                    if(!$user->pages->contains($checkPage->id)){
                        $checkPage->users()->attach($user->id);
                        if($checkPage->status_code == 104){
                            $checkPage->status_code = 102;
                            $checkPage->save();
                        }
                    }else{
                        session()->flash('warning', 'You already have this page added..');
                        return redirect('journal/pages');
                    }
                }
                Manager::save_activity($request,"Add Page");
                session()->flash('success', 'Your Page has been added successfully');
                return redirect('journal/pages');
            }
        }catch(Exception $ex){
            Manager::addLog(0,501,$ex->getMessage());
            return view('journal')->with('warning','Something went wrong! Please contact support.');
        }
    }

    public function cleanLink($link){
        $final = "";
        $fb = "https://www.facebook.com/";
        if(strpos($link,"facebook") && strlen($link) > 25){
            $final = $link;
        }else{
           $final = $fb . $link;
        }
        $final = str_replace("pg/", "", $final);
        $final = preg_replace('/ads.+/', "", $final);
        $final = preg_replace('/\?__tn.+/', "", $final);
        $final = str_replace(" ", "", $final);
        return $final;
    }

    public function getpageinfo($fburl){
        try{
            $url = str_replace("www", "m", $fburl);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.3) Gecko/20070309 Firefox/2.0.0.3");
            $result = curl_exec($ch) or die(curl_error($ch));
            $dom = new DOMDocument();
            libxml_use_internal_errors(true);
            $dom->loadHTML($result);
            $divs = $dom->getElementsByTagName('a');
            $id = "";
            foreach ($divs as $div) {
                foreach ($div->attributes as $attr) {
                    $name = $attr->nodeName;
                    $value = $attr->nodeValue;
                    if($name == "href" && strpos($value,"messages/thread")){
                        $id = explode("/",$value)[3];
                    }
                }
            }
            $spans = $dom->getElementsByTagName('span');
            $pname = "";
            foreach ($spans as $span) {
                foreach ($span->attributes as $attr) {
                    $name = $attr->nodeName;
                    $value = $attr->nodeValue;

                    if($name == "class" && ($value == "by"  || $value == "bu") && $span->textContent != ""){
                        $pname = $span->textContent;
                    }
                }
            }
            $rs = $this->getpagedata($fburl);
            if($id == ""){
                $id = $rs[0];
            }
            if($pname == ""){
                $pname = $rs[1];
            }
            
            $data = ["id" => $id, "name" => $pname];
            return $data;
        }catch(Exception $ex){
            Manager::addLog(0,501,$ex->getMessage());
            return redirect('journal')->with('warning','Something went wrong! Please contact support.');
        }
    }

    public function getPageLogo2($id){
        try {
            $fb = new \Facebook\Facebook([
              'app_id' => '287449085238857',
              'app_secret' => 'a452ab0406a32ecbe2060c197505552c',
              'default_graph_version' => 'v3.2',
              'default_access_token' => 'EAAEFbvSd3kkBAOOzXazwHB3pGZCEcsZBQGVxLOOb7oOXlF11mJYzp8nJut2hvoFoZAUdkwIQQcGGYZBicrLtZCYxFl7UxTr3ylip549ItR4KSyTmPP21pxDoFrSjUdiptZA9NcoVNnZBIhC9UUZBEHRWrgk63mfAZCjKK7nW8aYATzwZDZD'
            ]);
            
            $response = $fb->get(
                "/".$id."/picture?redirect=0&height=300"
            );
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
          echo 'Graph returned an error: ' . $e->getMessage();
          return false;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
          echo 'Facebook SDK returned an error: ' . $e->getMessage();
          return false;
        } 
        $graphNode = $response->getGraphNode();
        $path = "storage/image/$id.png";
        $this->download_image($graphNode['url'],$path);
        return true;
    }

    public function getPageLogo($id){
        $token = "EAAEFbvSd3kkBAOOzXazwHB3pGZCEcsZBQGVxLOOb7oOXlF11mJYzp8nJut2hvoFoZAUdkwIQQcGGYZBicrLtZCYxFl7UxTr3ylip549ItR4KSyTmPP21pxDoFrSjUdiptZA9NcoVNnZBIhC9UUZBEHRWrgk63mfAZCjKK7nW8aYATzwZDZD";
        $url = "https://graph.facebook.com/".$id."/picture?redirect=0&height=300&access_token=".$token;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.3) Gecko/20070309 Firefox/2.0.0.3");
        $result = curl_exec($ch) or die(curl_error($ch));
        $res = json_decode($result, false);
        $path = "storage/image/".$id.".png";
        if(isset($res->data)){
            $this->download_image($res->data->url,$path);
            return true;
        }else{
            return false;
        }
        
    }

    public function download_image($url,$name){
        $ch = curl_init($url);
        $fp = fopen($name, 'wb');
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_exec($ch);
        curl_close($ch);
        fclose($fp);
    }

    public function getpagedata($fburl){
        try{
            $pagedata = [];
            $url = "https://api.fbid.varal7.fr/?url=".$fburl;
            $data = json_decode(file_get_contents($url));
            array_push($pagedata, $data->id);
            if (preg_match('/-/',$fburl)){
                $data = preg_split('/-/', $fburl);
                $data = preg_split('/.com/', $data[0]);
                $name = str_replace('/',"",$data[1]);
                array_push($pagedata, $name);
            }else{
                $data = preg_split('/.com/', $fburl);
                $name = str_replace('/',"",$data[1]);
                array_push($pagedata, $name);
            }
            return $pagedata;
        }catch(Exception $ex){
            Manager::addLog(0,501,$ex->getMessage());
            return redirect('journal')->with('warning','Something went wrong! Please contact support.');
        }
    }

    // --------------- BOOKMARKS SECTION ---------------

    public function bookmarks(){
        try{
            $posts = Auth::user()->favorites()->paginate(18);
            $days = [];
            foreach($posts as $post){
                $days[$post->id] = $this->count_days($post->created_at,$post->updated_at);
            }
        return view('journal.bookmarks', ['posts' => $posts,'days' => $days , 'bookmark' => '1']);
        }catch(Exception $ex){
            Manager::addLog(0,501,$ex->getMessage());
            return redirect('journal')->with('warning','Something went wrong! Please contact support.');
        }
    }

    public function addBookmark($id){
        try{
            if(!Auth::user()->favorites->contains($id)){
                Auth::user()->favorites()->attach($id);
                return '1';
            }else{
                return '0';
            }
        }catch(Exception $ex){
            Manager::addLog(0,501,$ex->getMessage());
            return redirect('journal')->with('warning','Something went wrong! Please contact support.');
        }
    }

    public function cancelBookmark($id){
        try{
            if(Auth::user()->favorites->contains($id)){
                Auth::user()->favorites()->detach($id);
                return '1';
            }
            return '0';
        }catch(Exception $ex){
            Manager::addLog(0,501,$ex->getMessage());
            return view('journal')->with('warning','Something went wrong! Please contact support.');
        }
    }

    // ---------------- PROFILE SECTION ---------------

    

    public function count_days($create,$update){
        $created = new Carbon($create);
        $updated = new Carbon($update);
        $difference = ($created->diff($updated)->days < 1)
            ? '1'
            : $created->diffInDays($updated);
        return $difference;
    }



}
