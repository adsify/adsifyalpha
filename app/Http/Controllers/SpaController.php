<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Srmklive\PayPal\Services\ExpressCheckout;
use App\Http\Resources\PageResource;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use Cookie;
use Config;
use Auth;
use Validator;
use DOMDocument;
use DOMXpath;
use App\Mail\AdsifyMail;
use Carbon\Carbon;
use App\Adsify\Mailing;
use App\Adsify\Manager;
use App\Page;
use App\User;
use App\Post;
use App\Activity_Log;
use App\Subscription;
use App\Ticket;
use App\Task;
use App\Country;
use App\Updator;
use App\Log;
use App\NewPage;
use App\Server;
use App\Featured;
use App\Similar;
use App\Category;
use App\Comment;

class SpaController extends Controller
{
    public function posts(){
        $listposts = new Collection();
        $pages = Auth::user()->pages;
        if(count($pages) > 0){
            foreach ($pages as $page) {
                foreach($page->posts as $post){
                    $listposts->push($post->where('id','=',$post->id)->with('page')->with('status')->with('stats')->first());
                }
            }
            if(count($listposts)){
                return response()->json(["posts" => $listposts], 200);
            }
            return response()->json(["posts" => '1'], 200);
        }
        return response()->json(["posts" => '0'], 200);
    }

    public function getPost($id){
        $post = Post::find($id)->with('page')->with('status')->with('countries')->where('id','=',$id)->get();
        return response()->json([
            "post" => $post[0]
        ], 200);
    }

    public function pages(Request $request){
        $listpages = Auth::user()->pages()->with('status')->with('posts')->get();
        $archived = Auth::user()->archive_pages()->with('status')->with('posts')->get();
        return response()->json(["pages" => $listpages, "archived" => $archived], 200);
    }

    public function addPage(Request $request){
        $user = Auth::user();
        $pagelink = $this->cleanLink($request->input('link'));
        $pagedata = $this->getpageinfo($pagelink);
        if($pagedata == -1) {
            return response()->json(["page" => '0',"message" => 'Your Page is not found. Please check the link again or contact the support'], 200);
        }else{
            $checkPage = Page::find($pagedata["id"]);
            $user = Auth::user();
            $page = new Page();
            if(!$checkPage){
                $page->id = $pagedata['id'];
                $page->name = $pagedata["name"];
                if($this->getPageLogo($pagedata["id"])){
                    $page->logo_src = "storage/image/".$pagedata["id"].".png";
                }else{
                    $page->logo_src = "storage/image/nopage.png";
                }
                $page->status_code = 101;
                $page->save();
                $page->users()->attach($user->id);
                $checkPage = $page;
            }else{
                if(!$user->pages->contains($checkPage->id)){
                    $checkPage->users()->attach($user->id);
                    if($checkPage->status_code == 104){
                        $checkPage->status_code = 102;
                        $checkPage->save();
                    }
                }else{
                    return response()->json(["page" => '0',"message" => 'You already have this page added..'], 200);
                }
            }
            Manager::save_activity($request,"Add Page");
            return response()->json(["page" => '1',"message" => 'Your Page has been added successfully'], 200);
        }
    }

    public function bookmarks(){
        try{
            $posts = Auth::user()->favorites();
            return response()->json(["bookmarks" => $posts], 200);
        }catch(Exception $ex){
            Manager::addLog(0,501,$ex->getMessage());
            return response()->json(["bookmarks" => '0','message' => 'Something went wrong! Please contact support.'], 200);
        }
    }

    public function addBookmark(){
        if(!Auth::user()->favorites->contains($id)){
            Auth::user()->favorites()->attach($id);
            return response()->json(["bookmarks" => '1','message' => 'Post Added Successfully'], 200);
        }else{
            return response()->json(["bookmarks" => '0','message' => 'Post Already Bookmarked'], 200);
        }
    }

    public function rmvBookmark(){
        if(Auth::user()->favorites->contains($id)){
            Auth::user()->favorites()->detach($id);
            return response()->json(['message' => 'Bookmark Removed Successfully'], 200);
        }
    }
    
    public function cleanLink($link){
        $final = "";
        $fb = "https://www.facebook.com/";
        if(strpos($link,"facebook") && strlen($link) > 25){
            $final = $link;
        }else{
           $final = $fb . $link;
        }
        $final = str_replace("pg/", "", $final);
        $final = preg_replace('/ads.+/', "", $final);
        $final = preg_replace('/\?__tn.+/', "", $final);
        $final = str_replace(" ", "", $final);
        return $final;
    }

    public function getpageinfo($fburl){
            $url = str_replace("www", "m", $fburl);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.3) Gecko/20070309 Firefox/2.0.0.3");
            $result = curl_exec($ch) or die(curl_error($ch));
            $dom = new DOMDocument();
            libxml_use_internal_errors(true);
            $dom->loadHTML($result);
            $divs = $dom->getElementsByTagName('a');
            $id = "";
            foreach ($divs as $div) {
                foreach ($div->attributes as $attr) {
                    $name = $attr->nodeName;
                    $value = $attr->nodeValue;
                    if($name == "href" && strpos($value,"messages/thread")){
                        $id = explode("/",$value)[3];
                    }
                }
            }
            $spans = $dom->getElementsByTagName('span');
            $pname = "";
            foreach ($spans as $span) {
                foreach ($span->attributes as $attr) {
                    $name = $attr->nodeName;
                    $value = $attr->nodeValue;

                    if($name == "class" && ($value == "by"  || $value == "bu") && $span->textContent != ""){
                        $pname = $span->textContent;
                    }
                }
            }
            $rs = $this->getpagedata($fburl);
            if($id == ""){
                $id = $rs[0];
            }
            if($pname == ""){
                $pname = $rs[1];
            }
            
            $data = ["id" => $id, "name" => $pname];
            return $data;
    }

    public function getPageLogo($id){
        $token = "EAAEFbvSd3kkBAOOzXazwHB3pGZCEcsZBQGVxLOOb7oOXlF11mJYzp8nJut2hvoFoZAUdkwIQQcGGYZBicrLtZCYxFl7UxTr3ylip549ItR4KSyTmPP21pxDoFrSjUdiptZA9NcoVNnZBIhC9UUZBEHRWrgk63mfAZCjKK7nW8aYATzwZDZD";
        $url = "https://graph.facebook.com/".$id."/picture?redirect=0&height=300&access_token=".$token;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.3) Gecko/20070309 Firefox/2.0.0.3");
        $result = curl_exec($ch) or die(curl_error($ch));
        $res = json_decode($result, false);
        $path = "storage/image/".$id.".png";
        if(isset($res->data)){
            $this->download_image($res->data->url,$path);
            return true;
        }else{
            return false;
        }
        
    }

    public function download_image($url,$name){
        $ch = curl_init($url);
        $fp = fopen($name, 'wb');
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_exec($ch);
        curl_close($ch);
        fclose($fp);
    }

    public function getpagedata($fburl){
        $pagedata = [];
        $url = "https://api.fbid.varal7.fr/?url=".$fburl;
        $data = json_decode(file_get_contents($url));
        array_push($pagedata, $data->id);
        if (preg_match('/-/',$fburl)){
            $data = preg_split('/-/', $fburl);
            $data = preg_split('/.com/', $data[0]);
            $name = str_replace('/',"",$data[1]);
            array_push($pagedata, $name);
        }else{
            $data = preg_split('/.com/', $fburl);
            $name = str_replace('/',"",$data[1]);
            array_push($pagedata, $name);
        }
        return $pagedata;
    }

    public function tickets(){
        $categories = Category::all();
        $tickets = Auth::user()->tickets()->with('status')->with('category')->with('comments')->get();
        return response()->json(['tickets' => $tickets, 'categories' => $categories], 200);
    }

    public function addTicket(Request $request){
        $ticket = new Ticket();
        
        $ticket->title = $request->ticket['title'];
        $ticket->user_id = Auth::user()->id;
        $ticket->ticket_id = strtoupper(str_random(10));
        $ticket->category_id = $request->ticket['category'];
        $ticket->priority = $request->ticket['priority'];
        $ticket->message = $request->ticket['message'];
        $ticket->status_code = 601;
        $ticket->save();
        $message = "A Ticket #-$ticket->ticket_id has been opened";
        $ticket = $ticket->where('id','=',$ticket->id)->with('status')->with('category')->with('comments')->first();
        $comment = Comment::create([
            'ticket_id' => $ticket->id,
            'user_id'    => Auth::user()->id,
            'comment'    => $request->ticket['message'],
        ]);

        $user = Auth::user();
        $params = [
            'mail' => $user->email,
            'name' => $user->name,
            'id' => $ticket->ticket_id,
        ];
        //Mailing::ticket($params,"created");
        //Mailing::adminNotification("support");
        return response()->json(['ticket' => $ticket, 'message' => $message]);
    }

    public function comments($id){
        $ticket = Ticket::where('ticket_id','=',$id)->first();
        $comments = $ticket->comments()->with('user')->where('ticket_id','=',$ticket->id)->get();
        return response()->json(['ticket' => $ticket,'comments' => $comments]);
	}

	public function addComment(Request $request){
        $comment = Comment::create([
            'ticket_id' => $request['id'],
            'user_id'    => Auth::user()->id,
            'comment'    => $request['message'],
        ]);
        $comment = $comment->with('user')->where('id','=',$comment->id)->first();
        return response()->json(['message' => 'Comment submited successfully','comment' => $comment]);
    }
    
    public function getUser(){
        $user = Auth::user();
        $updator = $user->updator;
        return response()->json(['user' => $user,'updator' => $updator]);
    }

    public function emailUpdate(Request $request){
        try{
            if($request->input('token') != null && $request->input('id') != null){
                $updator = Updator::find($request->input('id'));
                if($updator){
                    if($updator->token == $request->input('token')){
                        $user = Auth::user();
                        $newemail = $updator->email;
                        Manager::save_activity($request,"User email updated from $user->email to $newemail");
                        $params = [
                            'mail' => $user->email,
                            'name' => $user->name,
                            'update' => "Email Changed",
                        ];
                        Mailing::userNotification($params,'info');
                        $params2 = [
                            'mail' => $newemail,
                            'name' => $user->name,
                        ];
                        Mailing::userNotification($params2,'mails');
                        $user->email = $newemail;
                        $user->save();
                        $updator->delete();
                        return redirect('/journal/profile')->with('success','Email successfully updated');
                    }else{
                        return redirect('/journal/profile')->with('danger','Token invalid');
                    }
                }else{
                    return redirect('/journal/profile')->with('danger','Token invalid');
                }
            }
        }catch(Exception $ex){
            Manager::addLog(0,501,$ex->getMessage());
            return redirect('journal')->with('warning','Something went wrong! Please contact support.');
        }
    }

    public function emailCancel(){
        $updator = Auth::user()->updator;
        $updator->delete();
        return response()->json(['message' => "Email update canceled"]);
    }

    public function profileUpdate(Request $request){

        $vnames = Validator::make(["first"=>$request->user['fname'],"last"=>$request->user['lname']],
            ["first"=>"required|string","last"=>"required|string"]);
        
        $vemail = Validator::make(["email"=>$request->user['email']],["email" => "required|email"]);

        $vp = Validator::make(["pass"=>$request->user['oldpass'],"new"=>$request->user['newpass']],
            ["pass"=>"required|string","new"=>"required|string"]);

        $user = Auth::user();
        $success = false;
        $message = "";
        $verify = "";
        if($vnames->passes()){
            $up = false;
            if($user->name != $request->user['fname']){
                $user->name = $request->user['fname'];
                $up = true;
            }
            if($user->last != $request->user['lname']){
                $user->last = $request->user['lname'];
                $up = true;
            }
            if($up){
                $user->save();
            }
            $success = true;
        }

        if($vemail->passes()){
            if($user->email != $request->user['email']){
                $updator = new Updator();
                $updator->user_id = $user->id;
                $updator->email = $request->user['email'];
                $updator->token = str_random(40);
                $updator->save();
                $params = [
                    'mail' => $updator->email,
                    'name' => $user->name,
                    'token' => $updator->token."&id=".$updator->id,
                ];
                //Manager::save_activity($request,"Email Updated");
                //Mailing::userNotification($params,"mailv");
                $success = true;
                //session()->flash('warning','Please verify your new email to be valid!');
                $message = "Please Verify your new email to be valid!";
                $verify = $request->user['email'];
            }     
        }

        if($request->user['oldpass'] != null){
            if($vp->passes()){
                $pass = $request->user['oldpass'];
                $new = $request->user['newpass'];
                $confirm = $request->user['confirmpass'];
                if (Hash::check($pass, $user->password)) {
                   if($new == $confirm){
                        $user->password = Hash::make($new);
                        $user->save();
                        $success = true;
                        //Manager::save_activity($request,"Password Updated");
                        //Mailing::userNotification($params,"info");
                    }else{
                        $success = false;
                    }
                }else{
                    if($message == ''){
                        $message = "Your old password is incorrect";
                    }
                    $success = false;
                }
            }else{
                $success = false;
            }
        }
        $update = 0;
        if($success){
            if($message == ''){
                $message = "Profile updated successfully";
            }
            $update = 1;
        }else{
            if($message == '')
                $message = "Information entered was not right.";
        }
        return response()->json(['update' => $update,'message' => $message,'verify' => $verify]);
    }

    public function archivePage(Request $request){
        $page = Page::find($request->id);
        $page->users()->detach(Auth::user()->id);
        if(!Auth::user()->archive_pages->contains($page->id)){
            $page->archive_users()->attach(Auth::user()->id);
        }
        if(count($page->users) == 0){
            $page->status_code = 104;
            $page->save();
        }
        return response()->json(['success' => '1', 'message' => $page->name.' Archived']);
    }

    public function unarchivePage(Request $request){
        $page = Auth::user()->archive_pages()->find($request->id);
        Auth::user()->archive_pages()->detach($page->id);
        if(!Auth::user()->pages->contains($page->id)){
            Auth::user()->pages()->attach($page->id);
        }
        if($page->status_code == 104){
            $page->status_code = 102;
            $page->save();
        }
        return response()->json(['success' => '1', 'message' => $page->name.' UnArchived']);
    }
}
