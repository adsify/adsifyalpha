<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home(){
    	return view('adsify.home');
    }

    public function faq(){
    	return view('adsify.faq');
    }

    public function policy(){
    	return view('adsify.policy');
    }

    public function terms(){
    	return view('adsify.terms');
    }
}
