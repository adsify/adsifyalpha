<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use App\Paypal\CreateAgreement;
use App\Paypal\CreatePlan;
use App\Adsify\Mailing;
use App\Adsify\Manager;
use Auth;
use App\Server;
use App\Log;
use App\Page;
use App\User;
use App\Subscription;
use App\Ticket;
use App\Comment;
use App\Plan;
use App\Featured;
use App\Category;
use App\Profile;
use App\Payment;
use App\Agreement;


class AdminController extends Controller
{
    public function __construct(){
    	$this->middleware('auth');
		$this->middleware('admin');
	}

	public function servers(){
		$servers = Server::all();
		$logs = Log::all();
		return view("admin.servers" , ["servers" => $servers , "logs" => $logs]);
	}

	public function addServer(Request $request){
		$server = new Server();
		$server->name = $request->input('name');
		$server->status_code = 201;
		$server->description = $request->input('description');
		$server->save();
		return redirect('admin/servers');
	}

	public function removeServer($id){
		$server = Server::find($id);
		$server->delete();
		return redirect('/admin');
	}

	public function removeProfile($id){
		$profile = Profile::find($id);
		$profile->delete();
		return redirect('/admin');
	}

	public function getLogs($code){
		$logs = Log::where('code','=',$code)->with('profile')->orderBy('updated_at','desc')->get();
		return $logs;
	}

	public function serverLogs($id){
		$server = Server::find($id);
		if($server->log == 1){
			$server->log = 0;
			$server->save();
		}
		$logs = $server->logs()->with('server')->orderBy('updated_at','desc')->get();
		if(!$logs){
			$logs = [];
		}
		return ['logs' => $logs, 'server' => $server];
	}

	public function profileLogs($id){
		$profile = Profile::find($id);
		if($profile->log == 1){
			$profile->log = 0;
			$profile->save();
		}
		$logs = $profile->logs()->with('profile')->orderBy('updated_at','desc')->get();
		if(!$logs){
			$logs = [];
		}
		return ['logs' => $logs, 'profile' => $profile];
	}

	public function serverClearLogs($id){
		$server = Server::find($id);
		$profiles = $server->profiles;
		foreach($profiles as $prof){
			$logs = $prof->logs;
			foreach ($logs as $log) {
				if($log->type == "image"){
					Storage::delete('/storage/errors/'.$log->message);
				}
				$log->delete();
			}
		}
		return redirect('/admin')->with('success','All Logs Cleared');
	}

	public function profileClearLogs($id){
		$profile = Profile::find($id);
		$logs = $profile->logs;
		foreach ($logs as $log) {
			if($log->type == "image"){
				Storage::delete('/storage/errors/'.$log->message);
			}
			$log->delete();
		}
	}

	public function serverUpdate(Request $request){
		$server = Server::find($request->input('id'));
		$server->name = $request->input('name');
		$server->description = $request->input('description');
		$server->status_code = $request->input('status');
		$server->save();
		return redirect('/admin/servers');
	}

	public function profileUpdate(Request $request){
		$profile = Profile::find($request->input('id'));
		$profile->name = $request->input('name');
		$profile->proxy = $request->input('proxy');
		$profile->fb = $request->input('fb');
		$profile->status_code = $request->input('status');
		$profile->save();
		return redirect('/admin/servers');
	}

	public function pages(){
		$pages = Page::withCount('users')->orderBy('users_count', 'desc')->get();
		$totalusers = User::count(); 
		$x = 0;
		$top5 = array();
		foreach ($pages as $page) {
			if($x++ < 5){
				$page->percent = $this->Percent($page->users_count,$totalusers);
				array_push($top5,$page);
			}
			if($x == 5){
				break;
			}
		}
		return view("admin.pages", ["pages" => $pages , "top5" => $top5]);
	}

	public function featuredPages(){
		$pages = Featured::all();
		return view('admin.featured',['pages' => $pages]);
	}

	public function storeFeatured(Request $request){
		$page = new Featured();
		$id = $this->getpageinfo($request->input('link'));
		$page->id = $id;
		$page->name = $request->input('name');
		$page->logo_src = $this->getPageLogo($id);
		$page->ads = $request->input('ads');
		$page->likes = $request->input('likes');
		$page->chance = $request->input('chance');
		$page->shop = $request->input('shop');
		$page->save();
		return redirect('/admin/pages/featured');
	}

	public function getpageinfo($fburl){
        $url = "https://findmyfbid.com/?__amp_source_origin=https%3A%2F%2Ffindmyfbid.com";
        try{
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POSTFIELDS,'url='.$fburl);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.3) Gecko/20070309 Firefox/2.0.0.3");
            $result = curl_exec($ch) or die(curl_error($ch));
            if(strlen($result) > 5){
                $data = preg_split('/:/', $result);
                $id = str_replace('}',"",$data[1]);
                return $id;
            }
        }catch(Exception $ex){
            Manager::addLog(0,501,$ex->getMessage());
            return redirect('journal')->with('warning','Something went wrong! Please contact support.');
        }
    }

	public function getPageLogo($id){
        try {
            $fb = new \Facebook\Facebook([
              'app_id' => '287449085238857',
              'app_secret' => 'a452ab0406a32ecbe2060c197505552c',
              'default_graph_version' => 'v3.2',
              'default_access_token' => 'EAAEFbvSd3kkBAN67CWOK1LF0JZAyopKE5NaTprS2vHdPb1ibPdSOBuAUlKcswIsfKF5RBzWiWMhW79hZBHySIOJzPIKs2nwOPWF99HYCxXgFp5BmxydYNrFz7b3MfNRjJQvwNlD3py9JoWNKXcIR6hm38r3LQZD'
            ]);
            
            $response = $fb->get(
                "/".$id."/picture?redirect=0&height=300"
            );
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
          echo 'Graph returned an error: ' . $e->getMessage();
          return false;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
          echo 'Facebook SDK returned an error: ' . $e->getMessage();
          return false;
        }
        $graphNode = $response->getGraphNode();
        $path = "storage/image/$id.png";
        $this->download_image($graphNode['url'],$path);
        return $path;
    }

    public function download_image($url,$name){
        $ch = curl_init($url);
        $fp = fopen($name, 'wb');
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_exec($ch);
        curl_close($ch);
        fclose($fp);
    }

	public function activeUsers(){
		$users = User::whereHas('subscription', function ($query) {
		    $query->where('status_code', '!=', 900);
		})->get();
		$title = "Active";
		return view("admin.users", ['users' => $users , 'title' => $title]);
	}

	public function suspendUsers(){
		$users = User::whereHas('subscription', function ($query) {
		    $query->where('status_code', '=', 900);
		})->get();
		$title = "Suspended";
		return view("admin.users", ['users' => $users , 'title' => $title]);
	}

	public function userLogs($id){
		$logs = DB::table('activity_log')->where('causer_id','=',$id)->orderBy('created_at','desc')->get();
		return $logs;
	}

	public function userInfo($id){
		$user = User::find($id);
		return view('admin.profile', ['user' => $user]);
	}

	public function billedSubs(){
		$today = Subscription::whereDate('created_at', Carbon::today())->where('status_code',302)->get()->count();
		$totalusers = User::count();
		$month = Subscription::where('status_code','=' , 302)->get()->count();
		$month = $month * 20;
		$title = "billed";
		$subs = Subscription::where('status_code','=' , 302)->get();
		return view("admin.subscriptions" , ['today' => $today , 'totalusers' => $totalusers , 'month' => $month , 'subs' => $subs , 'title' => $title]);
	}

	public function unbilledSubs(){
		$today = Subscription::whereDate('created_at', Carbon::today())->where('status_code',302)->get()->count();
		$totalusers = User::count();
		$month = Subscription::where('status_code','=' , 302)->get()->count();
		$month = $month * 20;
		$title = "unbilled";
		$subs = Subscription::where('service','=' , 'pending')->get();
		foreach ($subs as $sub) {
			$delay = $this->count_days($sub->updated_at);
			$sub->delay = $delay;
		}
		return view("admin.subscriptions" , ['today' => $today , 'totalusers' => $totalusers , 'month' => $month , 'subs' => $subs , 'title' => $title]);
	}

	public function suspendedSubs(){
		$today = Subscription::whereDate('created_at', Carbon::today())->where('status_code',302)->get()->count();
		$totalusers = User::count();
		$month = Subscription::where('status_code','=' , 302)->get()->count();
		$month = $month * 20;
		$title = "canceled";
		$subs = Subscription::where('status_code','=' , 303)->get();
		return view("admin.subscriptions" , ['today' => $today , 'totalusers' => $totalusers , 'month' => $month , 'subs' => $subs , 'title' => $title]);
	}

	public function freeSubs(){
		$today = Subscription::whereDate('created_at', Carbon::today())->where('status_code',302)->get()->count();
		$totalusers = User::count();
		$month = Subscription::where('status_code','=' , 302)->get()->count();
		$month = $month * 20;
		$title = "free";
		$subs = Subscription::where('status_code','=' , 301)->get();
		return view("admin.subscriptions" , ['today' => $today , 'totalusers' => $totalusers , 'month' => $month , 'subs' => $subs , 'title' => $title]);
	}

	public function count_days($update){
        $now = Carbon::today();
        $updated = new Carbon($update);
        $difference = $now->diffInDays($updated);
        return $difference;
    }

	public function pendingTickets(){
		$tickets = Ticket::where('status_code','=',601)->orderBy('updated_at','desc')->get();
		$title = "pending";
		$categories = Category::all();
		return view("admin.tickets" , ['tickets' => $tickets , 'title' => $title, 'categories' => $categories]);
	}

	public function solvedTickets(){
		$tickets = Ticket::where('status_code','=',602)->orderBy('updated_at','desc')->get();
		$title = "solved";
		$categories = Category::all();
		return view("admin.tickets" , ['tickets' => $tickets , 'title' => $title, 'categories' => $categories]);
	}

	public function canceledTickets(){
		$tickets = Ticket::where('status_code','=',603)->orderBy('updated_at','desc')->get();
		$title = "canceled";
		$categories = Category::all();
		return view("admin.tickets" , ['tickets' => $tickets , 'title' => $title, 'categories' => $categories]);
	}

	public function createTicket(Request $request){
		$user = User::where('email','=',$request->input('email'))->first();
			if($user->subscription->status_code == 301){
				$params = [
		            'mail' => $user->email,
		            'name' => $user->name,
		            'title' => $request->input('title'),
		            'message' => $request->input('message'),
		        ];
	        Mailing::ticket($params,"message");
			return redirect()->back()->with('warning','This user is not subscribed to read tickets; an email was sent to him instead.');
		}
		$ticket = new Ticket();
		$ticket->title = $request->input('title');
        $ticket->user_id = $user->id;
        $ticket->ticket_id = strtoupper(str_random(10));
        $ticket->category_id = $request->input('category');
        $ticket->priority = 3;
        $ticket->message = $request->input('message');
        $ticket->status_code = 601;
        $ticket->save();
        $params = [
            'mail' => $user->email,
            'name' => $user->name,
            'id' => $ticket->ticket_id,
        ];
        Mailing::ticket($params,"admin");
        session()->flash('success', "A ticket with ID: $ticket->ticket_id has been opened");
        return redirect('/admin/tickets/pending');
	}

	public function showTicket($id){
		$ticket = Ticket::find($id);
	    $comments = $ticket->comments;
		return view("admin.chat" , compact('ticket','comments'));
	}

	public function postComment(Request $request){

        $comment = Comment::create([
            'ticket_id' => $request->input('ticket_id'),
            'user_id'    => Auth::user()->id,
            'comment'    => $request->input('comment'),
        ]);
        $ticket = Ticket::find($request->input('ticket_id'));

        $user = $ticket->user;
        $params = [
            'mail' => $user->email,
            'name' => $user->name,
            'id' => $ticket->ticket_id,
        ];
        Mailing::ticket($params,"respond");

        return redirect()->back()->with("success", "Comment submitted.");
	}

	public function cancelTicket($id){

        $ticket = Ticket::find($id);
        $ticket->status_code = 603;
        $ticket->save();
        $user = $ticket->user;
        $params = [
            'mail' => $user->email,
            'name' => $user->name,
            'id' => $ticket->ticket_id,
        ];
        Mailing::ticket($params,"canceled");
        return redirect()->back()->with("warning", "Ticket : #$ticket->ticket_id has been canceled!");
    }

    public function solveTicket($id){
    	$ticket = Ticket::find($id);
        $ticket->status_code = 602;
        $ticket->save();
        $user = $ticket->user;
        $params = [
            'mail' => $user->email,
            'name' => $user->name,
            'id' => $ticket->ticket_id,
        ];
        Mailing::ticket($params,"solved");
        return redirect()->back()->with("success", "Ticket : #$ticket->ticket_id has been canceled!");
    }

	public function Percent($nombre, $total) {
		return $nombre * 100 / $total;
	}

	public function suspend_user($id){
		$user = User::find($id);
		Manager::updateSubscription($user,900,0,"by admin");
		if($user->subscription->recurring_id != null){
			$agreement = new CreateAgreement();
        	$agreement->cancel($user->subscription->recurring_id);
		}
		$params = [
            'mail' => $user->email,
            'name' => $user->name,
        ];
        Mailing::userNotification($params,"suspend");
		session()->flash('success', "User $user->email has been suspended");
		return redirect()->back();
	}

	public function activate_user($id){
		$user = User::find($id);
		Manager::updateSubscription($user,301,0,"by admin");
		$params = [
            'mail' => $user->email,
            'name' => $user->name,
        ];
        Mailing::userNotification($params,"activate");
		session()->flash('success', "User $user->email has been activated");
		return redirect()->back();
	}

	public function suspend_sub($id){
		$user = User::find($id);
		Manager::updateSubscription($user,303,0,"by admin");
		if($user->subscription->recurring_id != null){
			$agreement = new CreateAgreement();
        	$agreement->cancel($user->subscription->recurring_id);
		}
		$params = [
            'mail' => $user->email,
            'name' => $user->name,
        ];
        Mailing::subscription($params,"suspended");
		session()->flash('success', "User $user->email Subscription has been updated");
		return redirect()->back();
	}

	public function activate_sub($id){
		$user = User::find($id);
		Manager::updateSubscription($user,302,0,"by admin");
		$params = [
            'mail' => $user->email,
            'name' => $user->name,
        ];
        Mailing::subscription($params,'created');
		session()->flash('success', "User $user->email Subscription has been updated");
		return redirect()->back();
	}

	public function cancel_sub($id){
		$user = User::find($id);
		Manager::updateSubscription($user,304,0,'by admin');
		try{
			if($user->subscription->recurring_id != null){
				$agreement = new CreateAgreement();
	        	$agreement->cancel($user->subscription->recurring_id);
			}
		}catch(Exception $ex){
            //Manager::addLog(0,501,$ex->getMessage());
            //return redirect('ad')->with('warning','Something went wrong! Please contact support.');
        }
		$params = [
            'mail' => $user->email,
            'name' => $user->name,
        ];
        Mailing::subscription($params,'canceled');
		session()->flash('success', "User $user->email Subscription has been updated");
		return redirect()->back();
	}

	public function manualPayment(Request $request){
		return view('admin.payments', ['email' => $request->input('email')]);
	}

	public function createPayment(Request $request){
		$user = User::where('email','=',$request->input('email'))->first();
		$payment_amount = $request->input('amount');
		$type = $request->input('type');
		$transaction_id = $request->input('txn_id');
		Manager::addPayment($user,701,$payment_amount,"paypal",$transaction_id,"Paid");
        $params = [
            'mail' => $user->email,
            'name' => $user->name,
        ];
        Mailing::subscription($params,"paid");
        return redirect('admin/users');
	}

    public function listPlans(){
    	$plans = Plan::all();
        return view('admin.plans', ['plans' => $plans]);
    }

	public function createPlan(Request $request){
        $plan = new CreatePlan();
        $xplan = $plan->create($request);
        if(isset($xplan->id)){
        	$dbPlan = new Plan();
	        $dbPlan->name = $xplan->name;
	        $dbPlan->price = $xplan->payment_definitions[0]->amount->value;
	        $dbPlan->plan_id = $xplan->id;
	        $dbPlan->state = $xplan->state;
	        $dbPlan->save();
        }
        return redirect('/admin/payments/plans')->with('success','Plan Created!');
    }

    public function showPlan($id){
    	$cp = new CreatePlan();
        $plan = $cp->getPlan($id);
        dd($plan);
    }

    public function activatePlan($id){
        $plan = new CreatePlan();
        $plan->activate($id);
        $dbPlan = Plan::where('plan_id','=',$id)->get()->first();
        $dbPlan->state = "ACTIVE";
        $dbPlan->save();
        return redirect()->back()->with('success','Plan Activated!');
    }

    public function unactivatePlan($id){
    	$plan = new CreatePlan();
        $plan->unactivate($id);
        $dbPlan = Plan::where('plan_id','=',$id)->get()->first();
        $dbPlan->state = "INACTIVE";
        $dbPlan->save();
        return redirect()->back()->with('success','Plan Unactivated!');
    }

    public function deletePlan($id){
        $plan = new CreatePlan();
        $plan->delete($id);
        $dbPlan = Plan::where('plan_id','=',$id)->get()->first();
        $dbPlan->delete();
        return redirect('/admin/payments/plans')->with('success','Plan Deleted!');
    }

    public function settings(){
    	return view('admin.settings');
    }

    public function updateSettings(Request $request){
    	Manager::updateDotEnv('MAINTENANCE', $request->input('maintenance'));
    	Manager::updateDotEnv("MAINTENANCE_MESSAGE",$this->format_message($request->input('message')));
    	Manager::updateDotEnv('FREE_PAGES', $request->input('pages'));
    	Manager::updateDotEnv('DELAY', $request->input('delay'));
    	Manager::updateDotEnv('TRIAL', $request->input('trial'));
    	return redirect('admin/settings');
    }

    public function format_message($message){
    	return str_replace(" ","_",$message);
    }

    public function removeDuplicates($id){
    	$user = User::find($id);
    	$pages = $user->pages;
    	$archives = $user->archive_pages;
    }
}
