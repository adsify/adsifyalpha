<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\PageResource;
use App\Http\Requests;
use Carbon\Carbon;
use App\Page;
use App\User;
use App\Post;
use App\Server;
use App\Country;
use App\Error;
use App\Task;
use App\Log;
use App\Stat;
use App\NewPage;
use App\Profile;
use App\Similar;
use App\Adsify\Manager;
use App\Adsify\Mailing;
use Illuminate\Http\UploadedFile;
use App\Jobs\ProcessTasks;


class ApiController extends Controller
{
    //For instant
    public function taskPosts(Request $request){
        try {
            $data = $request->getContent();
            $json = json_decode($data, false);
            $bdcountry = Country::find($json->country);
            $bdpage = Page::find($json->page);
            $posts = $json->posts;
            $newposts = [];
            foreach ($posts as $post) {
                $newpost = $this->store_post($post,$bdpage,"1");
                $this->attach_post($newpost,$bdcountry);
                array_push($newposts,$newpost);
            }
            //uncomment to detach unused countries after updating posts
            //$this->posts_status($bdpage->posts,$json->country,$newposts);
            $bdpage->status_code = 102;
            $bdpage->save();
            echo '1';
        } catch (Exception $e) {
            echo '0';
        }
    }

    //For Old Server
    public function oldposts(Request $request){
        try {
            $data = $request->getContent();
            $pages = json_decode($data, false);
            foreach($pages as $page){
                $page_posts = [];
                $bdpage = Page::find($page->page);
                $old_posts = $bdpage->posts;
                if($page->status_ads == 1){
                    $countries = $page->countries;
                    $post_countries = [];
                    foreach ($countries as $country) {
                        $bdcountry = $this->store_country($country);
                        $posts = $country->posts;
                        foreach ($posts as $post) {
                            $newpost = $this->store_post($post,$bdpage);
                            array_push($page_posts,$newpost);
                            $this->attach_post($newpost,$bdcountry);
                        }
                    }
                    $bdpage->status_code = 102;
                    $this->old_post_status($page_posts,$old_posts);
                }else{
                    $bdpage->status_code = 103;
                    $this->stoppedAds($bdpage);
                }
                $bdpage->save();
                
            }
            echo '1';
        } catch (Exception $e) {
            echo '0';
        }
    }

    //For New Server
    public function posts(Request $request){
        try {
            $data = $request->getContent();
            $page = json_decode($data, false);
            $bdpage = Page::find($page->page);
            $country = $page->country;

            $bdcountry = $this->store_country($country);
            
            $posts = $country->posts;
            $newposts = [];
            foreach ($posts as $post) {
                $newpost = $this->store_post($post,$bdpage);
                $this->attach_post($newpost,$bdcountry);
                array_push($newposts,$newpost);
            }
            $this->posts_status($bdpage->posts,$country,$newposts);

            $bdpage->status_code = 102;
            $bdpage->save();
            echo '1';
        } catch (Exception $e) {
            echo '0';
        }
    }

    public function stoppedAds($page){
        $posts = $page->posts;
        foreach($posts as $post){
            if($post->status_code != 113){
                $post->status_code = 113;
                $post->save();
            }
        }
    }

    public function store_post($post,$page,$instant = "0"){
    	$bdpost = Post::find($post->id);
    	if(!$bdpost){
    		$newpost = new Post();
    		$newpost->id = $post->id;
    		$newpost->type = $post->type;
    		$newpost->text = $this->clean_text($post->text);
    		$newpost->post = $post->post;

            $name = $page->id.'_'.$newpost->id;
            if($post->type == 'list'){
                $x = 1;
                foreach($post->url as $url){
                    $nurl = $this->downloader($url,$name."_".$x++,"image");
                    $newpost->url = $newpost->url . $nurl . ";";
                }
            }else if($post->type == 'video'){
                $newpost->url = $this->downloader($post->url,$name,$post->type);
                $newpost->thumbnail = $this->downloader($post->thumbnail,"Thumbnail_".$name,'image');
            }else if($post->type == 'image'){
                $newpost->url = $this->downloader($post->url,$name,$post->type);
            }
    		$newpost->page_id = $page->id;
    		$newpost->status_code = 111;
            if($post->stats->date != "0"){
                $newpost->created_at = Carbon::createFromTimestamp($post->stats->date)->toDateTimeString();
            }
    		$newpost->save();

            $stats = $post->stats;
            $stat = new Stat();
            $stat->post_id = $post->id;
            $this->store_stats($stats,$stat);
    		return $newpost;
    	}else{
            if($instant == "0"){
    		  $bdpost->status_code = 112;
            }
            $bdpost->save();
            $stats = $post->stats;
            $stat = $bdpost->stats;
            $this->store_stats($stats,$stat);
    		return $bdpost;
    	}	
    }

    public function store_stats($stats,$stat){
        $stat->likes = $stats->likes;
        $stat->comments = $stats->comments;
        $stat->shares = $stats->shares;
        $stat->views = $stats->views;
        $stat->save();
        return $stat;
    }

    public function clean_text($text){
        $text = preg_replace('/<span class="text_exposed_link">(.*?)<\/span>/', "", $text);
        $text = preg_replace('/<div class="_43f9 _63qh" (.*?)>(.*?)<\/div>/', "", $text);
        $text = preg_replace('/<span class="_6qdm" (.*?)>/', '<span class="_6qdm" >', $text);
        $text = preg_replace('/See Translation/', '', $text);
        return $text;
    }

    public function old_post_status($new,$old){
    	foreach($old as $op){
    		$exists = 0;
    		foreach ($new as $np) {
    			if($np->id == $op->id){
    				$exists = 1;
    				break;
    			}
    		}
    		if(!$exists){
    			$op->status_code = 113;
    			$op->save();
    		}
    	}
    }

    public function posts_status($oldposts,$country,$newposts){
        $cposts = $oldposts->filter(function($post) use ($country){
            foreach ($post->countries as $c) {
                if($c->id == $country){
                    return true;
                }
            }
            return false;
        });
        foreach ($cposts as $post) {
            $exist = false; 
            foreach ($newposts as $newpost) {
                if($post->id == $newpost->id){
                    $exist = true;
                }
            }
            if(!$exist){
                $post->countries()->detach($country);
            }
        }
    }


    public function verify_posts2(){
        $pages = Page::where('status_code','!=',104)->get();
        foreach ($pages as $page) {
            $posts = $page->posts()->where('status_code','!=',113)->get();
            foreach ($posts as $post) {
                if(sizeof($post->countries) == 0){
                    $post->status_code = 113;
                    $post->save();
                }
            }
        }
    }

    public function verify_posts(){
        $pages = Page::where('status_code','!=',104)->get();
        foreach ($pages as $page) {
            $posts = $page->posts()->where('status_code','!=',113)->get();
            foreach ($posts as $post) {
                $diff = $this->count_days($post->stats->updated_at);
                if($diff > 30){
                    $post->status_code = 113;
                    $post->save();
                }
            }
        }
    }

    public function verify_users(){
        $subs = Subscription::where('status_code','==',302)->get();
        foreach($subs as $sub){
            $user = $sub->user;
            if($sub->service == "paypal"){
                $days = $this->count_days($sub->updated_at);
                if($days >= 32){
                    Manager::updateSubscription($user,302,0,"pending");
                    return redirect('/journal/subscription');
                }
            }else if($sub->service == "pending"){
                $delay = $this->count_days($sub->updated_at);
                if($delay >= config('adsify.delay')){
                    Manager::updateSubscription($user,301,0,"archive");
                }
            }else if($sub->service == "free"){
                $days = $this->count_days($sub->updated_at);
                if($days > config('adsify.trial')){
                    Manager::archivePages($user);
                }
            }
        }
    }

    public function attach_post($post,$cntry){
    	$countries = $post->countries()->get();
    	$exist = 0;
    	if(count($countries) != 0){
	    	foreach ($countries as $country) {
	    		if($country->id == $cntry->id){
	    			$exist = 1;
                    break;
	    		}
	    	}
	    	if(!$exist){
	    		$post->countries()->attach($cntry->id);
	    	}
	    }else {
	    	$post->countries()->attach($cntry->id);
	    }
    }

    public function downloader($url,$name,$type){
        if($type == "image"){
            file_put_contents("storage/image/" . trim($name) . ".png", file_get_contents($url));
            return 'storage/image/'. $name . ".png";
        }else if($type == "video"){
            file_put_contents("storage/video/" . trim($name) . ".mp4", file_get_contents($url));
            return 'storage/video/'. $name . ".mp4";
        }
    }

    public function oldpages(){
    	$pages = Page::where('status_code','!=',104)->get();
    	$servers = Server::where([
                ['status_code', '=', 201],
                ['name', 'like', '%server%'],
            ])->get();
    	foreach($servers as $server){
    		$server->pages()->detach();
    	}
    	$totalservers = count($servers);
        //send log of how many servers/pages sent to be examined
    	$dis = 1;
    	foreach ($pages as $page) {
            if(count($page->users) > 0){
                $page->servers()->attach($servers[$dis-1]->id);
                if($dis == $totalservers){
                    $dis = 1;
                }else{
                    $dis++;
                }
            }else{
                $page->status_code = 104;
                $page->save();
            }
    	}
    	$data = ServerResource::collection($servers);
    	$json = json_encode($pages);
    	return $json;
    }

    public function pages(){
        $pages = Page::whereNotIn('status_code',[101,104])->get();
        if(sizeof($pages)){
            $data = PageResource::collection($pages);
            $json = json_encode($data);
            return $json;
        }else{
            echo "0";
        }
    }

    public function getPage(){
        $task = NewPage::where('type','=','page')->orderBy('created_at','desc')->first();
        if($task){
            $page = $task->page;
            $task->delete();
            $data = ['id' => $page->id];
            $json = json_encode($data);
            return $json;
        }else{
            echo "0";
        }
    }

    public function getSimPage(){
        $task = NewPage::where('type','=','similar')->orderBy('created_at','desc')->first();
        if($task){
            $page = $task->page;
            $task->delete();
            $data = ['id' => $page->id];
            $json = json_encode($data);
            return $json;
        }else{
            echo "0";
        }
    }

    public function storeTasks(Request $request){
        $data = $request->getContent();
        $json = json_decode($data, false);
        $dbpage = Page::find($json->page->id);
        $mode = $json->page->mode;
        if($mode == "instant"){
            if($json->page->name != ""){
                $dbpage->name = $json->page->name;
            }
            if($json->page->status_ads == 1){
                $dbpage->size = $json->page->size;
                foreach ($json->page->countries as $country) {
                    $task = new Task();
                    $task->page_id = $dbpage->id;
                    $task->mode = $mode;
                    $this->store_country($country);
                    $task->country = $country->link;
                    $task->save();
                }
            }else{
                $dbpage->status_code = 103;
            }
            $dbpage->save();
            return '1';
        }else if($mode == "server"){
            if($json->page->status_ads == 1){
                $dbpage->size = $json->page->size;
                foreach ($json->page->countries as $country) {
                    $task = new Task();
                    $task->page_id = $dbpage->id;
                    $task->mode = $mode;
                    $this->store_country($country);
                    $task->country = $country->link;
                    $task->save();
                }
            }else{
                $dbpage->status_code = 103;
                $this->stoppedAds($dbpage);
            }
            $dbpage->save();
            return '1';
        }
        return '0';
    }

    public function store_country($country){
        $bdcountry = Country::find($country->id);
        if(!$bdcountry){
            $newcountry = new Country();
            $newcountry->id = $country->id; 
            $newcountry->name = $country->name;
            $newcountry->save();
            return $newcountry;
        }
        return $bdcountry;
    }

    public function store_similar($data,$page){
        $similar = new Similar();
        $similar->pid = $data->id;
        $similar->name = $data->name;
        $similar->likes = $data->likes;
        $similar->ads = $data->ads;
        $similar->logo_src = $this->downloader($data->logo,$data->id,"image");
        $similar->page_id = $page;
        $similar->save();
    }

    public function similars(Request $request){
        $data = $request->getContent();
        $json = json_decode($data, false);
        $page = $json->page;
        $similars = $json->similars;
        foreach ($similars as $similar) {
            $this->store_similar($similar,$page);
        }
        echo "1";
    }

    public function restoreTasks(Request $request){
        $data = $request->getContent();
        $json = json_decode($data, false);
        foreach($json->tasks as $task){
            $dbtask = new Task();
            $dbtask = $task->id;
            $dbtask->page_id = $task->page;
            $dbtask->country = $task->link;
            $dbtask->mode = $json->mode;
            $dbtask->save();
        }
    }

    public function getTask(){
        $task = Task::where('mode','=','instant')->lockForUpdate()->first();
        if($task){
            $country_link = $task->country;
            $country_id = "0";
            if(strpos($country_link,"=")){
                $country_id = explode("=",$country_link)[1];
            }
            $page = $task->page;
            $posts = $page->postsIds;
            $data = ['id' => $task->id , 'page' => $page->id , 'name' => $page->name , 'country' => ['link' => $country_link , 'id' => $country_id ], 'posts' => $posts];
            $json = json_encode($data);
            $task->delete();
            return $json;
        }else{
            echo "0";
        }
    }

    public function getTasks(){
        $tasks = Task::where('mode','=','server')->limit(5)->lockForUpdate()->get();
        if(sizeof($tasks) > 0 ){
            $data = [];
            foreach($tasks as $task){
                $country_link = $task->country;
                $country_id = "0";
                if(strpos($country_link,"=")){
                    $country_id = explode("=",$country_link)[1];
                }
                $page = $task->page;
                $posts = $page->postsIds;
                $row = ['id' => $task->id , 'page' => $page->id , 'name' => $page->name , 'country' => ['link' => $country_link , 'id' => $country_id ], 'posts' => $posts];
                array_push($data, $row);
                $task->delete();
            }
            $json = json_encode($data);
            return $json;
        }else{
            echo "0";
        }
    }

    public function getNewTasks(){
        $this->dispatch(new ProcessTasks());
    }

    public function addServer(Request $request){
        try{
            $data = $request->getContent();
            $serv = json_decode($data, false);
            $server = new Server();
            $server->name = $serv->name;
            $server->status_code = 201;
            $server->description = $serv->description;
            $server->save();
            return $server->id;
        }catch(Exception $ex){
            return "0";
        }
        
    }

    public function addProfile(Request $request){
        try{
            $data = $request->getContent();
            $prof = json_decode($data, false);
            $profile = new Profile();
            $profile->name = $prof->name;
            $profile->proxy = $prof->proxy;
            $profile->fb = $prof->fb;
            $profile->server_id = $prof->server;
            $profile->status_code = 201;
            $profile->save();
            return $profile->id;
        }catch(Exception $ex){
            return "0";
        }
    }

    public function errors(Request $request){
        $profile = Profile::find($request->input('id'));
        $profile->log = 1;
        $profile->save();
        $log = new Log();
        $log->profile_id = $profile->id;
        $log->code = 502;
        $log->message = $request->input('message');
        $log->save();
    }

    public function imageError(Request $request){
        if($request->hasFile('media')){
            $image = $request->file('media');
            $name = "Error_".time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('../../storage/errors');
            $image->move($destinationPath, $name);

            $profile = Profile::find($request->input('id'));
            $profile->log = 1;
            $profile->save();
            $log = new Log();
            $log->profile_id = $profile->id;
            $log->code = 502;
            $log->message = $name;
            $log->type = "image";
            $log->save();

            return "1";
        }
        
        return "0";
    }

    public function updateServer(Request $request){
        $server = Server::find($request->input('id'));
        $server->status_code = $request->input('status');
        if($server->status_code == 202){
            $server->created_at = Carbon::now();
        }
        if($request->input('activity')){
            $date1 = Carbon::parse($server->created_at);
            $date2 = Carbon::now();
            $diff = $date1->diff($date2)->format('%H:%I:%S');
            $server->last_activity = $diff;
        }
        $server->save();
    }

    public function updateProfile(Request $request){
        $profile = Profile::find($request->input('id'));
        $profile->status_code = $request->input('status');
        if($profile->status_code == 202){
            $profile->created_at = Carbon::now();
        }else if($profile->status_code == 203){
            Mailing::adminNotification("server", $profile->name);
        }
        if($request->input('activity')){
            $date1 = Carbon::parse($profile->created_at);
            $date2 = Carbon::now();
            $diff = $date1->diff($date2)->format('%H:%I:%S');
            $profile->last_activity = $diff;
        }
        $profile->save();
    }

    public function serverHealth(Request $request){
        $server = Server::find($request->input('id'));
        $server->used_pro = $request->input('cpuval');
        $server->count_pro = $request->input('cpucount');
        $iserver->used_memory = $request->input('vmused');
        $server->free_memory = $request->input('vmfree');
        $server->save();
        //$this->checkServers();
    }

    public function checkServers(){
        $servers = Server::all();
        foreach($servers as $server){
            $last = Carbon::parse($server->updated_at);
            $now = Carbon::now();
            $diff = $last->diffInMinutes($now);
            if($diff >= 5){
                Manager::addLog($server->id,502,"Server Dead");
                Mailing::adminNotification("server");
            }
        }
    }

    public function checkProfiles(){
        $profiles = Profile::all();
        foreach($profiles as $profile){
            if($diff >= 5){
                Manager::addLog($profile->id,502,"Profile Dead");
                Mailing::adminNotification("server");
            }
        }
    }

    public function count_days($update){
        $updated = Carbon::parse($update);
        $now = Carbon::now();
        return $updated->diffInHours($now);
    }

    public function verify_pages(){
        $pages = Page::where('status_code','=',103)->get();
        foreach ($pages as $page) {
            $diff = $this->count_days($page->updated_at);
            if($diff > 7){
                $page->status_code = 104;
                $page->save();
            }
            
        }
    }
}
