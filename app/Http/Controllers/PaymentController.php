<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Srmklive\PayPal\Services\ExpressCheckout;
use App\Invoice;
use Auth;
use URL;
use Redirect;
use Config;
use App\Subscription;
use App\Paypal\CreatePlan;
use App\Paypal\CreateAgreement;
use App\Log;
use App\Plan;
use App\User;
use App\Payment;
use App\Agreement;
use App\Adsify\Manager;
use App\Adsify\Mailing;

class PaymentController extends Controller
{

	public function subscribe(Request $request){
        try{
            $payment = $request->input('payment');
            Manager::save_activity($request,'subscribe with '.$payment);
            if($payment == "paypal"){
                if(Auth::user()->agreement == null){
            	   return $this->createAgreement();
                }else{
                    return $this->reactivateAgreement();
                }
            }else{
            	//$this->bitpayCheckout();
            }
        }catch(Exception $ex){
            Manager::addLog(0,501,$ex->getMessage());
            return redirect('journal')->with('warning','Something went wrong! Please contact support.');
        }
    }

    public function createAgreement(){
        $plan = Plan::where('state','=','ACTIVE')->get()->first();
        if(!$plan){
            return redirect('/journal/subscription')->with('warning','Payment Services on Maintenance, Please contact support: support@adsify.io ');
        }
        $agreement = new CreateAgreement();
        return $agreement->create($plan->plan_id);
    }

    public function executeAgreement($status){
        try{
            if ($status == 'true') {
                $agreement = new CreateAgreement();
                $agreement_id = $agreement->execute(request('token'));
                $user = Auth::user();

                $agr = $agreement->getAgreement($agreement_id);
                $bdAgreement = new Agreement();
                $bdAgreement->id = $agr->id;
                $bdAgreement->state = $agr->state;
                $bdAgreement->payer_email = $agr->payer->payer_info->email;
                $bdAgreement->payer_status = $agr->payer->status;
                $bdAgreement->payer_method = $agr->payer->payment_method;
                $bdAgreement->payer_name = $agr->shipping_address->recipient_name;
                $bdAgreement->payer_address = $agr->shipping_address->line1.' '.$agr->shipping_address->city.' '.$agr->shipping_address->postal_code.' - '.$agr->shipping_address->country_code;
                $bdAgreement->user_id = $user->id;
                $bdAgreement->save();

                Manager::updateSubscription($user,302,$agreement_id,"pending");
                session()->flash('success', 'Woohoo! You have Subscribed to Adsify,Thank you!');
                return redirect('/thankyou');
            }else{
                session()->flash('danger', 'Something went wrong, try again!');
                return redirect('journal/subscription');
            }
        }catch(Exception $ex){
            Manager::addLog(0,501,$ex->getMessage());
            return redirect('journal')->with('warning','Something went wrong! Please contact support.');
        }
    }

    public function reactivateAgreement(){
        try{
            $agreement = new CreateAgreement();
            $agreement = $agreement->reactivate(Auth::user()->agreement->id);
            if($agreement->state == 'Active'){
                $user = Auth::user();
                $agr = $user->agreement;
                $agr->state = "Active";
                $agr->save();
                //if($user->subscription->status_code != 303)
                Manager::updateSubscription($user,302,0,"reactive");
                $params = [
                    'mail' => $user->email,
                    'name' => $user->name,
                ];
                Mailing::subscription($params,"reactivated");
            }
            session()->flash('success', 'Welcome Back! You re-subscribed to Adsify. Check your email for more info.');
            return redirect('journal/subscription');
        }catch(Exception $ex){
            Manager::addLog(0,501,$ex->getMessage());
            return redirect('journal')->with('warning','Something went wrong! Please contact support.');
        }
    }

    public function cancel(){
        try{
            $user = Auth::user();
            $agreement = new CreateAgreement();
            $agreement->cancel($user->agreement->id);
            $agr = $user->agreement;
            $agr->state = "Cancel";
            $agr->save();
            Manager::updateSubscription($user,302,"0","cancel");
            $params = [
                'mail' => $user->email,
                'name' => $user->name,
            ];
            Mailing::subscription($params,"canceled");
            session()->flash('warning', 'You unsubscribed from Adsify! Check your email for more info.');
            return redirect('journal/subscription');
        }catch(Exception $ex){
            Manager::addLog(0,501,$ex->getMessage());
            return redirect('journal')->with('warning','Something went wrong! Please contact support.');
        }
    }

    public function paypalNotify(Request $request)
    {

        $request->merge(['cmd' => "_notify-validate"]);
        $post = $request->all();
        //$url = config('services.paypal.sandbox.notify');
        $url = config('services.paypal.live.notify');
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));

        if ( !($res = curl_exec($ch)) ) {
          curl_close($ch);
        }
        curl_close($ch);

        if (strcmp ($res, "VERIFIED") == 0) {
            $payment_status = $request->input('payment_status');
            $payment_amount = $request->input('mc_gross');
            $transaction_type = $request->input('txn_type');
            $transaction_id = $request->input('txn_id');
            
            $recurring_id = $request->input('recurring_payment_id');

            $payer_email = $request->input('payer_email');
            $payer_status = $request->input('payer_status');
            $agreement = new CreateAgreement();
            
            $agr = $this->getUserAgreement($recurring_id);
            if($agr){
                $user = $agr->user;
                if($transaction_type == "recurring_payment"){
                    Manager::updateSubscription($user,302,"0","payment");
                    Manager::addPayment($user,701,$payment_amount,"paypal",$transaction_id,"Paid");
                    $params = [
                        'mail' => $user->email,
                        'name' => $user->name,
                    ];
                    Mailing::subscription($params,"paid");
                }else if($transaction_type == 'recurring_payment_profile_created'){
                    $params = [
                        'mail' => $user->email,
                        'name' => $user->name,
                    ];
                    Mailing::subscription($params,"created");
                    Mailing::adminNotification("money");
                }else if($transaction_type == 'recurring_payment_suspended'){
                    /*Manager::updateSubscription($user,302,"0","cancel");
                    $agr = $user->agreement;
                    $agr->state = "Cancel";
                    $agr->save();
                    $params = [
                        'mail' => $user->email,
                        'name' => $user->name,
                    ];
                    Mailing::subscription($params,"canceled");*/
                }else if($transaction_type == 'recurring_payment_profile_cancel'){
                    Manager::updateSubscription($user,303,"0","archive");
                    //Manager::addPayment($user,702,0,"paypal","None","Suspended");
                    $agr = $user->agreement;
                    $agr->state = "Suspend";
                    $agr->save();
                    $params = [
                        'mail' => $user->email,
                        'name' => $user->name,
                    ];
                    Mailing::subscription($params,"suspended");
                }
            }else{
                $message = "Unknown paypal Notification from : $payer_email , txn_id : $transaction_id , type : $transaction_type , recurrement : $recurring_id ,amount : $payment_amount.";
                Manager::addLog(0,503,$message);
            }
        }
    }

    public function getUserAgreement($id){
        $agreement = Agreement::find($id);
        return $agreement;
    }

}
