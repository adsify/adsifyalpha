<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Srmklive\PayPal\Services\ExpressCheckout;
use App\Http\Resources\PageResource;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use Cookie;
use Config;
use Auth;
use Validator;
use DOMDocument;
use DOMXpath;
use App\Mail\AdsifyMail;
use Carbon\Carbon;
use App\Adsify\Mailing;
use App\Adsify\Manager;
use App\Page;
use App\User;
use App\Post;
use App\Activity_Log;
use App\Subscription;
use App\Ticket;
use App\Task;
use App\Country;
use App\Updator;
use App\Log;
use App\NewPage;
use App\Server;
use App\Featured;
use App\Similar;
use App\Category;
use App\Comment;

class StatsController extends Controller
{
    public function statsPages(){
        $processing = Page::where('status_code','=',101)->get()->count();
        $running = Page::where('status_code','=',102)->get()->count();
        $broken = Page::where('status_code','=',103)->get()->count();
        $unused = Page::where('status_code','=',104)->get()->count();

        return response()->json(['processing' => $processing, 'running' => $running, 'broken' => $broken, 'unused' => $unused]);
    }

    public function statsUsers2(){
        $users = User::select('id','created_at')->get()->groupBy(function($user){
            return Carbon::parse($user->created_at)->format('M d');
        });
        $keys = [];
        $values = [];
        foreach($users as $key => $data){
            array_push($keys,$key);
            array_push($values,sizeof($data));
        }
        return response()->json(['dates' => $keys,'values' => ["name" => "Users", "data"=> $values]]);
    }

    public function statsUsers(Request $request){
        $year = $request->year;
        $users = User::where('created_at', '>=', $year.'-01-01 00:00:01')
                            ->where('created_at','<=',$year.'-12-31 23:59:59')
                            ->select(\DB::raw('COUNT(*) as login_num, DATE_FORMAT(created_at, "%m") as login_month'))
                            ->groupBy('login_month')
                            ->get();
        
        /*$keys = [];
        $values = [];
        foreach($users as $month){
            array_push($keys,$month->login_month);
            array_push($values,$month->login_num);
        }
        $keys = $this->my_reverse($keys);
        $values = $this->my_reverse($values);*/
        return $users;
        return response()->json(['data' => $users]);
    }

    public function my_reverse($arr){
        $k = array_keys($arr);
        $v = array_values($arr);

        $rv = array_reverse($v);

        return array_combine($k, $rv);
    }
}
